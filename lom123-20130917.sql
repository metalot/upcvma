-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: lom123
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AuthAssignment`
--

DROP TABLE IF EXISTS `AuthAssignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthAssignment`
--

LOCK TABLES `AuthAssignment` WRITE;
/*!40000 ALTER TABLE `AuthAssignment` DISABLE KEYS */;
INSERT INTO `AuthAssignment` VALUES ('Admin','1',NULL,'N;'),('Admin','14',NULL,'N;'),('Admin','22',NULL,'N;'),('Admin','23',NULL,'N;'),('Admin','34',NULL,'N;'),('Admin','40',NULL,'N;'),('ContentManager','20',NULL,'N;'),('ContentManager','23',NULL,'N;'),('ContentManager','40',NULL,'N;'),('CourseAdmin','18',NULL,'N;'),('CourseAdmin','2',NULL,'N;'),('CourseAdmin','23',NULL,'N;'),('CourseAdmin','32',NULL,'N;'),('CourseAdmin','33',NULL,'N;'),('CourseAdmin','40',NULL,'N;'),('Guest','23',NULL,'N;'),('Guest','40',NULL,'N;'),('Operator','17',NULL,'N;'),('Operator','23',NULL,'N;'),('Operator','31',NULL,'N;'),('Operator','40',NULL,'N;'),('Operator','7',NULL,'N;'),('Student','23',NULL,'N;'),('Student','24',NULL,'N;'),('Student','25',NULL,'N;'),('Student','26',NULL,'N;'),('Student','27',NULL,'N;'),('Student','28',NULL,'N;'),('Student','29',NULL,'N;'),('Student','30',NULL,'N;'),('Student','35',NULL,'N;'),('Student','36',NULL,'N;'),('Student','37',NULL,'N;'),('Student','38',NULL,'N;'),('Student','39',NULL,'N;'),('Student','40',NULL,'N;'),('Student','49',NULL,'N;'),('Student','50',NULL,'N;'),('Student','51',NULL,'N;'),('Student','7',NULL,'N;'),('Tutor','16',NULL,'N;'),('Tutor','23',NULL,'N;'),('Tutor','40',NULL,'N;'),('Tutor','7',NULL,'N;'),('UserManager','21',NULL,'N;'),('UserManager','23',NULL,'N;'),('UserManager','40',NULL,'N;');
/*!40000 ALTER TABLE `AuthAssignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthItem`
--

DROP TABLE IF EXISTS `AuthItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthItem`
--

LOCK TABLES `AuthItem` WRITE;
/*!40000 ALTER TABLE `AuthItem` DISABLE KEYS */;
INSERT INTO `AuthItem` VALUES ('Admin',2,'Sistemos administratorius',NULL,'N;'),('ClassificatorItems.*',1,NULL,NULL,'N;'),('ClassificatorItems.Admin',0,NULL,NULL,'N;'),('ClassificatorItems.Create',0,NULL,NULL,'N;'),('ClassificatorItems.Delete',0,NULL,NULL,'N;'),('ClassificatorItems.Index',0,NULL,NULL,'N;'),('ClassificatorItems.Update',0,NULL,NULL,'N;'),('ClassificatorItems.View',0,NULL,NULL,'N;'),('Classificators.*',1,NULL,NULL,'N;'),('Classificators.Admin',0,NULL,NULL,'N;'),('Classificators.Create',0,NULL,NULL,'N;'),('Classificators.Delete',0,NULL,NULL,'N;'),('Classificators.Index',0,NULL,NULL,'N;'),('Classificators.Update',0,NULL,NULL,'N;'),('Classificators.View',0,NULL,NULL,'N;'),('ContentManager',2,'Medžiagos tvarkytojas',NULL,'N;'),('CourseAdmin',2,'Kursų administratorius',NULL,'N;'),('Courses.*',1,NULL,NULL,'N;'),('Courses.Admin',0,NULL,NULL,'N;'),('Courses.Create',0,NULL,NULL,'N;'),('Courses.Delete',0,NULL,NULL,'N;'),('Courses.Index',0,NULL,NULL,'N;'),('Courses.Update',0,NULL,NULL,'N;'),('Courses.View',0,NULL,NULL,'N;'),('DalykinesSritys.*',1,NULL,NULL,'N;'),('DalykinesSritys.Admin',0,NULL,NULL,'N;'),('DalykinesSritys.Create',0,NULL,NULL,'N;'),('DalykinesSritys.Delete',0,NULL,NULL,'N;'),('DalykinesSritys.Index',0,NULL,NULL,'N;'),('DalykinesSritys.Update',0,NULL,NULL,'N;'),('DalykinesSritys.View',0,NULL,NULL,'N;'),('Guest',2,'Svečias',NULL,'N;'),('Kompetencijos.*',1,NULL,NULL,'N;'),('Kompetencijos.Admin',0,NULL,NULL,'N;'),('Kompetencijos.Create',0,NULL,NULL,'N;'),('Kompetencijos.Delete',0,NULL,NULL,'N;'),('Kompetencijos.Index',0,NULL,NULL,'N;'),('Kompetencijos.Update',0,NULL,NULL,'N;'),('Kompetencijos.View',0,NULL,NULL,'N;'),('Menu.*',1,NULL,NULL,'N;'),('Menu.Admin',0,NULL,NULL,'N;'),('Menu.Create',0,NULL,NULL,'N;'),('Menu.Delete',0,NULL,NULL,'N;'),('Menu.Index',0,NULL,NULL,'N;'),('Menu.Update',0,NULL,NULL,'N;'),('Menu.View',0,NULL,NULL,'N;'),('News.*',1,NULL,NULL,'N;'),('News.Admin',0,NULL,NULL,'N;'),('News.Create',0,NULL,NULL,'N;'),('News.Delete',0,NULL,NULL,'N;'),('News.Index',0,NULL,NULL,'N;'),('News.Search',0,NULL,NULL,'N;'),('News.Translations',0,NULL,NULL,'N;'),('News.Update',0,NULL,NULL,'N;'),('News.View',0,NULL,NULL,'N;'),('NewsSubscription.*',1,NULL,NULL,'N;'),('NewsSubscription.Admin',0,NULL,NULL,'N;'),('NewsSubscription.Create',0,NULL,NULL,'N;'),('NewsSubscription.Delete',0,NULL,NULL,'N;'),('NewsSubscription.Index',0,NULL,NULL,'N;'),('NewsSubscription.Update',0,NULL,NULL,'N;'),('NewsSubscription.View',0,NULL,NULL,'N;'),('Operator',2,'Operatorius',NULL,'N;'),('Pages.*',1,NULL,NULL,'N;'),('Pages.Admin',0,NULL,NULL,'N;'),('Pages.Create',0,NULL,NULL,'N;'),('Pages.Delete',0,NULL,NULL,'N;'),('Pages.Index',0,NULL,NULL,'N;'),('Pages.Update',0,NULL,NULL,'N;'),('Pages.View',0,NULL,NULL,'N;'),('Site.*',1,NULL,NULL,'N;'),('Site.Contact',0,NULL,NULL,'N;'),('Site.Error',0,NULL,NULL,'N;'),('Site.Index',0,NULL,NULL,'N;'),('Site.Login',0,NULL,NULL,'N;'),('Site.Logout',0,NULL,NULL,'N;'),('Student',2,'Besimokantysis',NULL,'N;'),('Tutor',2,'Kuratorius',NULL,'N;'),('User.Activation.*',1,NULL,NULL,'N;'),('User.Activation.Activation',0,NULL,NULL,'N;'),('User.Admin.*',1,NULL,NULL,'N;'),('User.Admin.Admin',0,NULL,NULL,'N;'),('User.Admin.Create',0,NULL,NULL,'N;'),('User.Admin.Delete',0,NULL,NULL,'N;'),('User.Admin.Update',0,NULL,NULL,'N;'),('User.Admin.View',0,NULL,NULL,'N;'),('User.Default.*',1,NULL,NULL,'N;'),('User.Default.Index',0,NULL,NULL,'N;'),('User.Login.*',1,NULL,NULL,'N;'),('User.Login.Login',0,NULL,NULL,'N;'),('User.Logout.*',1,NULL,NULL,'N;'),('User.Logout.Logout',0,NULL,NULL,'N;'),('User.Profile.*',1,NULL,NULL,'N;'),('User.Profile.Changepassword',0,NULL,NULL,'N;'),('User.Profile.Edit',0,NULL,NULL,'N;'),('User.Profile.Profile',0,NULL,NULL,'N;'),('User.ProfileField.*',1,NULL,NULL,'N;'),('User.ProfileField.Admin',0,NULL,NULL,'N;'),('User.ProfileField.Create',0,NULL,NULL,'N;'),('User.ProfileField.Delete',0,NULL,NULL,'N;'),('User.ProfileField.Update',0,NULL,NULL,'N;'),('User.ProfileField.View',0,NULL,NULL,'N;'),('User.Recovery.*',1,NULL,NULL,'N;'),('User.Recovery.Recovery',0,NULL,NULL,'N;'),('User.Registration.*',1,NULL,NULL,'N;'),('User.Registration.Registration',0,NULL,NULL,'N;'),('User.User.*',1,NULL,NULL,'N;'),('User.User.Index',0,NULL,NULL,'N;'),('User.User.View',0,NULL,NULL,'N;'),('UserManager',2,'Naudotojų administratorius',NULL,'N;');
/*!40000 ALTER TABLE `AuthItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AuthItemChild`
--

DROP TABLE IF EXISTS `AuthItemChild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AuthItemChild`
--

LOCK TABLES `AuthItemChild` WRITE;
/*!40000 ALTER TABLE `AuthItemChild` DISABLE KEYS */;
INSERT INTO `AuthItemChild` VALUES ('ContentManager','ClassificatorItems.Admin'),('Guest','ClassificatorItems.Admin'),('Operator','ClassificatorItems.Admin'),('Student','ClassificatorItems.Admin'),('Tutor','ClassificatorItems.Admin'),('UserManager','ClassificatorItems.Admin'),('ContentManager','ClassificatorItems.Create'),('Guest','ClassificatorItems.Create'),('Operator','ClassificatorItems.Create'),('Student','ClassificatorItems.Create'),('Tutor','ClassificatorItems.Create'),('UserManager','ClassificatorItems.Create'),('ContentManager','ClassificatorItems.Delete'),('Guest','ClassificatorItems.Delete'),('Operator','ClassificatorItems.Delete'),('Student','ClassificatorItems.Delete'),('Tutor','ClassificatorItems.Delete'),('UserManager','ClassificatorItems.Delete'),('Guest','ClassificatorItems.Index'),('ContentManager','ClassificatorItems.Update'),('Guest','ClassificatorItems.Update'),('Operator','ClassificatorItems.Update'),('Student','ClassificatorItems.Update'),('Tutor','ClassificatorItems.Update'),('UserManager','ClassificatorItems.Update'),('Guest','ClassificatorItems.View'),('ContentManager','Classificators.Admin'),('Guest','Classificators.Admin'),('Operator','Classificators.Admin'),('Student','Classificators.Admin'),('Tutor','Classificators.Admin'),('UserManager','Classificators.Admin'),('ContentManager','Classificators.Create'),('Guest','Classificators.Create'),('Operator','Classificators.Create'),('Student','Classificators.Create'),('Tutor','Classificators.Create'),('UserManager','Classificators.Create'),('ContentManager','Classificators.Delete'),('Guest','Classificators.Delete'),('Operator','Classificators.Delete'),('Student','Classificators.Delete'),('Tutor','Classificators.Delete'),('UserManager','Classificators.Delete'),('Guest','Classificators.Index'),('ContentManager','Classificators.Update'),('Guest','Classificators.Update'),('Operator','Classificators.Update'),('Student','Classificators.Update'),('Tutor','Classificators.Update'),('UserManager','Classificators.Update'),('Guest','Classificators.View'),('Guest','Courses.Admin'),('Operator','Courses.Admin'),('Tutor','Courses.Admin'),('UserManager','Courses.Admin'),('Guest','Courses.Create'),('Operator','Courses.Create'),('Student','Courses.Create'),('Tutor','Courses.Create'),('UserManager','Courses.Create'),('Guest','Courses.Delete'),('Operator','Courses.Delete'),('Student','Courses.Delete'),('Tutor','Courses.Delete'),('UserManager','Courses.Delete'),('Guest','Courses.Update'),('Operator','Courses.Update'),('Student','Courses.Update'),('Tutor','Courses.Update'),('UserManager','Courses.Update'),('ContentManager','DalykinesSritys.*'),('Guest','DalykinesSritys.*'),('Student','DalykinesSritys.*'),('Tutor','DalykinesSritys.*'),('UserManager','DalykinesSritys.*'),('ContentManager','DalykinesSritys.Admin'),('Guest','DalykinesSritys.Admin'),('Operator','DalykinesSritys.Admin'),('Student','DalykinesSritys.Admin'),('Tutor','DalykinesSritys.Admin'),('UserManager','DalykinesSritys.Admin'),('ContentManager','DalykinesSritys.Create'),('Guest','DalykinesSritys.Create'),('Operator','DalykinesSritys.Create'),('Student','DalykinesSritys.Create'),('Tutor','DalykinesSritys.Create'),('UserManager','DalykinesSritys.Create'),('ContentManager','DalykinesSritys.Delete'),('Guest','DalykinesSritys.Delete'),('Operator','DalykinesSritys.Delete'),('Student','DalykinesSritys.Delete'),('Tutor','DalykinesSritys.Delete'),('UserManager','DalykinesSritys.Delete'),('ContentManager','DalykinesSritys.Update'),('Guest','DalykinesSritys.Update'),('Operator','DalykinesSritys.Update'),('Student','DalykinesSritys.Update'),('UserManager','DalykinesSritys.Update'),('ContentManager','Kompetencijos.*'),('Guest','Kompetencijos.*'),('Student','Kompetencijos.*'),('Tutor','Kompetencijos.*'),('UserManager','Kompetencijos.*'),('ContentManager','Kompetencijos.Admin'),('Guest','Kompetencijos.Admin'),('Operator','Kompetencijos.Admin'),('Student','Kompetencijos.Admin'),('Tutor','Kompetencijos.Admin'),('UserManager','Kompetencijos.Admin'),('ContentManager','Kompetencijos.Create'),('Guest','Kompetencijos.Create'),('Operator','Kompetencijos.Create'),('Student','Kompetencijos.Create'),('Tutor','Kompetencijos.Create'),('UserManager','Kompetencijos.Create'),('ContentManager','Kompetencijos.Delete'),('Guest','Kompetencijos.Delete'),('Operator','Kompetencijos.Delete'),('Student','Kompetencijos.Delete'),('Tutor','Kompetencijos.Delete'),('UserManager','Kompetencijos.Delete'),('ContentManager','Kompetencijos.Update'),('Guest','Kompetencijos.Update'),('Operator','Kompetencijos.Update'),('Student','Kompetencijos.Update'),('Tutor','Kompetencijos.Update'),('UserManager','Kompetencijos.Update'),('ContentManager','Menu.*'),('Guest','Menu.*'),('Operator','Menu.*'),('Student','Menu.*'),('Tutor','Menu.*'),('UserManager','Menu.*'),('ContentManager','Menu.Admin'),('Guest','Menu.Admin'),('Operator','Menu.Admin'),('Student','Menu.Admin'),('Tutor','Menu.Admin'),('UserManager','Menu.Admin'),('ContentManager','Menu.Create'),('Guest','Menu.Create'),('Operator','Menu.Create'),('Student','Menu.Create'),('UserManager','Menu.Create'),('ContentManager','Menu.Delete'),('Guest','Menu.Delete'),('Operator','Menu.Delete'),('Student','Menu.Delete'),('Tutor','Menu.Delete'),('UserManager','Menu.Delete'),('ContentManager','Menu.Index'),('Guest','Menu.Index'),('Operator','Menu.Index'),('Student','Menu.Index'),('Tutor','Menu.Index'),('UserManager','Menu.Index'),('ContentManager','Menu.Update'),('Operator','Menu.Update'),('Student','Menu.Update'),('Tutor','Menu.Update'),('UserManager','Menu.Update'),('ContentManager','Menu.View'),('Guest','Menu.View'),('Operator','Menu.View'),('Student','Menu.View'),('Tutor','Menu.View'),('UserManager','Menu.View'),('Student','News.*'),('Guest','News.Admin'),('Operator','News.Admin'),('Student','News.Admin'),('UserManager','News.Admin'),('Guest','News.Create'),('Operator','News.Create'),('Student','News.Create'),('UserManager','News.Create'),('Guest','News.Delete'),('Operator','News.Delete'),('Student','News.Delete'),('UserManager','News.Delete'),('Guest','News.Translations'),('Operator','News.Translations'),('Student','News.Translations'),('UserManager','News.Translations'),('Guest','News.Update'),('Operator','News.Update'),('Student','News.Update'),('UserManager','News.Update'),('Student','NewsSubscription.*'),('Guest','NewsSubscription.Admin'),('Operator','NewsSubscription.Admin'),('Student','NewsSubscription.Admin'),('UserManager','NewsSubscription.Admin'),('Operator','NewsSubscription.Create'),('Student','NewsSubscription.Create'),('UserManager','NewsSubscription.Create'),('Guest','NewsSubscription.Delete'),('Operator','NewsSubscription.Delete'),('Student','NewsSubscription.Delete'),('UserManager','NewsSubscription.Delete'),('Guest','NewsSubscription.Index'),('Operator','NewsSubscription.Index'),('Student','NewsSubscription.Index'),('UserManager','NewsSubscription.Index'),('Guest','NewsSubscription.Update'),('Operator','NewsSubscription.Update'),('Student','NewsSubscription.Update'),('UserManager','NewsSubscription.Update'),('Guest','NewsSubscription.View'),('Operator','NewsSubscription.View'),('Student','NewsSubscription.View'),('UserManager','NewsSubscription.View'),('ContentManager','Pages.*'),('Guest','Pages.*'),('Operator','Pages.*'),('Student','Pages.*'),('Tutor','Pages.*'),('UserManager','Pages.*'),('ContentManager','Pages.Admin'),('Guest','Pages.Admin'),('Operator','Pages.Admin'),('Student','Pages.Admin'),('Tutor','Pages.Admin'),('UserManager','Pages.Admin'),('ContentManager','Pages.Create'),('Guest','Pages.Create'),('Student','Pages.Create'),('Tutor','Pages.Create'),('ContentManager','Pages.Delete'),('Guest','Pages.Delete'),('Operator','Pages.Delete'),('Student','Pages.Delete'),('Tutor','Pages.Delete'),('UserManager','Pages.Delete'),('ContentManager','Pages.Index'),('Guest','Pages.Index'),('Operator','Pages.Index'),('Student','Pages.Index'),('Tutor','Pages.Index'),('UserManager','Pages.Index'),('ContentManager','Pages.Update'),('Guest','Pages.Update'),('Operator','Pages.Update'),('Student','Pages.Update'),('Tutor','Pages.Update'),('UserManager','Pages.Update'),('ContentManager','Pages.View'),('Guest','Pages.View'),('Operator','Pages.View'),('Student','Pages.View'),('Tutor','Pages.View'),('UserManager','Pages.View'),('ContentManager','Site.*'),('Guest','Site.*'),('Operator','Site.*'),('Student','Site.*'),('Tutor','Site.*'),('UserManager','Site.*'),('ContentManager','User.Activation.*'),('Guest','User.Activation.*'),('Operator','User.Activation.*'),('Student','User.Activation.*'),('Tutor','User.Activation.*'),('UserManager','User.Activation.*'),('ContentManager','User.Admin.*'),('Guest','User.Admin.*'),('Operator','User.Admin.*'),('Student','User.Admin.*'),('Tutor','User.Admin.*'),('UserManager','User.Admin.*'),('ContentManager','User.Admin.Admin'),('Guest','User.Admin.Admin'),('Student','User.Admin.Admin'),('Tutor','User.Admin.Admin'),('ContentManager','User.Admin.Create'),('Guest','User.Admin.Create'),('Student','User.Admin.Create'),('Tutor','User.Admin.Create'),('Guest','User.Admin.Delete'),('Student','User.Admin.Delete'),('Tutor','User.Admin.Delete'),('ContentManager','User.Admin.Update'),('Guest','User.Admin.Update'),('Student','User.Admin.Update'),('Tutor','User.Admin.Update'),('ContentManager','User.Admin.View'),('Guest','User.Admin.View'),('Student','User.Admin.View'),('Tutor','User.Admin.View'),('ContentManager','User.Default.*'),('Guest','User.Default.*'),('Operator','User.Default.*'),('Student','User.Default.*'),('Tutor','User.Default.*'),('UserManager','User.Default.*'),('ContentManager','User.Default.Index'),('Guest','User.Default.Index'),('Student','User.Default.Index'),('Tutor','User.Default.Index'),('ContentManager','User.Login.*'),('Guest','User.Login.*'),('Operator','User.Login.*'),('Student','User.Login.*'),('Tutor','User.Login.*'),('UserManager','User.Login.*'),('Guest','User.Login.Login'),('ContentManager','User.Logout.*'),('Guest','User.Logout.*'),('Operator','User.Logout.*'),('Student','User.Logout.*'),('Tutor','User.Logout.*'),('UserManager','User.Logout.*'),('Guest','User.Logout.Logout'),('ContentManager','User.Profile.*'),('Guest','User.Profile.*'),('Operator','User.Profile.*'),('Student','User.Profile.*'),('Tutor','User.Profile.*'),('UserManager','User.Profile.*'),('Guest','User.Profile.Changepassword'),('Guest','User.Profile.Edit'),('Guest','User.Profile.Profile'),('ContentManager','User.ProfileField.*'),('Guest','User.ProfileField.*'),('Operator','User.ProfileField.*'),('Student','User.ProfileField.*'),('Tutor','User.ProfileField.*'),('UserManager','User.ProfileField.*'),('ContentManager','User.ProfileField.Admin'),('Guest','User.ProfileField.Admin'),('Operator','User.ProfileField.Admin'),('Student','User.ProfileField.Admin'),('Tutor','User.ProfileField.Admin'),('UserManager','User.ProfileField.Admin'),('ContentManager','User.ProfileField.Create'),('Guest','User.ProfileField.Create'),('Operator','User.ProfileField.Create'),('Student','User.ProfileField.Create'),('Tutor','User.ProfileField.Create'),('UserManager','User.ProfileField.Create'),('ContentManager','User.ProfileField.Delete'),('Guest','User.ProfileField.Delete'),('Operator','User.ProfileField.Delete'),('Student','User.ProfileField.Delete'),('Tutor','User.ProfileField.Delete'),('UserManager','User.ProfileField.Delete'),('ContentManager','User.ProfileField.Update'),('Guest','User.ProfileField.Update'),('Operator','User.ProfileField.Update'),('Student','User.ProfileField.Update'),('Tutor','User.ProfileField.Update'),('UserManager','User.ProfileField.Update'),('ContentManager','User.ProfileField.View'),('Guest','User.ProfileField.View'),('Operator','User.ProfileField.View'),('Student','User.ProfileField.View'),('Tutor','User.ProfileField.View'),('UserManager','User.ProfileField.View'),('ContentManager','User.Recovery.*'),('Operator','User.Recovery.*'),('Student','User.Recovery.*'),('Tutor','User.Recovery.*'),('UserManager','User.Recovery.*'),('Guest','User.Recovery.Recovery'),('ContentManager','User.Registration.*'),('Operator','User.Registration.*'),('Student','User.Registration.*'),('Tutor','User.Registration.*'),('UserManager','User.Registration.*'),('Guest','User.Registration.Registration'),('ContentManager','User.User.*'),('Guest','User.User.*'),('Operator','User.User.*'),('Student','User.User.*'),('Tutor','User.User.*'),('UserManager','User.User.*'),('ContentManager','User.User.Index'),('Guest','User.User.Index'),('Student','User.User.Index'),('Tutor','User.User.Index'),('ContentManager','User.User.View'),('Guest','User.User.View'),('Student','User.User.View'),('Tutor','User.User.View');
/*!40000 ALTER TABLE `AuthItemChild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificator_items`
--

DROP TABLE IF EXISTS `classificator_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificator_items` (
  `cid` int(6) NOT NULL COMMENT 'classificator id',
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT 'catalog item id',
  `parent` int(6) NOT NULL COMMENT 'catalog item parent item id',
  `title` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'title in lithuanian',
  `value` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'classificator item predefined value set (to limit input variety)',
  `help` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'help in lithuanian',
  `example` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'example in lithuanian',
  `author` int(6) DEFAULT NULL COMMENT 'who created it',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date when created',
  `version` int(6) NOT NULL COMMENT 'catalog version',
  `language` varchar(2) DEFAULT 'lt' COMMENT 'classificator language',
  `usage` int(6) NOT NULL COMMENT 'how many objects use this catalog item',
  PRIMARY KEY (`id`),
  KEY `classificator` (`cid`),
  CONSTRAINT `classificator` FOREIGN KEY (`cid`) REFERENCES `classificators` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=430 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificator_items`
--

LOCK TABLES `classificator_items` WRITE;
/*!40000 ALTER TABLE `classificator_items` DISABLE KEYS */;
INSERT INTO `classificator_items` VALUES (2,1,-1,'Lietuvių kalba (gimtoji)','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,2,-1,'Lietuvių kalba (valstybinė)','','','',1,'2007-11-26 15:21:10',0,'lt',0),(2,3,-1,'Gimtoji kalba (Rusų)','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,8,-1,'Užsienio kalba (anglų)','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,9,-1,'Užsienio kalba (rusų)','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,17,-1,'Užsienio kalba (vokiečių)','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,18,-1,'Matematika','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,19,-1,'Informatika ir Informacinės technologijos','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,20,-1,'Istorija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,21,-1,'Politologija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,22,-1,'Geografija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,23,-1,'Fizika ir astronomija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,24,-1,'Dailė','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,25,-1,'Muzika','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,26,-1,'Tikyba','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,27,-1,'Etika','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,28,-1,'Kūno kultūra','','','',1,'2007-11-26 15:21:10',0,'lt',0),(2,29,-1,'Pasaulio pažinimas','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,30,-1,'Gamta ir ','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,31,-1,'Pilietinės visuomenės pagrindai','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,32,-1,'Technologijos','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,33,-1,'Biologija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,34,-1,'Chemija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,35,-1,'Civilinė sauga','','','',1,'2007-11-26 15:21:10',0,'lt',0),(2,37,-1,'Pradinio ugdymo programos dalykai','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,39,-1,'Psichologija','','','',1,'2007-11-26 15:21:10',0,'lt',0),(2,42,-1,'Lotynų kalba','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,43,-1,'Ekonomika','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,44,-1,'Pilietinis ugdymas','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,45,-1,'Sveikatos ugdymas','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,46,-1,'Lietuvos istorija','','','',1,'2007-11-26 15:21:11',0,'lt',0),(2,47,-1,'Etnokultūra','','','',1,'2007-11-26 15:21:11',0,'lt',0),(12,321,-1,'Dailė','','','',1,'2007-11-02 11:35:24',0,'lt',0),(12,322,-1,'Biologija','','','',1,'2007-11-02 11:35:23',0,'lt',0),(12,323,-1,'Psichologija','','','',1,'2007-11-02 11:35:55',0,'lt',0),(12,324,-1,'Pilietinės visuomenės pagrindai','','','',1,'2007-11-02 11:36:12',0,'lt',0),(12,325,-1,'Pilietinis ugdymas','','','',1,'2007-11-02 11:36:26',0,'lt',0),(12,326,-1,'Ekonomika','','','',1,'2007-11-02 11:36:43',0,'lt',0),(12,327,-1,'Etika','','','',1,'2007-11-02 11:36:48',0,'lt',0),(12,328,-1,'Geografija','','','',1,'2007-11-02 11:37:01',0,'lt',0),(12,329,-1,'Istorija','','','',1,'2007-11-02 11:37:02',0,'lt',0),(12,330,-1,'Informatika','','','',1,'2007-11-02 11:37:39',0,'lt',0),(12,331,-1,'Lietuvių kalba (gimtoji)','','','',1,'2007-11-02 11:38:06',0,'lt',0),(12,332,-1,'Lietuvių kalba (valstybinė)','','','',1,'2007-11-02 11:38:29',0,'lt',0),(12,333,-1,'Užsienio kalba (anglų)','','','',1,'2007-11-02 11:38:38',0,'lt',0),(12,334,-1,'Užsieino kalba (vokiečių)','','','',1,'2007-11-02 11:38:47',0,'lt',0),(12,335,-1,'Užsienio kalba (lotynų)','','','',1,'2007-11-02 11:38:46',0,'lt',0),(12,336,-1,'Užsienio kalbos','','','',1,'2007-11-02 11:39:06',0,'lt',0),(12,337,-1,'Ispanų kalba','','','',1,'2007-11-02 11:39:15',0,'lt',0),(12,338,-1,'Italų kalba','','','',1,'2007-11-02 11:39:24',0,'lt',0),(12,339,-1,'Užsienio kalba (lenkų)','','','',1,'2007-11-02 11:38:37',0,'lt',0),(12,340,-1,'Norvegų kalba','','','',1,'2007-11-02 11:39:41',0,'lt',0),(12,341,-1,'Užsienio kalba (prancūzų)','','','',1,'2007-11-02 11:38:46',0,'lt',0),(12,342,-1,'Užsienio kalba (rusų)','','','',1,'2007-11-02 11:38:46',0,'lt',0),(12,343,-1,'Matematika','','','',1,'2007-11-02 11:40:07',0,'lt',0),(12,344,-1,'Muzika','','','',1,'2007-11-02 11:40:15',0,'lt',0),(12,345,-1,'Pasaulio pažinimas','','','',1,'2007-11-02 11:40:47',0,'lt',0),(12,346,-1,'Gamta ir ','','','',1,'2007-11-02 11:40:54',0,'lt',0),(12,347,-1,'Filosofija','','','',1,'2007-11-02 11:41:05',0,'lt',0),(12,348,-1,'Kūno kultūra','','','',1,'2007-11-02 11:41:17',0,'lt',0),(12,349,-1,'Fizika','','','',1,'2007-11-02 11:41:28',0,'lt',0),(12,350,-1,'Politiniai mokslai','','','',1,'2007-11-02 11:41:56',0,'lt',0),(12,351,-1,'Tikyba','','','',1,'2007-11-02 11:42:03',0,'lt',0),(12,352,-1,'Religijotyra','','','',1,'2007-11-02 11:42:14',0,'lt',0),(12,353,-1,'Technologijos','','','',1,'2007-11-02 11:42:34',0,'lt',0),(12,354,-1,'Pradinio ugdymo programos dalykai','','','',1,'2007-11-02 11:42:40',0,'lt',0),(12,355,-1,'Sveikatos ugdymas','','','',1,'2007-11-02 11:42:56',0,'lt',0),(12,356,-1,'Etnokultūra','','','',1,'2007-11-02 11:43:06',0,'lt',0),(14,363,-1,'red','','','',1,'2007-11-19 18:22:58',0,'lt',0),(14,364,-1,'yellow','','','',1,'2007-11-19 18:23:24',0,'lt',0),(14,365,364,'dark','','','',1,'2007-11-19 18:23:31',0,'lt',0),(14,366,364,'light','','','',1,'2007-11-19 18:23:39',0,'lt',0),(14,367,-1,'blue','','','',1,'2007-11-19 18:23:51',0,'lt',0),(12,370,-1,'Chemija','','','',1,'2007-11-02 11:35:23',0,'lt',0),(12,371,336,'Ispanų kalba','','','',1,'2013-04-11 02:07:09',0,'lt',0),(15,373,-1,'Sudoku sprendimo','','','',1,'2013-06-20 08:08:08',0,'lt',0),(15,375,-1,'Žaibiško mąstymo','','','',1,'2013-06-20 08:10:05',0,'lt',0),(15,376,-1,'Erelio akies','','','',1,'2013-06-20 08:10:04',0,'lt',0),(15,377,376,'Tėvo akis','','','',1,'2013-06-20 09:06:27',0,'lt',0),(16,378,-1,'Dalykinė sritis  1','','','',1,'2013-06-26 06:05:52',0,'lt',0),(16,379,-1,'Dalykinė sritis 2','','','',1,'2013-06-26 06:06:00',0,'lt',0),(16,380,-1,'Dalykinė sritisi 3','','','',1,'2013-06-26 06:06:13',0,'lt',0),(16,381,-1,'Dalykinė sritis 4','','','',1,'2013-06-26 06:06:28',0,'lt',0),(17,382,-1,'Kauno','','','',1,'2013-07-01 11:07:53',0,'lt',0),(17,383,-1,'Vilniaus','','','',1,'2013-07-01 11:08:43',0,'lt',0),(18,384,-1,'asfsdf','','','',1,'2013-07-11 08:06:02',0,'lt',0),(18,385,-1,'asfsafsadf','','','',1,'2013-07-11 08:06:04',0,'lt',0),(18,386,-1,'asdfsafsdafsdaf','','','',1,'2013-07-11 08:06:06',0,'lt',0),(18,387,-1,'asfasfasdfasdf','','','',1,'2013-07-11 08:06:08',0,'lt',0),(19,390,-1,'Siuvimas','','','',1,'2013-07-24 10:13:18',0,'lt',0),(19,391,-1,'Suvirinimas','','','',1,'2013-07-24 10:13:42',0,'lt',0),(19,392,-1,'Piešimas','','','',1,'2013-07-24 10:14:25',0,'lt',0),(19,393,-1,'Medžio drožimas','','','',1,'2013-07-24 10:14:32',0,'lt',0),(19,394,-1,'Žvejyba','','','',1,'2013-07-24 10:14:40',0,'lt',0),(19,395,-1,'Kalvystė','','','',1,'2013-07-24 10:14:47',0,'lt',0),(19,396,-1,'Labai labai ilga įgyjama kompetencija','','','',1,'2013-07-24 10:15:25',0,'lt',0),(20,397,-1,'Aludarių sąjunga','','','',1,'2013-07-25 07:21:18',0,'lt',0),(20,398,-1,'Bankų sąjunga','','','',1,'2013-07-25 07:21:27',0,'lt',0),(20,399,-1,'Gidų sąjunga','','','',1,'2013-07-25 07:21:43',0,'lt',0),(20,400,-1,'Grūdų augintojų sąjunga','','','',1,'2013-07-25 07:21:56',0,'lt',0),(20,401,-1,'Kauno regiono smulkių ir vidutinių verslininkų sąjunga','','','',1,'2013-07-25 07:22:06',0,'lt',0),(20,402,-1,'LIKS','','','',1,'2013-07-25 07:22:13',0,'lt',0),(20,403,-1,'Literatūros vertėjų sąjunga','','','',1,'2013-07-25 07:22:19',0,'lt',0),(20,404,-1,'Matininkų sąjunga','','','',1,'2013-07-25 07:22:26',0,'lt',0),(20,405,-1,'Marketingo specialistų asociacija','','','',1,'2013-07-25 07:22:31',0,'lt',0),(20,406,-1,'Litnet','','','',1,'2013-07-25 07:22:38',0,'lt',0),(20,407,-1,'Mokslininkų sąjunga','','','',1,'2013-07-25 07:22:45',0,'lt',0),(20,408,-1,'Mokslo taryba','','','',1,'2013-07-25 07:22:50',0,'lt',0),(20,409,-1,'Notarų rūmai','','','',1,'2013-07-25 07:22:57',0,'lt',0),(20,410,-1,'Pramoninkų konfederacija','','','',1,'2013-07-25 07:23:03',0,'lt',0),(20,411,-1,'Savivaldybių sąjunga','','','',1,'2013-07-25 07:23:17',0,'lt',0),(20,412,-1,'Statybos pramonės sąjunga','','','',1,'2013-07-25 07:23:22',0,'lt',0),(20,413,-1,'Teisėjų sąjunga','','','',1,'2013-07-25 07:23:31',0,'lt',0),(20,414,-1,'Turizmo rūmai','','','',1,'2013-07-25 07:23:38',0,'lt',0),(20,415,-1,'Žemės ūkio rūmai','','','',1,'2013-07-25 07:23:46',0,'lt',0),(20,416,-1,'Airijos lietuvių bendruomenė','','','',1,'2013-07-25 07:24:14',0,'lt',0),(20,417,-1,'Britų ir lietuvių draugija','','','',1,'2013-07-25 07:24:20',0,'lt',0),(20,418,-1,'Lietuvių kalbos draugija','','','',1,'2013-07-25 07:24:27',0,'lt',0),(20,419,-1,'Jaunimo karjeros centras','','','',1,'2013-07-25 07:24:33',0,'lt',0),(20,420,-1,'Agapao','','','',1,'2013-07-25 07:24:50',0,'lt',0),(20,421,-1,'Baltų žygiai','','','',1,'2013-07-25 07:24:56',0,'lt',0),(20,422,-1,'Raudonasis kryžius','','','',1,'2013-07-25 07:25:02',0,'lt',0),(20,423,-1,'Žemaičių bendruomenės fondas','','','',1,'2013-07-25 07:25:08',0,'lt',0),(20,424,-1,'Afganistano karo Vilniaus veteranų sąjunga','','','',1,'2013-07-25 07:25:14',0,'lt',0),(20,425,-1,'Baltoji banga ','','','',1,'2013-07-25 07:25:24',0,'lt',0),(20,426,-1,'Černobylis','','','',1,'2013-07-25 07:25:32',0,'lt',0),(20,427,-1,'Gyvūnų globos draugija','','','',1,'2013-07-25 07:25:38',0,'lt',0),(20,428,-1,'Moterų draugija','','','',1,'2013-07-25 07:25:45',0,'lt',0),(20,429,-1,'Žmogaus teisių centras - atskirtis ir įtrauktis','','','',1,'2013-07-25 07:25:54',0,'lt',0);
/*!40000 ALTER TABLE `classificator_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificator_mapping`
--

DROP TABLE IF EXISTS `classificator_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificator_mapping` (
  `cid1` int(6) NOT NULL,
  `id1` int(6) NOT NULL,
  `cid2` int(6) NOT NULL,
  `id2` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='classificator cid1 mapping of fields id1 to classificator ci';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificator_mapping`
--

LOCK TABLES `classificator_mapping` WRITE;
/*!40000 ALTER TABLE `classificator_mapping` DISABLE KEYS */;
INSERT INTO `classificator_mapping` VALUES (13,368,14,363),(13,362,14,363),(13,361,14,363),(13,359,14,363),(13,358,14,363),(13,357,14,363);
/*!40000 ALTER TABLE `classificator_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificators`
--

DROP TABLE IF EXISTS `classificators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificators` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT 'classificator id',
  `title` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'title in lithuanian',
  `searchable` int(1) DEFAULT '0' COMMENT 'is this classificator used for searching?',
  `mapping` tinyint(1) DEFAULT '0' COMMENT 'mapping classificators are used only for XML export (not by default)',
  `object_type_limiter` varchar(254) DEFAULT NULL COMMENT 'This field limits selection of this classificator based on current object type',
  `author` int(6) DEFAULT NULL COMMENT 'who created it',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'date when created',
  `version` int(6) NOT NULL DEFAULT '0' COMMENT 'catalog version',
  `language` char(2) DEFAULT 'lt' COMMENT 'classificator language',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificators`
--

LOCK TABLES `classificators` WRITE;
/*!40000 ALTER TABLE `classificators` DISABLE KEYS */;
INSERT INTO `classificators` VALUES (1,'asdfasdfasdf',1,0,NULL,1,'2013-07-11 09:33:20',0,'lt'),(2,'Lietuvos klasifikatorius',1,0,NULL,1,'2007-11-19 18:22:50',0,'lt'),(12,'Dalykai',1,0,NULL,1,'2007-11-02 11:35:01',0,'lt'),(13,'spalvos',0,0,NULL,1,'2007-11-15 07:51:35',0,'lt'),(14,'colors',0,1,NULL,1,'2007-11-19 18:22:50',0,'lt'),(15,'Kompetencijos',0,0,NULL,1,'2013-06-20 08:05:19',0,'lt'),(16,'Dalykinės sritys',0,0,NULL,1,'2013-06-26 06:05:25',0,'lt'),(17,'Savivaldybes',0,0,NULL,1,'2013-07-01 11:05:38',0,'lt'),(18,'Pabandymui toks',0,0,NULL,1,'2013-07-11 08:05:54',0,'lt'),(19,'Įgyjamos kompetencijos',0,0,NULL,1,'2013-07-24 10:13:03',0,'lt'),(20,'Organizacijos',0,0,NULL,1,'2013-07-25 07:20:30',0,'lt');
/*!40000 ALTER TABLE `classificators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentary`
--

DROP TABLE IF EXISTS `commentary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentary` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `com_text` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `date` date NOT NULL,
  `need_check` int(11) NOT NULL,
  `user_email` varchar(30) NOT NULL,
  `rate` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`com_id`),
  FULLTEXT KEY `com_text` (`com_text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentary`
--

LOCK TABLES `commentary` WRITE;
/*!40000 ALTER TABLE `commentary` DISABLE KEYS */;
/*!40000 ALTER TABLE `commentary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments_bad_word`
--

DROP TABLE IF EXISTS `comments_bad_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments_bad_word` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bad_word` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments_bad_word`
--

LOCK TABLES `comments_bad_word` WRITE;
/*!40000 ALTER TABLE `comments_bad_word` DISABLE KEYS */;
INSERT INTO `comments_bad_word` VALUES (1,'blemba'),(2,'krc'),(3,'karocia'),(4,'lalala'),(5,'blabla'),(8,'lietuvos rytas'),(7,'runkelis'),(9,'labas');
/*!40000 ALTER TABLE `comments_bad_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum`
--

DROP TABLE IF EXISTS `forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forum` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(120) NOT NULL,
  `description` text NOT NULL,
  `listorder` smallint(5) unsigned NOT NULL,
  `is_locked` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_forum_forum` (`parent_id`),
  CONSTRAINT `FK_forum_forum` FOREIGN KEY (`parent_id`) REFERENCES `forum` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum`
--

LOCK TABLES `forum` WRITE;
/*!40000 ALTER TABLE `forum` DISABLE KEYS */;
INSERT INTO `forum` VALUES (1,NULL,'Announcements','Announcements',0,1),(2,1,'New releases','Announcements about new releases',10,0),(3,NULL,'Support','',20,0),(4,3,'Installation and configuration','Problems with installation and/or configuration, incompatibility issues, etc.',10,0),(5,3,'Bugs','Things not working at all, or not as they should',20,0),(6,3,'Missing features','Fetures you think should be included in a future release',30,0);
/*!40000 ALTER TABLE `forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forumuser`
--

DROP TABLE IF EXISTS `forumuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forumuser` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `siteid` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `signature` text,
  `firstseen` int(10) unsigned NOT NULL,
  `lastseen` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `siteid` (`siteid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forumuser`
--

LOCK TABLES `forumuser` WRITE;
/*!40000 ALTER TABLE `forumuser` DISABLE KEYS */;
INSERT INTO `forumuser` VALUES (1,'admin','admin',NULL,0,1377677559),(2,'demo','demo',NULL,0,0);
/*!40000 ALTER TABLE `forumuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lombase_settings`
--

DROP TABLE IF EXISTS `lombase_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lombase_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(30) NOT NULL,
  `isset` int(11) NOT NULL,
  `selected` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lombase_settings`
--

LOCK TABLES `lombase_settings` WRITE;
/*!40000 ALTER TABLE `lombase_settings` DISABLE KEYS */;
INSERT INTO `lombase_settings` VALUES (1,'active_version',1,'LOMAS');
/*!40000 ALTER TABLE `lombase_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta1`
--

DROP TABLE IF EXISTS `meta1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta1` (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name_lt` varchar(254) NOT NULL DEFAULT '',
  `name_en` varchar(254) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `xpath` varchar(254) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `xpath2` varchar(254) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `limits` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `definition_lt` varchar(254) NOT NULL DEFAULT '',
  `value_lt` text NOT NULL,
  `value_en` text NOT NULL,
  `definition2_lt` varchar(254) NOT NULL DEFAULT '',
  `note_lt` varchar(254) NOT NULL DEFAULT '',
  `sample_lt` varchar(254) NOT NULL DEFAULT '',
  `level` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `html_type` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`xpath`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta1`
--

LOCK TABLES `meta1` WRITE;
/*!40000 ALTER TABLE `meta1` DISABLE KEYS */;
INSERT INTO `meta1` VALUES ('Nr.','Lauko pavadinimas (liet.)','x','Kelias','Kelias','Reik?mi? skai?iu','Lauko apibrėžtis','Reikšmė (liet.)','Reikšmė (angl.)','Reikšmių apibrėžtys','Pastaba','Pavyzdys','Būsena','Lauko tipas'),('','LOM','LOM','lom','lom','','','LOM','LOM','','','','','GROUP'),('1','Bendroji dalis','<','lom/general','lom/general','1','Ši grupė apima bendriausią informaciją apie MO.','','','','','','','GROUP'),('1.1','Identifikatorius','Identifier','lom/general/identifier','lom/general/identifier','1..* (10)','Bendras MO vardas ir laikymo vieta drauge.','','','','','','Tik administratoriui','GROUP'),('1.1.1','Katalogas','Catalog','lom/general/identifier/catalog','lom/general/identifier/catalog','1','Įrašo atpažinimo arba katalogo sistemos vardas arba nuoroda.',',,ISBN,,URL',',,ISBN,,URL','Standartas ISO/IEC 10646-1:2000','Metaduomenų kūrėjas šio lauko nemato','http://foo.org/1234','Tik administratoriui','SELECT'),('1.1.2','Įrašas','Entry','lom/general/identifier/entry','lom/general/identifier/entry','1','MO identifikatoriaus arba nuorodos reikšmė jo atpažinimo arba katalogų sistemoje.','','','Standartas ISO/IEC 10646-1:2000','Metaduomenų kūrėjas šio lauko nemato. Įrašoma laisvo teksto eilutė','„DB123456”, „2-7342-0318\", „LEAO875\", „MAT9“','Tik administratoriui','INPUT'),('1.2','Pavadinimas','Title','lom/general/title','lom/general/title','1','Mokymosi objekto pavadinimas.','','','','Įrašomas laisvo teksto eilutė','Spygliuočių miško gyvūnų garsai','Privalomasis','INPUT'),('1.3','Kalba','Language','lom/general/language','lom/general/language','1..* (11)','Pagrindinė kalba arba MO naudojamos kalbos (galima pasirinkti iki 10 kalbų vienam MO).','lt:::Lietuvių,,en:::Anglų,,de:::Vokiečių,,fr:::Prancūzų,,it:::Italų,,es:::Ispanų,,ru:::Rusų,,pl:::Lenkų,,by:::Baltarusių,,dk:::Danų,,no:::Norvegų,,x-none:::nenustatyta','lt:::Lithuanian,,en::: English,,de::: German,,fr::: French,,it::: Italian,,es::: Spanish,,ru::: Russian,,pl::: Polish,,by::: Belorussian,,dk::: Danish,,no::: Norwegian,,x-none:::None','','Pasirenkama iki 11 kalbų iš sąrašo. Pažymimi norimų kalbų žymimieji langeliai. Jei neįmanoma atpažinti kurios nors mokymosi objekto kalbos, tai pasirenkama reikšmė „Kita“','Lietuvių, Anglų','Privalomasis','CHECKBOX'),('1.4','Aprašas','Description','lom/general/description','lom/general/description','1','Mokymosi objekto aprašymas grynuoju tekstu.','','','','Įrašoma laisvu tekstu. Galimas tik vienas aprašymas viena kalba.','Lietuvių, „Skirtingais metų laikais įrašyti spygliuočių miškų gyvūnų garsų įrašai \", Anglų, \"This is a collection of animal sounds recorded in conifer forest at different seasons\"','Privalomasis','TEXTAREA'),('1.5','Reikšminis žodis','Keyword','lom/general/keyword','lom/general/keyword','1','Mokymosi objekto temą nusakantis žodis arba frazė.','','','','Įrašoma laisvu tekstu. Šiame lauke galima įrašyti keletą frazių įvairiomis kalbomis. Frazės viena nuo kitos atskiriamos kableliu','Dinaminė geometrija, geometer\'s sketchpad, brėžinys, sketch','Privalomasis','INPUT'),('1.6','Aprėptis','Coverage','lom/general/coverage','lom/general/coverage','0..1','Laikas, kultūra ar regionas, kuriam priskiriamas MO.','','','','Galima įrašyti iki 10 reikšmių viename lauke atskirtų kableliais. Dažniausiai laukas naudojamas geografijos, istorijos, dailės ir pan. dalykų MO',' Šiaurės pusrutulis','Neprivalomasis','INPUT'),('1.7','Struktūra','Structure','lom/general/structure','lom/general/structure','0..1','Pagrindinė organizacinė MO struktūra.',',,elementarus objektas,,rinkinys,,tinklinis,,hierarchinis,,tiesinis  ',',,atomic,,collection,,networked,,hierarchical,,linear ','elementarus objektas: nedalomas vienas objektas;,,rinkinys: objektų aibė be sąryšių tarp jų;,,tinklinis: objektų rinkinys su neapibrėžtais sąryšiais tarp jų;,,hierarchinis: objektų aibė, kurių objektai susieti medžio struktūra;,,tiesinis: objektų aibė, k','Pasirenkama viena reikšmė iš sąrašo','elementarus: paveikslėlis, rinkinys: tarpusavyje nesusijusių paveikslėlių rinkinys, hierarchinis: tinklalapis','Rekomenduojamasis','SELECT'),('1.8','Agregacijos lygis','Aggregation Level','lom/general/aggregationlevel','lom/general/aggregationLevel','0..1','MO funkcinis vienetas.',',,1,,2,,3,,4',',,1,,2,,3,,4','1: eilutė arba fragmentas;,,2: vienas mokymosi objektas, pvz. pamoka;,,3: du ar keli mokymosi objektų rinkiniai, pvz. kursas;,,4: didžiausias sankaupos lygis, pvz. kursų rinkinys kurio klausytojams išduodamas sertifikatas.','Pasirenkama viena reikšmė iš sąrašo','','Rekomenduojamasis','SELECT'),('2','MO gyvavimo ciklas','Life Cycle','lom/lifecycle','lom/lifeCycle','0..1','Čia aprašoma MO istorija ir dabartinė situacija bei subjektai, kurie įtakoja MO jo kūrimo bei naudojimo metu.','','','','','','','GROUP'),('2.1','Versija','Version','lom/lifecycle/version','lom/lifeCycle/version','0..1','MO laida.','','','','Įrašoma laisvo teksto eilutė','„1.2.alpha”, 4.06','Rekomenduojamasis','INPUT'),('2.2','Būsena','Status','lom/lifecycle/status','lom/lifeCycle/status','1','MO užbaigtumas.',',,juodraštis,,galutinis,,patikslintas,,nepasiekiamas',',,draft,,final,,revised,,unavailable','juodraštis: pateikiamas pirminis, neišbaigtas MO variantas;,,galutinis: pateikiamas išbaigtas MO; patikslintas: pateikiamas patikslintas anksčiau aprašyto MO variantas;,,nepasiekiamas: MO nėra arba nurodytas klaidingas jo adresas','Pasirenkama viena reikšmė iš sąrašo','juodraštis','Privalomasis','SELECT'),('2.3','Talkintojai','Contribute','lom/lifecycle/contribute','lom/lifeCycle/contribute','0..* (30)','Subjektai (pvz., fiziniai arba juridiniai asmenys), kurie susiję su mokymosi objektu gyvavimo laikotarpiu (t. y. kuriant, redaguojant, atliekant sklaidą).','','','','Galima aprašyti iki 30 talkininkų. Talkininkai vertinami labai plačia prasme, apgalvojant visus veiksmus, kurie įtakoja mokymosi objektą. Galima aprašyti iki 30 talkintojų pareigų ir kiekvienai pareigai (vaidmeniui) iki 40 subjektų','','','GROUP'),('2.3.1','Vaidmuo','Role','lom/lifecycle/contribute/role','lom/lifeCycle/contribute/role','1','Subjekto darbo įnašo rūšis.',',,autorius,,leidėjas,,nežinomas,,pradininkas,,salintojas,,vertintojas,,redaktorius,,grafikas,,techninis įgyvendintojas,,turinio teikėjas,,techninis vertintojas,,edukacinis vertintojas,,scenarijaus autorius,,metodininkas',',,author,,publisher,,unknown,,initiator,,terminator,,validator,,editor,,graphical designer,,technical implementer,,content provider,,technical validator,,educational validator,,script writer,,instructional designer','autorius: MO turinio kūrėjas;,,leidėjas:subjektas užsiimantis MO sklaida;,,nežinomas: subjektas, kurios bendradarbiavimo vaidmuo neapibrėžtas;,,pradininkas: asmuo, institucija ar fondas atsakingas už visą plėtros procesą;,,salintojas: asmuo ar subjektas','Pasirenkama viena reikšmė iš sąrašo. Minimaliai turi būti nurodytas mokymosi objekto autorius.','','Privalomasis','SELECT'),('2.3.2','Subjektas','Entity','lom/lifecycle/contribute/entity','lom/lifeCycle/contribute/entity','1..* (40)','Informacija apie subjektus (fizinius asmenis arba juridinius vienetus), susijusius su MO. Subjektai rikiuojami pradedant svarbiausiu.','','','','Įvedama laisvo teksto eilutė. Minimali informacija apie fizinį asmenį vardas ir pavardė, apie juridinį – organizacijos pavadinimas ir tinklapis.','UAB TEV, http://new.tev.lt/','Privalomasis','INPUT'),('2.3.3','Data','Date','lom/lifecycle/contribute/date','lom/lifeCycle/contribute/date','0..1','Lauke 2.3.2 Gyvavimo ciklas. Talkintojai Subjektas įrašyto subjekto atlikto darbo susijusio su MO data.','1990,,1991,,1992,,1993,,1994,,1995,,1996,,1997,,1998,,1999,,2000,,2001,,2002,,2003,,2004,,2005,,2006,,2007,,2008,,2009,,2010,,2011,,2012,,2013,,2014','1990,,1991,,1992,,1993,,1994,,1995,,1996,,1997,,1998,,1999,,2000,,2001,,2002,,2003,,2004,,2005,,2006,,2007','','Įvedama laisvo teksto eilutė. Metus galima nurodyti keturiais skaitmenimis','2006','Rekomenduojamasis','DATE'),('3','Metametaduomenys','Meta-Metadata','lom/meta-metadata','lom/metaMetadata','0..1','Čia aprašomi patys metaduomenų įrašai. Nurodoma, kaip apibrėžiami metaduomenų objektai, kas juos kuria, kaip, kada ir su kokiomis nuorodomis.','','','','Tai nėra informacija, kuri aprašo pačius mokymosi objektus.','','Tik administratoriui','GROUP'),('3.1','Identifikatorius','Identifier','lom/meta-metadata/idetifier','lom/metaMetadata/idetifier','0..* (10)','Bendras metaduomenų įrašo vardas ir vieta.','','','','','','Tik administratoriui','GROUP'),('3.1.1','Katalogas','Catalog','lom/meta-metadata/idetifier/catalog','lom/metaMetadata/idetifier/catalog','1','Įrašo atpažinimo arba katalogo sistemos vardas arba nuoroda.',',,URL,,ISBN',',,URL,,ISBN','','','URL','Tik administratoriui','SELECT'),('3.1.2','Įrašas','Entry','lom/meta-metadata/idetifier/entry','lom/metaMetadata/idetifier/entry','1','Metaduomenų objekto vardo arba nuorodos reikšmė jo atpažinimo arba katalogų sistemoje.','','','','','www.emokykla.lt','Tik administratoriui','INPUT'),('3.2','Talkintojai','Contribute','lom/meta-metadata/contribute','lom/metaMetadata/contribute','0..* (10)','Subjektai (pvz. asmenys, organizacijos), kurie įtakoja metaduomenų būseną jo gyvavimo laikotarpiu (t.y. kūrimas, tikrinimą ir kt.).','','','',' Šis duomenų laukas yra susijęs su darbo įnašu į metaduomenų objekto kūrimą.','','Tik administratoriui','GROUP'),('3.2.1','Vaidmuo','Role','lom/meta-metadata/contribute/role','lom/metaMetadata/contribute/role','1','Subjekto darbo įnašo rūšis.',',,kūrėjas,,vertintojas',',,creator,,validator','Pasirenka iš sąrašo. Metaduomenų objekto kūrėjas (laukų pildytojas)','Turi būti tiksliai vienas duomenų lauko objektas su reikšme „kūrėjas“.','Kūrėjas','Tik administratoriui','SELECT'),('3.2.2','Subjektas','Entity','lom/meta-metadata/contribute/entity','lom/metaMetadata/contribute/entity','1..* (10)','Informacija apie subjektus (asmenis, organizacijas) susijusius su metaduomenimis. Subjektai išrikiuojami pradedant svarbiausiu.','','','Nustatoma automatiškai, užsiregistravus naudotojui','','','Tik administratoriui','INPUT'),('3.2.3','Data','Date','lom/meta-metadata/contribute/date','lom/metaMetadata/contribute/date','0..1','Lauke 3.2.2 Metametaduomenys. Talkintojai. Subjektas įrašyto subjekto atlikto darbo susijusio su metaduomenų objektu data.','1990,,1991,,1992,,1993,,1994,,1995,,1996,,1997,,1998,,1999,,2000,,2001,,2002,,2003,,2004,,2005,,2006,,2007,,2008,,2009,,2010,,2011,,2012,,2013,,2014','1990,,1991,,1992,,1993,,1994,,1995,,1996,,1997,,1998,,1999,,2000,,2001,,2002,,2003,,2004,,2005,,2006,,2007','Nustatoma automatiškai dabartinė (pildymo) data','Metaduomenų kūrimo (dabartinė lauko pildymo arba vertinimo data. Pasirodo automatiškai pildant metametaduomenų laukus','38921','Tik administratoriui','DATE'),('3.3','Metametaduomenų struktūra','Metadata Schema','lom/meta-metadata/metadataschema','lom/metaMetadata/metadataSchema','0..* (10)','Metaduomenų objekto kūrimo instrukcijos pavadinimas ir versija.','','','','Jei pateikiamos sudėtinės reikšmės, tai metaduomenų objektas turi atitikti sudėtinę metaduomenų struktūrą.','„CELEB_MDv1.0”','Tik administratoriui','INPUT'),('3.4','Kalba','Language','lom/meta-metadata/language','lom/metaMetadata/language','0..1','Metaduomenų objekto kalba. Metaduomenų objekte apibrėžta kalba. Jei duomenų lauko reikšmė nepateikiama metaduomenų objekte, tai kalba yra neapibrėžta.',',,lt:::Lietuvių,,en:::Anglų,,de:::Vokiečių,,fr:::Prancūzų,,it:::Italų,,es:::Ispanų,,ru:::Rusų,,pl:::Lenkų,,by:::Baltarusių,,dk:::Danų,,no:::Norvegų',',,lt:::Lithuanian,,en::: English,,de::: German,,fr::: French,,it::: Italian,,es::: Spanish,,ru::: Russian,,pl::: Polish,,by::: Belorussian,,dk::: Danish,,no::: Norwegian','','Pasirenkama iš sąrašo viena kalba. Kalba, kuria pildome metaduomenų laukai. Pasirenkama viena kalba.','Lietuvių','Tik administratoriui','SELECT'),('4','Techninė dalis','Technical','lom/technical','lom/technical','0..1','Šioje grupėje aprašomi techniniai duomenys.','','','','','','','GROUP'),('4.1','Formatas','Format','lom/technical/format','lom/technical/format','1..* (40)','Techniniai MO (visų sudėtinių dalių) duomenų tipai.','formatas JAVA,,formatas msexcel,,formatas msword,,formatas PDF,,formatas PPT,,formatas RTF,,formatas x-compressed,,formatas x-shockwave-flash,,formatas ZIP,,formatas (VRML),,garsas (midi),,garsas (mp3),,garsas (mpeg),,garsas (x-pn-realaudio),,garsas (x-pn-realaudio-plugin),,garsas (x-wav),,paveikslas (BMP),,paveikslas (GIF),,paveikslas (JPEG),,paveikslas (PNG),,paveikslas (TIFF),,paveikslas (x-wmf),,tekstas (HTML),,tekstas (plain),,tekstas (richtext),,tekstas (XML),,vaizdas (mpeg),,vaizdas (x-pn-realvideo),,vaizdas (x-pn-realvideo-plugin),,vaizdas (quicktime),,Kitas','application,,java,,application,,msexcel,,application,,msword,,application,,pdf,,application,,ppt,,application,,rtf,,application,,x-compressed,,application,,x-shockwave-flash,,application,,zip,,application,,VRML,,audio,,midi,,audio,,mp3,,audio,,mpeg,,audio,,x-pn-realaudio,,audio,,x-pn-realaudio-plugin,,audio,,x-wav,,image,,bmp,,image,,gif,,image,,jpeg,,image,,png,,image,,tiff,,image,,x-wmf,,text,,html,,text,,plain,,text,,richtext,,text,,xml,,video,,mpeg,,video,,x-pn-realvideo,,video,,x-pn-realvideo-plugin,,video,,quicktime,,Other','','Pasirenkama iki 40 formatų iš sąrašo: pažymimi norimų formatų žymimieji langeliai. Jei sąraše nėra reikiamo formato, tai pažymimas Kitas žymimasis langelis.','paveikslas (GIF),','Privalomasis','CHECKBOX'),('4.2','Dydis','Size','lom/technical/size','lom/technical/size','1','Skaitmeninio mokymosi objekto dydis kilobaitais.','','','','Įrašoma laisvu tekstu. Galima rašyti tik skaitmenis. Šis duomenų laukas privalo nurodyti tikslų mokymosi objekto dydį kilobaitais. Jei mokymosi objektas yra supakuotas, tai šis duomenų laukas nurodo išpakuoto objekto dydį.','440','Privalomasis','INPUT'),('4.3','Vieta','Location','lom/technical/location','lom/technical/location','1..* (10)','Eilutė naudojama pasiekti mokymosi objektą. Tai gali būti vieta (pvz. universalus ištekliaus adresas).','','','','Nuoroda įrašomas laisvu tekstu arba įkopijuojama. Tai mokymosi objekto aprašomo metaduomenų objekte fizinė vieta. Galima nurodyti iki 10 MO vietų (URL).','http://www.emokykla.lt/admin/file.php?id=107  Aritmetika/','Privalomasis','INPUT'),('4.4','MO techniniai reikalavimai','Requirement','lom/technical/requirement','lom/technical/requirement','0..* (40)','MO naudojimui privalomi techninei reikalavimai. Sudėtiniai reikalavimai. Visa reikalavimų grupė tenkinama, kai tenkinami visi reikalavimai, t. y. jie sujungiami logine jungtimi „ir“.','','','','Galima aprašyti iki 40 reikalavimų.','-','','GROUP'),('4.4.1','Reikalavimai OS arba naršyklei','OrComposite','lom/technical/requirement/orcomposite','lom/technical/requirement/orComposite','0..* (40)','Sudėtiniai reikalavimai. Visa reikalavimų grupė tenkinama, kai tenkinamas bent vienas iš grupės reikalavimų, t. y. jie sujungiami logine jungtimi „arba“.','','','','Galima aprašyti iki 40 reikalavimų.','','','GROUP'),('4.4.1.1','Tipas','Type','lom/technical/requirement/orcomposite/type','lom/technical/requirement/orComposite/type','0..1','MO reikalinga operacinė sistema ir/arba naršyklė.',',,oc:::operacinė sistema,,browser:::naršyklė',',,oc:::operating system,,browser:::browser','','Pasirenkama iš sąrašo','operacinė sistema','Rekomenduojamasis','SELECT'),('4.4.1.2','Pavadinimas','Name','lom/technical/requirement/orcomposite/name','lom/technical/requirement/orComposite/name','0..5','MO reikalingos priemonės pavadinimas.','os:::pcdos:::pc-dos,,os:::mcwin:::mc-windows,,os:::macos:::macus,,os:::unix:::unix,,os:::mltios:::multi-os,,browser:::bk:::bet kokia,,browser:::nc:::ntscape communicat,,browser:::opera:::opera,,browser:::amay:::amay,,browser:::mozila:::mozila','os:::pcdos:::pc-dos,,os:::mcwin:::mc-windows,,os:::macos:::macus,,os:::unix:::unix,,os:::mltios:::multi-os,,browser:::bk:::bet kokia,,browser:::nc:::ntscape communicat,,browser:::opera:::opera,,browser:::amay:::amay,,browser:::mozila:::mozila','','Jei pasirinkta 4.4.1.1 operacinė sistema, tai automatiškai siūloma pasirinkti vieną ar kelias sistemas iš OP sąrašo. .','ms-wondows','Rekomenduojamasis','CONNECTED'),('4.4.1.3','Anksčiausioji versija','Minimum Version','lom/technical/requirement/orcomposite/minimumversion','lom/technical/requirement/orComposite/minimumVersion','0..1','Seniausia (anksčiausiai sukurta)  MO reikalingos priemonės versija.','','','','Įrašoma laisvu tekstu','98','Rekomenduojamasis','INPUT'),('4.4.1.4','Vėliausioji versija','Maximum Version','lom/technical/requirement/orcomposite/maximumversion','lom/technical/requirement/orcomposite/maximumVersion','0..1','Naujausia (vėliausiai sukurta) MO reikalingos priemonės versija.','','','','Įrašoma laisvu tekstu','XP','Rekomenduojamasis','INPUT'),('4.5','Diegimo instrukcija','Installation Remarks','lom/technical/installationremarks','lom/technical/installationRemarks','0..1','MO diegimo instrukcija.','','','','Įrašoma laisvu tekstu','Išpakuokite failą ir paleiskite failą Setup.','Rekomenduojamasis','TEXTAREA'),('4.6','Kiti platformos reikalavimai','Other Platform Requirements','lom/technical/otherplatformrequirements','lom/technical/otherPlatformRequirements','0..1','Informacija apie kitus programinės ir kompiuterinės įrangos reikalavimus.','','','','Įrašoma laisvu tekstu','reikalingas mikrofonas','Neprivalomasis','TEXTAREA'),('4.7','Trukmė','Duration','lom/technical/duration','lom/technical/duration','0..1','MO veikimo trukmė (pvz. filmo rodymo arba garso įrašo trukmė). Trukmė nurodoma minutėmis.','','','','Įrašoma laisvu tekstu laiko trukmė minutėmis. Šis duomenų laukas naudojamas išskirtinai garsiniams, vaizdiniams ir animaciniams duomenims.','45','Rekomenduojamasis','INPUT'),('4.8','Ypatumas','Facet','lom/technical/facet','lom/technical/facet','0..* (15)','Aprašomas mokymosi objekto pako ypatumas.','','','','Aspektas gali būti naudojamas techninių reikalavimų klasifikacijoje. Šis laukas panašus į sąrašo 4.8.1 tąsą.','','Neprivalomasis','GROUP'),('4.8.1','Vardas','Name','lom/technical/facet/name','lom/technical/facet/name','0..2','Mokymosi objekto pako pavadinimas.',',,pako formatas,,IMS,,SCORM 1.2,,SCORM 2004',',,packaged format,,IMS,,SCORM 1.2,,SCORM 2004','','Pasirenkama iš sąrašo','pako formatas','Neprivalomasis','SELECT'),('4.8.2','Reikšmė','Value','lom/technical/facet/value','lom/technical/facet/value','1','Mokymosi objekto pako ypatumas.','pako formatas:::application:::įprastas pakas,,IMS:::encancsd:::sustiprintas,,SCORM 1.2:::encancsd:::sustiprintas,,SCORM 2004:::encancsd:::sustiprintas','pako formatas:::application:::application/zip,,IMS:::encanced:::encanced,,SCORM 1.2:::encanced:::encanced,,SCORM 2004:::encanced:::encanced','','Atomatiškai nustatomas laukas pagal lauko 4.8.1 pasirinkimą. Jei pakoformatas, tai šio lauko reikšmė yra „įprastas pakas“, jei IMS, SCORM, tai šio lauko reikšmė yra „sustiprintas“','sustiprintas','Tik administratoriui','CONNECTED'),('4.8.3','Aprašas','Description','lom/technical/facet/description','lom/technical/facet/description','0..1','Tekstinis mokymosi objekto pako ypatumo aprašymas.','','','','Įrašoma laisvu tekstu. Galimas tik vienas aprašymas viena kalba.','','Neprivalomasis','TEXTAREA'),('6','Teisės','Rights','lom/rights','lom/rights','1','Šioje grupėje aprašomos MO naudojimo teisės.','','','','','','','GROUP'),('6.1','Mokama','Cost','lom/rights/cost','lom/rights/cost','0..1','Ar naudojimasis mokymosi objektu yra mokamas ar ne.',',,taip,,ne',',,yes,,no','','Pasirenkama iš sąrašo','ne','Rekomenduojamasis','SELECT'),('6.2','Autorių teisės ir kiti ribojimai','Copyright and Other Restrictions','lom/rights/copyrightandotherrestrictions','lom/rights/copyrightAndOtherRestrictions','1','Ar mokymosi objektui taikomos autorių teisės ir kiti ribojimai.',',,taip,,ne',',,yes,,no','Jei čia nurodyta, kad apribojimai yra, tai lauke 6.3 Teisinė dalis. Aprašymas turi būti daugiau duomenų apie apribojimus.','Pasirenkama iš sąrašo','taip','Privalomasis','SELECT'),('6.3','Aprašas','Description','lom/rights/description','lom/rights/description','1..3','Pasirenkama, ar MO yra laisvai platinamas, ar jam taikomos kitos teisės. Jei MO taikomos kitos teisės, teksto langelyje pateikiamas jų aprašas (arba nuoroda).',',,Su autorinėmis teisėmis,,Nekomerciniams tikslams,,Be redagavimo teisės,,Bet kokiems tikslams,,Kitos teisės',',,Atribution,,Noncommercial,,No derivative,,Share Alike,,Other rights','su autorinėmis teisėmis: galima kopijuoti, platinti, demonstruoti MO su autoriaus vardu ir pavarde,,nekomerciniams tikslams: MO galima kopijuoti, platinti, demonstruoti tik nekomerciniais tikslais,,be redagavimo teisės: MO galima kopijuoti, platinti, dem','Pasirenkama iki 3 teisių iš sąrašo: pažymimi norimų teisių žymimieji langeliai. Negalima pasirinkti  „betkokiems tikslams“ su „be redagavimo teisės“.','Su autorinėmis teisėmis','Privalomasis','SELECT+A'),('7','Ryšiai','Relation','lom/relation','lom/relation','0..* (100)','Šioje grupėje aprašomi MO ryšiai su kitais MO.','','','','as turi turėti naują sąryšio lauko dalį. Galima aprašyti iki 10 ryšių^','','','GROUP'),('7.1','Ryšio tipas','Kind','lom/relation/kind','lom/relation/kind','0..1','Sąryšio tarp aprašomo ir tikslinio MO tipas, o pats tikslinis MO nurodomas lauke 7.2 Sąryšių dalis. Išteklius.',',,yra dalis,,turi sudedamąją dalį,,yra versija,,turi versiją,,yra formatas,,turi formatą,,nuoroda į,,nuoroda iš,,pagrįstas (kuo),,pagrindas (kam),,reikalauja,,reikalaujamas,,turi peržiūrą,,yra peržiūra,,yra vertimas,,turi vertimą,,turi metaduomenis',',,ispartof,,haspart ,,isversionof ,,hasversion ,,isformatof ,,hasformat ,,references ,,isreferencedby ,,isbasedon ,,isbasisfor ,,requires ,,isrequiredby,,haspreview,,ispreviewof,,istranslationof,,hastranslation,,hasmatedata','yra dalis: aprašomas išteklius yra tikslinio ištekliaus dalis fizine arba logine prasme;,,turi sudedamąją dalį: aprašomas išteklius apima tikslinį išteklių fizine arba logine prasme;,,yra versija: aprašomas išteklius yra tikslinio ištekliaus versija, pap','Pasirenkam iš sąrašo','yra versija','Rekomenduojamasis','SELECT'),('7.2','Išteklius','Resource','lom/relation/resource','lom/relation/resource','0..1','Tikslinis MO, į kurį nurodo šis sąryšis.','','','','','','','GROUP'),('7.2.1','Identifikatorius','Identifier','lom/relation/resource/identifier','lom/relation/resource/identifier','0..* (10)','Bendras tikslinio MO vardas ir laikymo vieta drauge.','','','Galima nurodyti 10 tikslinio MO vietų.','','','','GROUP'),('7.2.1.1','Katalogas','Catalog','lom/relation/resource/identifier/catalog','lom/relation/resource/identifier/catalog','0..1','Įrašo atpažinimo arba katalogo sistemos vardas arba nuoroda.',',,URL,,ISBN',',,URL,,ISBN','','Įrašomas arba įkopijuojamas tikslinio MO URL.','URL','Rekomenduojamasis','SELECT'),('7.2.1.2','Įrašas','Entry','lom/relation/resource/identifier/entry','lom/relation/resource/identifier/entry','0..1','Tikslinio MO identifikatoriaus arba nuorodos reikšmė jo atpažinimo arba katalogų sistemoje.','','','','Įrašomas tikslinio MO URL adresas.','Aritmetika','Rekomenduojamasis','INPUT'),('7.2.2','Aprašas','Description','lom/relation/resource/description','lom/relation/resource/description','0..1','Tikslinio MO aprašymas.','','','',' Įrašoma laisvu tekstu. Vienas aprašymas viena kalba','','Rekomenduojamasis','TEXTAREA'),('9','Klasifikacija','Classification','lom/classification','lom/classification','0..* (40)','Šioje grupėje aprašoma, kurioje vietoje yra MO pagal klasifikaciją.','','','','','','','GROUP'),('9.1','Paskirtis','Purpose','lom/classification/purpose','lom/classification/purpose','1','MO klasifikacijos tikslas.',',,Ugdymo programos',',,Educational programmes ','','Pasirenkame iš sąrašo. Kol kas MO grupuojame tik pagal sritį','sritis','Privalomasis','SELECT'),('9.2','Kelias iki taksono','Taxon Path','lom/classification/taxonpath','lom/classification/taxonPath','0..* (15)','Tam tikros klasifikacinės sistemos taksonominis kelias. Kiekvienas kitas lygis yra ankstesnio lygio praplėtimas.','','','Gali būti skirtingi keliai toje pačioje arba skirtingose klasifikacijose aprašomose tais pačiais požymiais. Šis laukas nenaudojamas, kai 9.1 Klasifikacija. Tikslas yra „dalykas“.','','','','GROUP'),('9.2.1','Šaltinis','Source','lom/classification/taxonpath/source','lom/classification/taxonPath/source','1','Klasifikacijos sistemos pavadinimas.',',,1:::Ikimokyklinis ugdymas,,2:::Bendrasis lavinimas,,3:::Profesinis mokymas,,4:::Aukštosios studijos,,5:::Specialusis ugdymas,,6:::Neformalusis švietimas',',,1:::Preschool education,,2:::General Education,,3:::Vocational training,,4:::Higher Education,,5:::Special Education,,6:::Non-formal education','Šis duomenų laukas gali naudoti bet kurį atpažįstamą taksonominį kelią arba bet kurį naudotojo apibrėžtą taksonominį kelią. Priemonė pateikia nustatytus aukščiausio lygio klasifikacijų įrašus  (LOC, UDC, DDC, t.t.).','','','Privalomasis','SELECT'),('9.2.2','Taksonas','Taxon','lom/classification/taxonpath/taxon','lom/classification/taxonPath/taxon','0..* (15)','Atskiras taksonominiam keliui priklausantis terminas. Taksonas standartizuotai nuorodai taip turi raidinę skaitmeninę žymę arba vardą. Ir žymė ir įrašas gali būti naudojami nusakant tam tikrą taksoną.','','','Išrikiuotas taksonų sąrašas sudaro taksonominį kelią, pvz. „sistematinis laiptatakis“ yra kelias nuo bendriausio iki specifiškiausio įrašo klasifikacijoje.','','','','GROUP'),('9.2.2.1','Id','Id','lom/classification/taxonpath/taxon/id','lom/classification/taxonPath/taxon/id','0..1','Taksono vardas iš skaičių arba raidžių kombinacijos. Jis nusako taksonominio kelio išteklių.','','','ISO/IEC 10646-1:2000','','','Tik administratoriui','INPUT'),('9.2.2.2','Įrašas','Entry','lom/classification/taxonpath/taxon/entry','lom/classification/taxonPath/taxon/entry','1..(10)','Taksono tekstinė žymė.','2:::1:::Lietuvių kalba (gimtoji),,2:::2:::Lietuvių kalba (valstybinė),,2:::3:::Gimtoji kalba (Rusų),,2:::4:::Gimtoji kalba (Lenkų),,2:::5:::Gimtoji kalba (Baltarusų),,2:::6:::Gimtoji kalba (Ukrainų),,2:::7:::Gimtoji kalba (Žydų),,2:::8:::Užsienio kalba (Anglų kalba),,2:::9:::Užsienio kalba (Vokiečių kalba),,2:::10:::Užsienio kalba (Baltarusių kalba),,2:::11:::Užsienio kalba (Danų kalba),,2:::12:::Užsienio kalba (Ispanų kalba),,2:::13:::Užsienio kalba (Italų kalba),,2:::14:::Užsienio kalba (Lenkų kalba),,2:::15:::Užsienio kalba (Norvegų kalba),,2:::16:::Užsienio kalba (Prancūzų kalba),,2:::17:::Užsienio kalba (Rusų kalba),,2:::18:::Matematika,,2:::19:::Informatika ir Informacinės technologijos,,2:::20:::Istorija,,2:::21:::Politologija,,2:::22:::Geografija,,2:::23:::Fizika ir astronomija,,2:::24:::Dailė,,2:::25:::Muzika,,2:::26:::Tikyba,,2:::27:::Etika,,2:::28:::Kūno kultūra,,2:::29:::Pasaulio pažinimas,,2:::30:::Gamta ir žmogus,,2:::31:::Pilietinės visuomenės pagrindai,,2:::32:::Technologijos,,2:::33:::Biologija,,2:::34:::Chemija,,2:::35:::Civilinė sauga,,2:::36:::Saugus eismas,,2:::37:::Pradinio ugdymo programos dalykai,,2:::38:::Filosofija,,2:::39:::Psichologija,,2:::40:::Religijotyra,,2:::41:::Braižyba,,2:::42:::Lotynų kalba,,2:::43:::Ekonomika,,2:::44:::Pilietinis ugdymas,,2:::45:::Sveikatos ugdymas,,2:::46:::Lietuvos istorija,,2:::47:::Etnokultūra','2:::1:::Lithuanian (mother tongue),,2:::2:::Lithuanian (national),,2:::3:::Mother tongue (Russian),,2:::4:::Mother tongue (Polish),,2:::5:::Mother tongue (Belorussian),,2:::6:::Mother tongue (Ukrainian),,2:::7:::Mother tongue (Jewish),,2:::8:::Foreign language (English language),,2:::9:::Foreign language (German language),,2:::10:::Foreign language (Belorussian language),,2:::11:::Foreign language (Danish language),,2:::12:::Foreign language (Spanish language),,2:::13:::Foreign language (Italian language),,2:::14:::Foreign language (Polish language),,2:::15:::Foreign language (Norwegian language),,2:::16:::Foreign language (French language),,2:::17:::Foreign language (Russian language),,2:::18:::Mathematics,,2:::19:::Informatics and information technology ,,2:::20:::History,,2:::21:::Political sience,,2:::22:::Geography,,2:::23:::Physics and astronomy ,,2:::24:::Arts,,2:::25:::Music,,2:::26:::Religion,,2:::27:::Ethics,,2:::28:::Physical education,,2:::29:::World cognition ,,2:::30:::Nature and human,,2:::31:::Civil science basics,,2:::32:::Technology,,2:::33:::Biology,,2:::34:::Chemistry,,2:::35:::Civil defence ,,2:::36:::Secure traffic,,2:::37:::Primary education subjects,,2:::38:::Philosophy ,,2:::39:::Psychology ,,2:::40:::Religion studies ,,2:::41:::Drawing ,,2:::42:::Latin language,,2:::43:::Economy ,,2:::44:::Civil education,,2:::45:::Healtheducation,,2:::46:::Lithanian history,,2:::47:::Ethnoculture','','Pasirenkama iš sąrašo viena reikšmė. Pagal Lietuvos bendrąsias programas ir išsilavinimo standartus. Pasirenkama viena sritis iš sąrašo.','Kalbos','Privalomasis','CONNECTED'),('9.3','Aprašas','Description','lom/classification/description','lom/classification/description','0..1','Tekstinis MO ryšio su nurodyta paskirtimi aprašymas.','','','','Šis laukas nenaudojamas, kai 9.1 Klasifikacija. Tikslas yra „dalykas“ tam kad būtų išvengta painiavos su 1.4 Bendroji dalis. Aprašas. PASTABA: Viena kalba galimas tik vienas aprašymas.','','Tik administratoriui','TEXTAREA'),('9.4','Reikšminis žodis','Keyword','lom/classification/keyword','lom/classification/keyword','0..* (40)','Užrašomi MO ryšio su nurodyta paskirtimi reikšminiai žodžiai arba frazės.','','','','PASTABA:  Kai laukas 9.1 Klasifikacija. Tikslas yra „dalykas“, lauke 1.5 Bendroji dalis. Reikšminis žodis surašomi žodžiai ir frazės laisvu tekstu. Kai 9.1 Klasifikacija. Tikslas yra „dalykas“, tai laukas yra privalomas, o reikšminių žodžių šaltinis yra','','Tik administratoriui','INPUT');
/*!40000 ALTER TABLE `meta1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta1_versions`
--

DROP TABLE IF EXISTS `meta1_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meta1_versions` (
  `id` varchar(32) DEFAULT NULL,
  `xpath` varchar(254) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `extra` varchar(254) DEFAULT NULL,
  `visible` tinyint(2) unsigned zerofill DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta1_versions`
--

LOCK TABLES `meta1_versions` WRITE;
/*!40000 ALTER TABLE `meta1_versions` DISABLE KEYS */;
INSERT INTO `meta1_versions` VALUES ('1.01','lom/classification/purpose',1,NULL,01),('1.02','lom/general/title',1,NULL,01),('1.03','lom/lifecycle/contribute/entity',1,NULL,01),('1.04','lom/general/description',1,NULL,01),('1.05','lom/general/keyword',1,NULL,01),('1.08','lom/classification/taxonpath/taxon/entry',1,'Kompetencijos',01),('1.11','lom/general/coverage',1,NULL,01),('2.02','lom/lifecycle/contribute/role',1,NULL,01),('1.18','lom/lifecycle/status',1,NULL,01),('1.24','lom/lifecycle/version',1,NULL,01),('2','lom/lifecycle',1,NULL,01),('1','lom/general',1,NULL,01),('1.07','lom/classification/taxonpath/taxon/entry',1,'Kompetencijos',01),('1.10','lom/classification/taxonpath/taxon/entry',1,'Lietuvos klasifikatorius',01),('1.09','lom/classification/taxonpath/taxon/entry',1,'Dalykinės sritys',01),('1.12','lom/lifecycle/contribute/role',1,NULL,01),('1.26','lom/lifecycle/contribute/entity',1,NULL,01),('2.01','lom/lifecycle/contribute/entity',1,NULL,01),('1.07','lom/classification/taxonpath/source',1,'Kompetencijos',00),('1.08','lom/classification/taxonpath/source',1,'Kompetencijos',00),('1.09','lom/classification/taxonpath/source',1,'Dalykinės sritys',00),('1.10','lom/classification/taxonpath/source',1,'Lietuvos klasifikatorius',00);
/*!40000 ALTER TABLE `meta1_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata`
--

DROP TABLE IF EXISTS `metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata` (
  `parent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'parent object id',
  `object` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'object id to which this metadata belongs',
  `xpath` varchar(254) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'xpath to this metadata item',
  `item` int(10) unsigned DEFAULT '0' COMMENT 'metadata item number, where multiple occurence allowed',
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'metadata item value',
  `author` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'author user id',
  `language` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT 'lt' COMMENT 'metadata language',
  PRIMARY KEY (`object`,`xpath`,`language`),
  KEY `xpath` (`xpath`(64)),
  KEY `object` (`object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata`
--

LOCK TABLES `metadata` WRITE;
/*!40000 ALTER TABLE `metadata` DISABLE KEYS */;
INSERT INTO `metadata` VALUES (3074,2,'lom/classification/description3081',3081,'',0,'lt'),(3074,2,'lom/classification/keyword3082',3082,'',0,'lt'),(3074,2,'lom/classification/purpose3075',3075,'',0,'lt'),(3076,2,'lom/classification/taxonpath/source3077',3077,'asdfasdfasdf',0,'lt'),(3078,2,'lom/classification/taxonpath/taxon/entry3080',3080,'',0,'lt'),(3078,2,'lom/classification/taxonpath/taxon/id3079',3079,'',0,'lt'),(3076,2,'lom/classification/taxonpath/taxon3078',3078,'',0,'lt'),(3074,2,'lom/classification/taxonpath3076',3076,'',0,'lt'),(0,2,'lom/classification3074',3074,'',0,'lt'),(2924,2,'lom/general/aggregationlevel3038',3038,'',0,'lt'),(2924,2,'lom/general/coverage3036',3036,'',0,'lt'),(2924,2,'lom/general/description3034',3034,'Test524111111',0,'lt'),(2979,2,'lom/general/identifier/catalog2980',2980,'',0,'lt'),(2979,2,'lom/general/identifier/entry2981',2981,'',0,'lt'),(2924,2,'lom/general/identifier2979',2979,'',0,'lt'),(2924,2,'lom/general/keyword3035',3035,'',0,'lt'),(2924,2,'lom/general/language3033_by',3033,'',0,'lt'),(2924,2,'lom/general/language3033_de',3033,'',0,'lt'),(2924,2,'lom/general/language3033_dk',3033,'',0,'lt'),(2924,2,'lom/general/language3033_en',3033,'',0,'lt'),(2924,2,'lom/general/language3033_es',3033,'',0,'lt'),(2924,2,'lom/general/language3033_fr',3033,'on',0,'lt'),(2924,2,'lom/general/language3033_it',3033,'',0,'lt'),(2924,2,'lom/general/language3033_lt',3033,'on',0,'lt'),(2924,2,'lom/general/language3033_no',3033,'',0,'lt'),(2924,2,'lom/general/language3033_pl',3033,'',0,'lt'),(2924,2,'lom/general/language3033_ru',3033,'on',0,'lt'),(2924,2,'lom/general/language3033_x-none',3033,'',0,'lt'),(2924,2,'lom/general/structure3037',3037,'',0,'lt'),(2924,2,'lom/general/title2982',2982,'Test524111111 0000000000',0,'lt'),(0,2,'lom/general2924',2924,'',0,'lt'),(3042,2,'lom/lifecycle/contribute/date3045',3045,'',0,'lt'),(3042,2,'lom/lifecycle/contribute/entity3044',3044,'',0,'lt'),(3042,2,'lom/lifecycle/contribute/role3043',3043,'',0,'lt'),(3039,2,'lom/lifecycle/contribute3042',3042,'',0,'lt'),(3039,2,'lom/lifecycle/status3041',3041,'',0,'lt'),(3039,2,'lom/lifecycle/version3040',3040,'',0,'lt'),(0,2,'lom/lifecycle3039',3039,'',0,'lt'),(3067,2,'lom/relation/kind3068',3068,'',0,'lt'),(3069,2,'lom/relation/resource/description3073',3073,'',0,'lt'),(3070,2,'lom/relation/resource/identifier/catalog3071',3071,'',0,'lt'),(3070,2,'lom/relation/resource/identifier/entry3072',3072,'',0,'lt'),(3069,2,'lom/relation/resource/identifier3070',3070,'',0,'lt'),(3067,2,'lom/relation/resource3069',3069,'',0,'lt'),(0,2,'lom/relation3067',3067,'',0,'lt'),(3063,2,'lom/rights/copyrightandotherrestrictions3065',3065,'',0,'lt'),(3063,2,'lom/rights/cost3064',3064,'',0,'lt'),(3063,2,'lom/rights/description3066',3066,'',0,'lt'),(0,2,'lom/rights3063',3063,'',0,'lt'),(3046,2,'lom/technical/duration3058',3058,'',0,'lt'),(3059,2,'lom/technical/facet/description3062',3062,'',0,'lt'),(3059,2,'lom/technical/facet/name3060',3060,'',0,'lt'),(3059,2,'lom/technical/facet/value3061',3061,'pako formatas:::application:::įprastas pakas',0,'lt'),(3046,2,'lom/technical/facet3059',3059,'',0,'lt'),(3046,2,'lom/technical/format3047_1',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_10',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_11',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_12',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_13',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_14',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_15',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_16',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_17',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_18',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_19',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_2',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_20',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_21',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_22',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_23',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_24',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_25',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_26',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_27',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_28',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_29',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_3',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_30',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_31',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_4',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_5',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_6',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_7',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_8',3047,'',0,'lt'),(3046,2,'lom/technical/format3047_9',3047,'',0,'lt'),(3046,2,'lom/technical/installationremarks3056',3056,'',0,'lt'),(3046,2,'lom/technical/location3049',3049,'',0,'lt'),(3046,2,'lom/technical/otherplatformrequirements3057',3057,'',0,'lt'),(3051,2,'lom/technical/requirement/orcomposite/maximumversion3055',3055,'',0,'lt'),(3051,2,'lom/technical/requirement/orcomposite/minimumversion3054',3054,'',0,'lt'),(3051,2,'lom/technical/requirement/orcomposite/name3053',3053,'os:::pcdos:::pc-dos',0,'lt'),(3051,2,'lom/technical/requirement/orcomposite/type3052',3052,'',0,'lt'),(3050,2,'lom/technical/requirement/orcomposite3051',3051,'',0,'lt'),(3046,2,'lom/technical/requirement3050',3050,'',0,'lt'),(3046,2,'lom/technical/size3048',3048,'',0,'lt'),(0,2,'lom/technical3046',3046,'',0,'lt'),(0,2,'lom1',0,'',0,'lt'),(3915,3,'lom/general/aggregationlevel3925',3925,'',1,'lt'),(3915,3,'lom/general/coverage3923',3923,'',1,'lt'),(3915,3,'lom/general/description3921',3921,'Privalomasis',1,'lt'),(3916,3,'lom/general/identifier/catalog3917',3917,'',1,'lt'),(3916,3,'lom/general/identifier/entry3918',3918,'',1,'lt'),(3915,3,'lom/general/identifier3916',3916,'',1,'lt'),(3915,3,'lom/general/keyword3922',3922,'',1,'lt'),(3915,3,'lom/general/language3920_by',3920,'',1,'lt'),(3915,3,'lom/general/language3920_de',3920,'',1,'lt'),(3915,3,'lom/general/language3920_dk',3920,'',1,'lt'),(3915,3,'lom/general/language3920_en',3920,'',1,'lt'),(3915,3,'lom/general/language3920_es',3920,'',1,'lt'),(3915,3,'lom/general/language3920_fr',3920,'',1,'lt'),(3915,3,'lom/general/language3920_it',3920,'',1,'lt'),(3915,3,'lom/general/language3920_lt',3920,'',1,'lt'),(3915,3,'lom/general/language3920_no',3920,'',1,'lt'),(3915,3,'lom/general/language3920_pl',3920,'',1,'lt'),(3915,3,'lom/general/language3920_ru',3920,'',1,'lt'),(3915,3,'lom/general/language3920_x-none',3920,'',1,'lt'),(3915,3,'lom/general/structure3924',3924,'',1,'lt'),(3915,3,'lom/general/title3919',3919,'objektas id 3',1,'lt'),(0,3,'lom/general3915',3915,'',1,'lt'),(2026,5,'lom/general/aggregationlevel2036',2036,'1',0,'lt'),(2026,5,'lom/general/coverage2034',2034,'',0,'lt'),(2026,5,'lom/general/description2032',2032,'Aprašas123',0,'lt'),(2027,5,'lom/general/identifier/catalog2028',2028,'',0,'lt'),(2027,5,'lom/general/identifier/entry2029',2029,'',0,'lt'),(2026,5,'lom/general/identifier2027',2027,'',0,'lt'),(2026,5,'lom/general/keyword2033',2033,'',0,'lt'),(2026,5,'lom/general/language2031_by',2031,'',0,'lt'),(2026,5,'lom/general/language2031_de',2031,'',0,'lt'),(2026,5,'lom/general/language2031_dk',2031,'',0,'lt'),(2026,5,'lom/general/language2031_en',2031,'',0,'lt'),(2026,5,'lom/general/language2031_es',2031,'',0,'lt'),(2026,5,'lom/general/language2031_fr',2031,'',0,'lt'),(2026,5,'lom/general/language2031_it',2031,'',0,'lt'),(2026,5,'lom/general/language2031_lt',2031,'on',0,'lt'),(2026,5,'lom/general/language2031_no',2031,'',0,'lt'),(2026,5,'lom/general/language2031_pl',2031,'',0,'lt'),(2026,5,'lom/general/language2031_ru',2031,'',0,'lt'),(2026,5,'lom/general/language2031_x-none',2031,'',0,'lt'),(2026,5,'lom/general/structure2035',2035,'',0,'lt'),(2026,5,'lom/general/title2030',2030,'Kursas apie gyvenimą',0,'lt'),(0,5,'lom/general2026',2026,'',0,'lt'),(2710,5,'lom/lifecycle/contribute/date2713',2713,'',0,'lt'),(2710,5,'lom/lifecycle/contribute/entity2712',2712,'',0,'lt'),(2710,5,'lom/lifecycle/contribute/role2711',2711,'',0,'lt'),(2707,5,'lom/lifecycle/contribute2710',2710,'',0,'lt'),(2707,5,'lom/lifecycle/status2709',2709,'galutinis',0,'lt'),(2707,5,'lom/lifecycle/version2708',2708,'',0,'lt'),(0,5,'lom/lifecycle2707',2707,'',0,'lt'),(6524,5,'lom/technical/duration6536',6536,'',0,'lt'),(6537,5,'lom/technical/facet/description6540',6540,'',0,'lt'),(6537,5,'lom/technical/facet/name6538',6538,'',0,'lt'),(6537,5,'lom/technical/facet/value6539',6539,'pako formatas:::application:::įprastas pakas',0,'lt'),(6524,5,'lom/technical/facet6537',6537,'',0,'lt'),(6524,5,'lom/technical/format6525_1',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_10',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_11',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_12',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_13',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_14',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_15',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_16',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_17',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_18',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_19',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_2',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_20',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_21',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_22',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_23',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_24',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_25',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_26',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_27',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_28',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_29',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_3',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_30',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_31',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_4',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_5',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_6',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_7',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_8',6525,'',0,'lt'),(6524,5,'lom/technical/format6525_9',6525,'',0,'lt'),(6524,5,'lom/technical/installationremarks6534',6534,'',0,'lt'),(6524,5,'lom/technical/location6527',6527,'',0,'lt'),(6524,5,'lom/technical/otherplatformrequirements6535',6535,'',0,'lt'),(6529,5,'lom/technical/requirement/orcomposite/maximumversion6533',6533,'',0,'lt'),(6529,5,'lom/technical/requirement/orcomposite/minimumversion6532',6532,'',0,'lt'),(6529,5,'lom/technical/requirement/orcomposite/name6531',6531,'os:::pcdos:::pc-dos',0,'lt'),(6529,5,'lom/technical/requirement/orcomposite/type6530',6530,'',0,'lt'),(6528,5,'lom/technical/requirement/orcomposite6529',6529,'',0,'lt'),(6524,5,'lom/technical/requirement6528',6528,'',0,'lt'),(6524,5,'lom/technical/size6526',6526,'',0,'lt'),(0,5,'lom/technical6524',6524,'',0,'lt'),(0,5,'lom1',0,'',0,'lt'),(6002,6,'lom/annotation/date6004',6004,'',1,'lt'),(6002,6,'lom/annotation/description6005',6005,'Anotacijos aprašymas',1,'lt'),(6002,6,'lom/annotation/entity6003',6003,'',1,'lt'),(0,6,'lom/annotation6002',6002,'',1,'lt'),(6006,6,'lom/classification/description6013',6013,'',1,'lt'),(6006,6,'lom/classification/keyword6014',6014,'',1,'lt'),(6006,6,'lom/classification/purpose6007',6007,'',1,'lt'),(6008,6,'lom/classification/taxonpath/source6009',6009,'Dalykai',1,'lt'),(6010,6,'lom/classification/taxonpath/taxon/entry6012',6012,'',1,'lt'),(6010,6,'lom/classification/taxonpath/taxon/id6011',6011,'',1,'lt'),(6008,6,'lom/classification/taxonpath/taxon6010',6010,'',1,'lt'),(6006,6,'lom/classification/taxonpath6008',6008,'',1,'lt'),(0,6,'lom/classification6006',6006,'',1,'lt'),(5979,6,'lom/educational/context5985_1',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_10',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_11',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_12',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_2',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_3',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_4',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_5',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_6',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_7',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_8',5985,'',1,'lt'),(5979,6,'lom/educational/context5985_9',5985,'',1,'lt'),(5979,6,'lom/educational/description5989',5989,'',1,'lt'),(5979,6,'lom/educational/difficulty5987',5987,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_1',5984,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_2',5984,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_3',5984,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_4',5984,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_5',5984,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_6',5984,'',1,'lt'),(5979,6,'lom/educational/intendedenduserrole5984_7',5984,'',1,'lt'),(5979,6,'lom/educational/interactivitylevel5982',5982,'labai žemas',1,'lt'),(5979,6,'lom/educational/interactivitytype5980',5980,'',1,'lt'),(5979,6,'lom/educational/language5990_by',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_de',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_dk',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_en',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_es',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_fr',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_it',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_lt',5990,'on',1,'lt'),(5979,6,'lom/educational/language5990_no',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_pl',5990,'',1,'lt'),(5979,6,'lom/educational/language5990_ru',5990,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_1',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_10',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_11',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_12',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_13',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_14',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_15',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_16',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_17',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_18',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_19',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_2',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_20',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_3',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_4',5981,'on',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_5',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_6',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_7',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_8',5981,'',1,'lt'),(5979,6,'lom/educational/learningresourcetype5981_9',5981,'',1,'lt'),(5979,6,'lom/educational/semanticdensity5983',5983,'labai žemas',1,'lt'),(5979,6,'lom/educational/typicalagerange5986',5986,'',1,'lt'),(5979,6,'lom/educational/typicallearningtime5988',5988,'',1,'lt'),(0,6,'lom/educational5979',5979,'',1,'lt'),(5944,6,'lom/general/aggregationlevel5954',5954,'1',1,'lt'),(5944,6,'lom/general/coverage5952',5952,'testas',1,'lt'),(5944,6,'lom/general/description5950',5950,'Aprašymas',1,'lt'),(5945,6,'lom/general/identifier/catalog5946',5946,'',1,'lt'),(5945,6,'lom/general/identifier/entry5947',5947,'',1,'lt'),(5944,6,'lom/general/identifier5945',5945,'',1,'lt'),(5944,6,'lom/general/keyword5951',5951,'testas',1,'lt'),(5944,6,'lom/general/language5949_by',5949,'',1,'lt'),(5944,6,'lom/general/language5949_de',5949,'',1,'lt'),(5944,6,'lom/general/language5949_dk',5949,'',1,'lt'),(5944,6,'lom/general/language5949_en',5949,'',1,'lt'),(5944,6,'lom/general/language5949_es',5949,'',1,'lt'),(5944,6,'lom/general/language5949_fr',5949,'',1,'lt'),(5944,6,'lom/general/language5949_it',5949,'',1,'lt'),(5944,6,'lom/general/language5949_lt',5949,'on',1,'lt'),(5944,6,'lom/general/language5949_no',5949,'',1,'lt'),(5944,6,'lom/general/language5949_pl',5949,'',1,'lt'),(5944,6,'lom/general/language5949_ru',5949,'',1,'lt'),(5944,6,'lom/general/language5949_x-none',5949,'',1,'lt'),(5944,6,'lom/general/structure5953',5953,'rinkinys',1,'lt'),(5944,6,'lom/general/title5948',5948,'Naujas testinis mokymo objektas',1,'lt'),(0,6,'lom/general5944',5944,'',1,'lt'),(5958,6,'lom/lifecycle/contribute/date5961',5961,'',1,'lt'),(5958,6,'lom/lifecycle/contribute/entity5960',5960,'',1,'lt'),(5958,6,'lom/lifecycle/contribute/role5959',5959,'',1,'lt'),(5955,6,'lom/lifecycle/contribute5958',5958,'',1,'lt'),(5955,6,'lom/lifecycle/status5957',5957,'juodraštis',1,'lt'),(5955,6,'lom/lifecycle/version5956',5956,'1',1,'lt'),(0,6,'lom/lifecycle5955',5955,'',1,'lt'),(5995,6,'lom/relation/kind5996',5996,'',1,'lt'),(5997,6,'lom/relation/resource/description6001',6001,'',1,'lt'),(5998,6,'lom/relation/resource/identifier/catalog5999',5999,'',1,'lt'),(5998,6,'lom/relation/resource/identifier/entry6000',6000,'',1,'lt'),(5997,6,'lom/relation/resource/identifier5998',5998,'',1,'lt'),(5995,6,'lom/relation/resource5997',5997,'',1,'lt'),(0,6,'lom/relation5995',5995,'',1,'lt'),(5991,6,'lom/rights/copyrightandotherrestrictions5993',5993,'',1,'lt'),(5991,6,'lom/rights/cost5992',5992,'ne',1,'lt'),(5991,6,'lom/rights/description5994',5994,'',1,'lt'),(0,6,'lom/rights5991',5991,'',1,'lt'),(5962,6,'lom/technical/duration5974',5974,'',1,'lt'),(5975,6,'lom/technical/facet/description5978',5978,'',1,'lt'),(5975,6,'lom/technical/facet/name5976',5976,'',1,'lt'),(5975,6,'lom/technical/facet/value5977',5977,'pako formatas:::application:::įprastas pakas',1,'lt'),(5962,6,'lom/technical/facet5975',5975,'',1,'lt'),(5962,6,'lom/technical/format5963_1',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_10',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_11',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_12',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_13',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_14',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_15',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_16',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_17',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_18',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_19',5963,'on',1,'lt'),(5962,6,'lom/technical/format5963_2',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_20',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_21',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_22',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_23',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_24',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_25',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_26',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_27',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_28',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_29',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_3',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_30',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_31',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_4',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_5',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_6',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_7',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_8',5963,'',1,'lt'),(5962,6,'lom/technical/format5963_9',5963,'',1,'lt'),(5962,6,'lom/technical/installationremarks5972',5972,'',1,'lt'),(5962,6,'lom/technical/location5965',5965,'',1,'lt'),(5962,6,'lom/technical/otherplatformrequirements5973',5973,'',1,'lt'),(5967,6,'lom/technical/requirement/orcomposite/maximumversion5971',5971,'',1,'lt'),(5967,6,'lom/technical/requirement/orcomposite/minimumversion5970',5970,'',1,'lt'),(5967,6,'lom/technical/requirement/orcomposite/name5969',5969,'os:::pcdos:::pc-dos',1,'lt'),(5967,6,'lom/technical/requirement/orcomposite/type5968',5968,'',1,'lt'),(5966,6,'lom/technical/requirement/orcomposite5967',5967,'',1,'lt'),(5962,6,'lom/technical/requirement5966',5966,'',1,'lt'),(5962,6,'lom/technical/size5964',5964,'1000',1,'lt'),(0,6,'lom/technical5962',5962,'',1,'lt'),(6155,7,'lom/annotation/date6157',6157,'2006',43,'lt'),(6155,7,'lom/annotation/description6158',6158,'',43,'lt'),(6155,7,'lom/annotation/entity6156',6156,'',43,'lt'),(0,7,'lom/annotation6155',6155,'',43,'lt'),(6132,7,'lom/educational/context6138_1',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_10',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_11',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_12',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_2',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_3',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_4',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_5',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_6',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_7',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_8',6138,'',43,'lt'),(6132,7,'lom/educational/context6138_9',6138,'',43,'lt'),(6132,7,'lom/educational/description6142',6142,'',43,'lt'),(6132,7,'lom/educational/difficulty6140',6140,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_1',6137,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_2',6137,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_3',6137,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_4',6137,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_5',6137,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_6',6137,'',43,'lt'),(6132,7,'lom/educational/intendedenduserrole6137_7',6137,'',43,'lt'),(6132,7,'lom/educational/interactivitylevel6135',6135,'',43,'lt'),(6132,7,'lom/educational/interactivitytype6133',6133,'',43,'lt'),(6132,7,'lom/educational/language6143_by',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_de',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_dk',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_en',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_es',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_fr',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_it',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_lt',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_no',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_pl',6143,'',43,'lt'),(6132,7,'lom/educational/language6143_ru',6143,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_1',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_10',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_11',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_12',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_13',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_14',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_15',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_16',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_17',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_18',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_19',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_2',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_20',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_3',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_4',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_5',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_6',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_7',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_8',6134,'',43,'lt'),(6132,7,'lom/educational/learningresourcetype6134_9',6134,'',43,'lt'),(6132,7,'lom/educational/semanticdensity6136',6136,'',43,'lt'),(6132,7,'lom/educational/typicalagerange6139',6139,'',43,'lt'),(6132,7,'lom/educational/typicallearningtime6141',6141,'',43,'lt'),(0,7,'lom/educational6132',6132,'',43,'lt'),(6097,7,'lom/general/aggregationlevel6107',6107,'1',43,'lt'),(6097,7,'lom/general/coverage6105',6105,'test',43,'lt'),(6097,7,'lom/general/description6103',6103,'Aprašymas',43,'lt'),(6098,7,'lom/general/identifier/catalog6099',6099,'ISBN',43,'lt'),(6098,7,'lom/general/identifier/entry6100',6100,'',43,'lt'),(6097,7,'lom/general/identifier6098',6098,'',43,'lt'),(6097,7,'lom/general/keyword6104',6104,'test',43,'lt'),(6097,7,'lom/general/language6102_by',6102,'',43,'lt'),(6097,7,'lom/general/language6102_de',6102,'',43,'lt'),(6097,7,'lom/general/language6102_dk',6102,'',43,'lt'),(6097,7,'lom/general/language6102_en',6102,'',43,'lt'),(6097,7,'lom/general/language6102_es',6102,'',43,'lt'),(6097,7,'lom/general/language6102_fr',6102,'',43,'lt'),(6097,7,'lom/general/language6102_it',6102,'',43,'lt'),(6097,7,'lom/general/language6102_lt',6102,'on',43,'lt'),(6097,7,'lom/general/language6102_no',6102,'',43,'lt'),(6097,7,'lom/general/language6102_pl',6102,'',43,'lt'),(6097,7,'lom/general/language6102_ru',6102,'',43,'lt'),(6097,7,'lom/general/language6102_x-none',6102,'',43,'lt'),(6097,7,'lom/general/structure6106',6106,'elementarus objektas',43,'lt'),(6097,7,'lom/general/title6101',6101,'Testinis MO',43,'lt'),(0,7,'lom/general6097',6097,'',43,'lt'),(6111,7,'lom/lifecycle/contribute/date6114',6114,'2013',43,'lt'),(6111,7,'lom/lifecycle/contribute/entity6113',6113,'',43,'lt'),(6111,7,'lom/lifecycle/contribute/role6112',6112,'',43,'lt'),(6108,7,'lom/lifecycle/contribute6111',6111,'',43,'lt'),(6108,7,'lom/lifecycle/status6110',6110,'galutinis',43,'lt'),(6108,7,'lom/lifecycle/version6109',6109,'',43,'lt'),(0,7,'lom/lifecycle6108',6108,'',43,'lt'),(6148,7,'lom/relation/kind6149',6149,'nuoroda į',43,'lt'),(6150,7,'lom/relation/resource/description6154',6154,'',43,'lt'),(4515,7,'lom/relation/resource/identifier/catalog4516',4516,'',43,'lt'),(6151,7,'lom/relation/resource/identifier/catalog6152',6152,'',43,'lt'),(4515,7,'lom/relation/resource/identifier/entry4517',4517,'',43,'lt'),(6151,7,'lom/relation/resource/identifier/entry6153',6153,'',43,'lt'),(6150,7,'lom/relation/resource/identifier4515',4515,'',43,'lt'),(6150,7,'lom/relation/resource/identifier6151',6151,'',43,'lt'),(6148,7,'lom/relation/resource6150',6150,'',43,'lt'),(0,7,'lom/relation6148',6148,'',43,'lt'),(6144,7,'lom/rights/copyrightandotherrestrictions6146',6146,'',43,'lt'),(6144,7,'lom/rights/cost6145',6145,'ne',43,'lt'),(6144,7,'lom/rights/description4514',4514,'',43,'lt'),(6144,7,'lom/rights/description6147',6147,'',43,'lt'),(0,7,'lom/rights6144',6144,'',43,'lt'),(6115,7,'lom/technical/duration6127',6127,'',43,'lt'),(6128,7,'lom/technical/facet/description6131',6131,'',43,'lt'),(6128,7,'lom/technical/facet/name6129',6129,'',43,'lt'),(6128,7,'lom/technical/facet/value6130',6130,'pako formatas:::application:::įprastas pakas',43,'lt'),(6115,7,'lom/technical/facet6128',6128,'',43,'lt'),(6115,7,'lom/technical/format6116_1',6116,'on',43,'lt'),(6115,7,'lom/technical/format6116_10',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_11',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_12',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_13',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_14',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_15',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_16',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_17',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_18',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_19',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_2',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_20',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_21',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_22',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_23',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_24',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_25',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_26',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_27',6116,'on',43,'lt'),(6115,7,'lom/technical/format6116_28',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_29',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_3',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_30',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_31',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_4',6116,'on',43,'lt'),(6115,7,'lom/technical/format6116_5',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_6',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_7',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_8',6116,'',43,'lt'),(6115,7,'lom/technical/format6116_9',6116,'',43,'lt'),(6115,7,'lom/technical/installationremarks6125',6125,'',43,'lt'),(6115,7,'lom/technical/location6118',6118,'',43,'lt'),(6115,7,'lom/technical/otherplatformrequirements6126',6126,'',43,'lt'),(6120,7,'lom/technical/requirement/orcomposite/maximumversion6124',6124,'',43,'lt'),(6120,7,'lom/technical/requirement/orcomposite/minimumversion6123',6123,'',43,'lt'),(6120,7,'lom/technical/requirement/orcomposite/name6122',6122,'os:::pcdos:::pc-dos',43,'lt'),(6120,7,'lom/technical/requirement/orcomposite/type6121',6121,'',43,'lt'),(6119,7,'lom/technical/requirement/orcomposite6120',6120,'',43,'lt'),(6115,7,'lom/technical/requirement6119',6119,'',43,'lt'),(6115,7,'lom/technical/size6117',6117,'',43,'lt'),(0,7,'lom/technical6115',6115,'',43,'lt'),(6802,10,'lom/classification/description6809',6809,'',43,'lt'),(6802,10,'lom/classification/keyword6810',6810,'',43,'lt'),(6802,10,'lom/classification/purpose6803',6803,'',43,'lt'),(6804,10,'lom/classification/taxonpath/source6805',6805,'spalvos',43,'lt'),(6806,10,'lom/classification/taxonpath/taxon/entry6808',6808,'',43,'lt'),(6806,10,'lom/classification/taxonpath/taxon/id6807',6807,'',43,'lt'),(6804,10,'lom/classification/taxonpath/taxon6806',6806,'',43,'lt'),(6802,10,'lom/classification/taxonpath6804',6804,'',43,'lt'),(0,10,'lom/classification6802',6802,'',43,'lt'),(5581,10,'lom/general/aggregationlevel5591',5591,'',43,'lt'),(5581,10,'lom/general/coverage5589',5589,'',43,'lt'),(5581,10,'lom/general/description5587',5587,'',43,'lt'),(5582,10,'lom/general/identifier/catalog5583',5583,'',43,'lt'),(5582,10,'lom/general/identifier/entry5584',5584,'',43,'lt'),(5581,10,'lom/general/identifier5582',5582,'',43,'lt'),(5581,10,'lom/general/keyword5588',5588,'abrakadabra123',43,'lt'),(5581,10,'lom/general/language5586_by',5586,'',43,'lt'),(5581,10,'lom/general/language5586_de',5586,'',43,'lt'),(5581,10,'lom/general/language5586_dk',5586,'',43,'lt'),(5581,10,'lom/general/language5586_en',5586,'',43,'lt'),(5581,10,'lom/general/language5586_es',5586,'',43,'lt'),(5581,10,'lom/general/language5586_fr',5586,'',43,'lt'),(5581,10,'lom/general/language5586_it',5586,'',43,'lt'),(5581,10,'lom/general/language5586_lt',5586,'on',43,'lt'),(5581,10,'lom/general/language5586_no',5586,'',43,'lt'),(5581,10,'lom/general/language5586_pl',5586,'',43,'lt'),(5581,10,'lom/general/language5586_ru',5586,'',43,'lt'),(5581,10,'lom/general/language5586_x-none',5586,'',43,'lt'),(5581,10,'lom/general/structure5590',5590,'',43,'lt'),(5581,10,'lom/general/title5585',5585,'Test MO 3',43,'lt'),(0,10,'lom/general5581',5581,'',43,'lt'),(6504,10,'lom/lifecycle/contribute/date6507',6507,'',43,'lt'),(6798,10,'lom/lifecycle/contribute/date6801',6801,'',43,'lt'),(6504,10,'lom/lifecycle/contribute/entity6506',6506,'',43,'lt'),(6798,10,'lom/lifecycle/contribute/entity6800',6800,'',43,'lt'),(6504,10,'lom/lifecycle/contribute/role6505',6505,'',43,'lt'),(6798,10,'lom/lifecycle/contribute/role6799',6799,'',43,'lt'),(6501,10,'lom/lifecycle/contribute6504',6504,'',43,'lt'),(6501,10,'lom/lifecycle/contribute6798',6798,'',43,'lt'),(6501,10,'lom/lifecycle/status6503',6503,'galutinis',43,'lt'),(6501,10,'lom/lifecycle/version6502',6502,'',43,'lt'),(0,10,'lom/lifecycle6501',6501,'',43,'lt'),(5756,11,'lom/educational/context5762_1',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_10',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_11',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_12',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_2',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_3',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_4',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_5',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_6',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_7',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_8',5762,'',43,'lt'),(5756,11,'lom/educational/context5762_9',5762,'',43,'lt'),(5756,11,'lom/educational/description5766',5766,'',43,'lt'),(5756,11,'lom/educational/difficulty5764',5764,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_1',5761,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_2',5761,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_3',5761,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_4',5761,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_5',5761,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_6',5761,'',43,'lt'),(5756,11,'lom/educational/intendedenduserrole5761_7',5761,'',43,'lt'),(5756,11,'lom/educational/interactivitylevel5759',5759,'labai žemas',43,'lt'),(5756,11,'lom/educational/interactivitytype5757',5757,'aiškinamasis',43,'lt'),(5756,11,'lom/educational/language5767_by',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_de',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_dk',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_en',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_es',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_fr',5767,'on',43,'lt'),(5756,11,'lom/educational/language5767_it',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_lt',5767,'on',43,'lt'),(5756,11,'lom/educational/language5767_no',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_pl',5767,'',43,'lt'),(5756,11,'lom/educational/language5767_ru',5767,'on',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_1',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_10',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_11',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_12',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_13',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_14',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_15',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_16',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_17',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_18',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_19',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_2',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_20',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_3',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_4',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_5',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_6',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_7',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_8',5758,'',43,'lt'),(5756,11,'lom/educational/learningresourcetype5758_9',5758,'',43,'lt'),(5756,11,'lom/educational/semanticdensity5760',5760,'aukštas',43,'lt'),(5756,11,'lom/educational/typicalagerange5763',5763,'',43,'lt'),(5756,11,'lom/educational/typicallearningtime5765',5765,'',43,'lt'),(0,11,'lom/educational5756',5756,'',43,'lt'),(5721,11,'lom/general/aggregationlevel5731',5731,'',43,'lt'),(5721,11,'lom/general/coverage5729',5729,'',43,'lt'),(5721,11,'lom/general/description5727',5727,'aprašas',43,'lt'),(5722,11,'lom/general/identifier/catalog5723',5723,'',43,'lt'),(5722,11,'lom/general/identifier/entry5724',5724,'',43,'lt'),(5721,11,'lom/general/identifier5722',5722,'',43,'lt'),(5721,11,'lom/general/keyword5728',5728,'test',43,'lt'),(5721,11,'lom/general/language5726_by',5726,'',43,'lt'),(5721,11,'lom/general/language5726_de',5726,'',43,'lt'),(5721,11,'lom/general/language5726_dk',5726,'',43,'lt'),(5721,11,'lom/general/language5726_en',5726,'',43,'lt'),(5721,11,'lom/general/language5726_es',5726,'',43,'lt'),(5721,11,'lom/general/language5726_fr',5726,'',43,'lt'),(5721,11,'lom/general/language5726_it',5726,'',43,'lt'),(5721,11,'lom/general/language5726_lt',5726,'on',43,'lt'),(5721,11,'lom/general/language5726_no',5726,'',43,'lt'),(5721,11,'lom/general/language5726_pl',5726,'',43,'lt'),(5721,11,'lom/general/language5726_ru',5726,'',43,'lt'),(5721,11,'lom/general/language5726_x-none',5726,'',43,'lt'),(5721,11,'lom/general/structure5730',5730,'elementarus objektas',43,'lt'),(5721,11,'lom/general/title5725',5725,'Testinis MO 4',43,'lt'),(0,11,'lom/general5721',5721,'',43,'lt'),(5735,11,'lom/lifecycle/contribute/date5738',5738,'',43,'lt'),(5735,11,'lom/lifecycle/contribute/entity5737',5737,'',43,'lt'),(5735,11,'lom/lifecycle/contribute/role5736',5736,'',43,'lt'),(5732,11,'lom/lifecycle/contribute5735',5735,'',43,'lt'),(5732,11,'lom/lifecycle/status5734',5734,'galutinis',43,'lt'),(5732,11,'lom/lifecycle/version5733',5733,'',43,'lt'),(0,11,'lom/lifecycle5732',5732,'',43,'lt'),(5739,11,'lom/technical/duration5751',5751,'',43,'lt'),(5752,11,'lom/technical/facet/description5755',5755,'',43,'lt'),(5752,11,'lom/technical/facet/name5753',5753,'',43,'lt'),(5752,11,'lom/technical/facet/value5754',5754,'pako formatas:::application:::įprastas pakas',43,'lt'),(5739,11,'lom/technical/facet5752',5752,'',43,'lt'),(5739,11,'lom/technical/format5740_1',5740,'on',43,'lt'),(5739,11,'lom/technical/format5740_10',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_11',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_12',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_13',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_14',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_15',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_16',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_17',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_18',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_19',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_2',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_20',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_21',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_22',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_23',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_24',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_25',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_26',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_27',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_28',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_29',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_3',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_30',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_31',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_4',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_5',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_6',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_7',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_8',5740,'',43,'lt'),(5739,11,'lom/technical/format5740_9',5740,'',43,'lt'),(5739,11,'lom/technical/installationremarks5749',5749,'',43,'lt'),(5739,11,'lom/technical/location5742',5742,'',43,'lt'),(5739,11,'lom/technical/otherplatformrequirements5750',5750,'',43,'lt'),(5744,11,'lom/technical/requirement/orcomposite/maximumversion5748',5748,'',43,'lt'),(5744,11,'lom/technical/requirement/orcomposite/minimumversion5747',5747,'',43,'lt'),(5744,11,'lom/technical/requirement/orcomposite/name5746',5746,'os:::pcdos:::pc-dos',43,'lt'),(5744,11,'lom/technical/requirement/orcomposite/type5745',5745,'',43,'lt'),(5743,11,'lom/technical/requirement/orcomposite5744',5744,'',43,'lt'),(5739,11,'lom/technical/requirement5743',5743,'',43,'lt'),(5739,11,'lom/technical/size5741',5741,'30987',43,'lt'),(0,11,'lom/technical5739',5739,'',43,'lt'),(6126,13,'lom/educational/context6132_1',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_10',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_11',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_12',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_2',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_3',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_4',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_5',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_6',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_7',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_8',6132,'',43,'lt'),(6126,13,'lom/educational/context6132_9',6132,'',43,'lt'),(6126,13,'lom/educational/description6136',6136,'',43,'lt'),(6126,13,'lom/educational/difficulty6134',6134,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_1',6131,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_2',6131,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_3',6131,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_4',6131,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_5',6131,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_6',6131,'',43,'lt'),(6126,13,'lom/educational/intendedenduserrole6131_7',6131,'',43,'lt'),(6126,13,'lom/educational/interactivitylevel6129',6129,'',43,'lt'),(6126,13,'lom/educational/interactivitytype6127',6127,'',43,'lt'),(6126,13,'lom/educational/language6137_by',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_de',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_dk',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_en',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_es',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_fr',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_it',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_lt',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_no',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_pl',6137,'',43,'lt'),(6126,13,'lom/educational/language6137_ru',6137,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_1',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_10',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_11',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_12',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_13',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_14',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_15',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_16',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_17',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_18',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_19',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_2',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_20',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_3',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_4',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_5',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_6',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_7',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_8',6128,'',43,'lt'),(6126,13,'lom/educational/learningresourcetype6128_9',6128,'',43,'lt'),(6126,13,'lom/educational/semanticdensity6130',6130,'',43,'lt'),(6126,13,'lom/educational/typicalagerange6133',6133,'',43,'lt'),(6126,13,'lom/educational/typicallearningtime6135',6135,'',43,'lt'),(0,13,'lom/educational6126',6126,'',43,'lt'),(6087,13,'lom/general/aggregationlevel6097',6097,'1',43,'lt'),(6087,13,'lom/general/coverage6095',6095,'',43,'lt'),(6087,13,'lom/general/description6093',6093,'Aprašymas',43,'lt'),(6088,13,'lom/general/identifier/catalog6089',6089,'',43,'lt'),(6088,13,'lom/general/identifier/entry6090',6090,'',43,'lt'),(6087,13,'lom/general/identifier6088',6088,'',43,'lt'),(6087,13,'lom/general/keyword6094',6094,'testas',43,'lt'),(6087,13,'lom/general/language6092_by',6092,'',43,'lt'),(6087,13,'lom/general/language6092_de',6092,'',43,'lt'),(6087,13,'lom/general/language6092_dk',6092,'',43,'lt'),(6087,13,'lom/general/language6092_en',6092,'',43,'lt'),(6087,13,'lom/general/language6092_es',6092,'',43,'lt'),(6087,13,'lom/general/language6092_fr',6092,'on',43,'lt'),(6087,13,'lom/general/language6092_it',6092,'',43,'lt'),(6087,13,'lom/general/language6092_lt',6092,'on',43,'lt'),(6087,13,'lom/general/language6092_no',6092,'',43,'lt'),(6087,13,'lom/general/language6092_pl',6092,'',43,'lt'),(6087,13,'lom/general/language6092_ru',6092,'on',43,'lt'),(6087,13,'lom/general/language6092_x-none',6092,'',43,'lt'),(6087,13,'lom/general/structure6096',6096,'elementarus objektas',43,'lt'),(6087,13,'lom/general/title6091',6091,'Mokymo objektas 123',43,'lt'),(0,13,'lom/general6087',6087,'',43,'lt'),(6101,13,'lom/lifecycle/contribute/date6104',6104,'',43,'lt'),(6101,13,'lom/lifecycle/contribute/entity6103',6103,'',43,'lt'),(6101,13,'lom/lifecycle/contribute/role6102',6102,'',43,'lt'),(6098,13,'lom/lifecycle/contribute6101',6101,'',43,'lt'),(6098,13,'lom/lifecycle/status6100',6100,'galutinis',43,'lt'),(6098,13,'lom/lifecycle/version6099',6099,'',43,'lt'),(0,13,'lom/lifecycle6098',6098,'',43,'lt'),(6122,13,'lom/rights/copyrightandotherrestrictions6124',6124,'',43,'lt'),(6122,13,'lom/rights/cost6123',6123,'',43,'lt'),(6122,13,'lom/rights/description6125',6125,'',43,'lt'),(0,13,'lom/rights6122',6122,'',43,'lt'),(6105,13,'lom/technical/duration6117',6117,'',43,'lt'),(6118,13,'lom/technical/facet/description6121',6121,'',43,'lt'),(6118,13,'lom/technical/facet/name6119',6119,'',43,'lt'),(6118,13,'lom/technical/facet/value6120',6120,'pako formatas:::application:::įprastas pakas',43,'lt'),(6105,13,'lom/technical/facet6118',6118,'',43,'lt'),(6105,13,'lom/technical/format6106_1',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_10',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_11',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_12',6106,'on',43,'lt'),(6105,13,'lom/technical/format6106_13',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_14',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_15',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_16',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_17',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_18',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_19',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_2',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_20',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_21',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_22',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_23',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_24',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_25',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_26',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_27',6106,'on',43,'lt'),(6105,13,'lom/technical/format6106_28',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_29',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_3',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_30',6106,'on',43,'lt'),(6105,13,'lom/technical/format6106_31',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_4',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_5',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_6',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_7',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_8',6106,'',43,'lt'),(6105,13,'lom/technical/format6106_9',6106,'',43,'lt'),(6105,13,'lom/technical/installationremarks6115',6115,'',43,'lt'),(6105,13,'lom/technical/location6108',6108,'',43,'lt'),(6105,13,'lom/technical/otherplatformrequirements6116',6116,'',43,'lt'),(6110,13,'lom/technical/requirement/orcomposite/maximumversion6114',6114,'',43,'lt'),(6110,13,'lom/technical/requirement/orcomposite/minimumversion6113',6113,'',43,'lt'),(6110,13,'lom/technical/requirement/orcomposite/name6112',6112,'os:::pcdos:::pc-dos',43,'lt'),(6110,13,'lom/technical/requirement/orcomposite/type6111',6111,'',43,'lt'),(6109,13,'lom/technical/requirement/orcomposite6110',6110,'',43,'lt'),(6105,13,'lom/technical/requirement6109',6109,'',43,'lt'),(6105,13,'lom/technical/size6107',6107,'',43,'lt'),(0,13,'lom/technical6105',6105,'',43,'lt'),(6169,14,'lom/annotation/date6171',6171,'',43,'lt'),(6169,14,'lom/annotation/description6172',6172,'',43,'lt'),(6169,14,'lom/annotation/entity6170',6170,'',43,'lt'),(0,14,'lom/annotation6169',6169,'',43,'lt'),(6220,14,'lom/classification/description6227',6227,'',43,'lt'),(6220,14,'lom/classification/keyword6228',6228,'',43,'lt'),(6220,14,'lom/classification/purpose6221',6221,'Ugdymo programos',43,'lt'),(6222,14,'lom/classification/taxonpath/source6223',6223,'Lietuvos klasifikatorius',43,'lt'),(6224,14,'lom/classification/taxonpath/taxon/entry6226',6226,'',43,'lt'),(6224,14,'lom/classification/taxonpath/taxon/id6225',6225,'',43,'lt'),(6222,14,'lom/classification/taxonpath/taxon6224',6224,'',43,'lt'),(6220,14,'lom/classification/taxonpath6222',6222,'',43,'lt'),(0,14,'lom/classification6220',6220,'',43,'lt'),(6173,14,'lom/educational/context6179_1',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_10',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_11',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_12',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_2',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_3',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_4',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_5',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_6',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_7',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_8',6179,'',43,'lt'),(6173,14,'lom/educational/context6179_9',6179,'',43,'lt'),(6173,14,'lom/educational/description6183',6183,'',43,'lt'),(6173,14,'lom/educational/difficulty6181',6181,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_1',6178,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_2',6178,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_3',6178,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_4',6178,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_5',6178,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_6',6178,'',43,'lt'),(6173,14,'lom/educational/intendedenduserrole6178_7',6178,'',43,'lt'),(6173,14,'lom/educational/interactivitylevel6176',6176,'',43,'lt'),(6173,14,'lom/educational/interactivitytype6174',6174,'',43,'lt'),(6173,14,'lom/educational/language6184_by',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_de',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_dk',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_en',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_es',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_fr',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_it',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_lt',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_no',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_pl',6184,'',43,'lt'),(6173,14,'lom/educational/language6184_ru',6184,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_1',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_10',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_11',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_12',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_13',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_14',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_15',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_16',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_17',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_18',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_19',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_2',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_20',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_3',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_4',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_5',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_6',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_7',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_8',6175,'',43,'lt'),(6173,14,'lom/educational/learningresourcetype6175_9',6175,'',43,'lt'),(6173,14,'lom/educational/semanticdensity6177',6177,'',43,'lt'),(6173,14,'lom/educational/typicalagerange6180',6180,'',43,'lt'),(6173,14,'lom/educational/typicallearningtime6182',6182,'',43,'lt'),(0,14,'lom/educational6173',6173,'',43,'lt'),(6158,14,'lom/general/aggregationlevel6168',6168,'',43,'lt'),(6158,14,'lom/general/coverage6166',6166,'',43,'lt'),(6158,14,'lom/general/description6164',6164,'a',43,'lt'),(6159,14,'lom/general/identifier/catalog6160',6160,'',43,'lt'),(6159,14,'lom/general/identifier/entry6161',6161,'',43,'lt'),(6158,14,'lom/general/identifier6159',6159,'',43,'lt'),(6158,14,'lom/general/keyword6165',6165,'test',43,'lt'),(6158,14,'lom/general/language6163_by',6163,'',43,'lt'),(6158,14,'lom/general/language6163_de',6163,'',43,'lt'),(6158,14,'lom/general/language6163_dk',6163,'',43,'lt'),(6158,14,'lom/general/language6163_en',6163,'',43,'lt'),(6158,14,'lom/general/language6163_es',6163,'',43,'lt'),(6158,14,'lom/general/language6163_fr',6163,'',43,'lt'),(6158,14,'lom/general/language6163_it',6163,'',43,'lt'),(6158,14,'lom/general/language6163_lt',6163,'on',43,'lt'),(6158,14,'lom/general/language6163_no',6163,'',43,'lt'),(6158,14,'lom/general/language6163_pl',6163,'',43,'lt'),(6158,14,'lom/general/language6163_ru',6163,'',43,'lt'),(6158,14,'lom/general/language6163_x-none',6163,'',43,'lt'),(6158,14,'lom/general/structure6167',6167,'',43,'lt'),(6158,14,'lom/general/title6162',6162,'Mokymo objektas',43,'lt'),(0,14,'lom/general6158',6158,'',43,'lt'),(6205,14,'lom/lifecycle/contribute/date6208',6208,'',43,'lt'),(6205,14,'lom/lifecycle/contribute/entity6207',6207,'',43,'lt'),(6205,14,'lom/lifecycle/contribute/role6206',6206,'',43,'lt'),(6202,14,'lom/lifecycle/contribute6205',6205,'',43,'lt'),(6202,14,'lom/lifecycle/status6204',6204,'galutinis',43,'lt'),(6202,14,'lom/lifecycle/version6203',6203,'',43,'lt'),(0,14,'lom/lifecycle6202',6202,'',43,'lt'),(6213,14,'lom/relation/kind6214',6214,'',43,'lt'),(6215,14,'lom/relation/resource/description6219',6219,'',43,'lt'),(6216,14,'lom/relation/resource/identifier/catalog6217',6217,'',43,'lt'),(6216,14,'lom/relation/resource/identifier/entry6218',6218,'',43,'lt'),(6215,14,'lom/relation/resource/identifier6216',6216,'',43,'lt'),(6213,14,'lom/relation/resource6215',6215,'',43,'lt'),(0,14,'lom/relation6213',6213,'',43,'lt'),(6209,14,'lom/rights/copyrightandotherrestrictions6211',6211,'',43,'lt'),(6209,14,'lom/rights/cost6210',6210,'',43,'lt'),(6209,14,'lom/rights/description6212',6212,'',43,'lt'),(0,14,'lom/rights6209',6209,'',43,'lt'),(6185,14,'lom/technical/duration6197',6197,'',43,'lt'),(6198,14,'lom/technical/facet/description6201',6201,'',43,'lt'),(6198,14,'lom/technical/facet/name6199',6199,'',43,'lt'),(6198,14,'lom/technical/facet/value6200',6200,'pako formatas:::application:::įprastas pakas',43,'lt'),(6185,14,'lom/technical/facet6198',6198,'',43,'lt'),(6185,14,'lom/technical/format6186_1',6186,'on',43,'lt'),(6185,14,'lom/technical/format6186_10',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_11',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_12',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_13',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_14',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_15',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_16',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_17',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_18',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_19',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_2',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_20',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_21',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_22',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_23',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_24',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_25',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_26',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_27',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_28',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_29',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_3',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_30',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_31',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_4',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_5',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_6',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_7',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_8',6186,'',43,'lt'),(6185,14,'lom/technical/format6186_9',6186,'',43,'lt'),(6185,14,'lom/technical/installationremarks6195',6195,'',43,'lt'),(6185,14,'lom/technical/location6188',6188,'',43,'lt'),(6185,14,'lom/technical/otherplatformrequirements6196',6196,'',43,'lt'),(6190,14,'lom/technical/requirement/orcomposite/maximumversion6194',6194,'',43,'lt'),(6190,14,'lom/technical/requirement/orcomposite/minimumversion6193',6193,'',43,'lt'),(6190,14,'lom/technical/requirement/orcomposite/name6192',6192,'os:::pcdos:::pc-dos',43,'lt'),(6190,14,'lom/technical/requirement/orcomposite/type6191',6191,'',43,'lt'),(6189,14,'lom/technical/requirement/orcomposite6190',6190,'',43,'lt'),(6185,14,'lom/technical/requirement6189',6189,'',43,'lt'),(6185,14,'lom/technical/size6187',6187,'',43,'lt'),(0,14,'lom/technical6185',6185,'',43,'lt'),(6508,15,'lom/classification/description6515',6515,'',43,'lt'),(6508,15,'lom/classification/keyword6516',6516,'',43,'lt'),(6508,15,'lom/classification/purpose6509',6509,'',43,'lt'),(6510,15,'lom/classification/taxonpath/source6511',6511,'Dalykai',43,'lt'),(6512,15,'lom/classification/taxonpath/taxon/entry6514',6514,'',43,'lt'),(6512,15,'lom/classification/taxonpath/taxon/id6513',6513,'',43,'lt'),(6510,15,'lom/classification/taxonpath/taxon6512',6512,'',43,'lt'),(6508,15,'lom/classification/taxonpath6510',6510,'',43,'lt'),(0,15,'lom/classification6508',6508,'',43,'lt'),(6382,15,'lom/general/aggregationlevel6392',6392,'',43,'lt'),(6382,15,'lom/general/coverage6390',6390,'',43,'lt'),(6382,15,'lom/general/description6388',6388,'',43,'lt'),(6383,15,'lom/general/identifier/catalog6384',6384,'URL',43,'lt'),(6383,15,'lom/general/identifier/entry6385',6385,'',43,'lt'),(6382,15,'lom/general/identifier6383',6383,'',43,'lt'),(6382,15,'lom/general/keyword6389',6389,'',43,'lt'),(6382,15,'lom/general/language6387_by',6387,'',43,'lt'),(6382,15,'lom/general/language6387_de',6387,'',43,'lt'),(6382,15,'lom/general/language6387_dk',6387,'',43,'lt'),(6382,15,'lom/general/language6387_en',6387,'',43,'lt'),(6382,15,'lom/general/language6387_es',6387,'',43,'lt'),(6382,15,'lom/general/language6387_fr',6387,'',43,'lt'),(6382,15,'lom/general/language6387_it',6387,'',43,'lt'),(6382,15,'lom/general/language6387_lt',6387,'on',43,'lt'),(6382,15,'lom/general/language6387_no',6387,'',43,'lt'),(6382,15,'lom/general/language6387_pl',6387,'',43,'lt'),(6382,15,'lom/general/language6387_ru',6387,'',43,'lt'),(6382,15,'lom/general/language6387_x-none',6387,'',43,'lt'),(6382,15,'lom/general/structure6391',6391,'',43,'lt'),(6382,15,'lom/general/title6386',6386,'MO paketas',43,'lt'),(0,15,'lom/general6382',6382,'',43,'lt'),(4950,15,'lom/lifecycle/contribute/date4953',4953,'',43,'lt'),(4950,15,'lom/lifecycle/contribute/entity4952',4952,'',43,'lt'),(4950,15,'lom/lifecycle/contribute/role4951',4951,'',43,'lt'),(4947,15,'lom/lifecycle/contribute4950',4950,'',43,'lt'),(4947,15,'lom/lifecycle/status4949',4949,'galutinis',43,'lt'),(4947,15,'lom/lifecycle/version4948',4948,'',43,'lt'),(0,15,'lom/lifecycle4947',4947,'',43,'lt'),(6393,15,'lom/rights/copyrightandotherrestrictions6395',6395,'',43,'lt'),(6393,15,'lom/rights/cost6394',6394,'ne',43,'lt'),(6393,15,'lom/rights/description6396',6396,'',43,'lt'),(0,15,'lom/rights6393',6393,'',43,'lt'),(6486,16,'lom/general/aggregationlevel6496',6496,'',44,'lt'),(6486,16,'lom/general/coverage6494',6494,'',44,'lt'),(6486,16,'lom/general/description6492',6492,'',44,'lt'),(6487,16,'lom/general/identifier/catalog6488',6488,'',44,'lt'),(6487,16,'lom/general/identifier/entry6489',6489,'',44,'lt'),(6486,16,'lom/general/identifier6487',6487,'',44,'lt'),(6486,16,'lom/general/keyword6493',6493,'',44,'lt'),(6486,16,'lom/general/language6491_by',6491,'',44,'lt'),(6486,16,'lom/general/language6491_de',6491,'',44,'lt'),(6486,16,'lom/general/language6491_dk',6491,'',44,'lt'),(6486,16,'lom/general/language6491_en',6491,'',44,'lt'),(6486,16,'lom/general/language6491_es',6491,'',44,'lt'),(6486,16,'lom/general/language6491_fr',6491,'',44,'lt'),(6486,16,'lom/general/language6491_it',6491,'',44,'lt'),(6486,16,'lom/general/language6491_lt',6491,'on',44,'lt'),(6486,16,'lom/general/language6491_no',6491,'',44,'lt'),(6486,16,'lom/general/language6491_pl',6491,'',44,'lt'),(6486,16,'lom/general/language6491_ru',6491,'',44,'lt'),(6486,16,'lom/general/language6491_x-none',6491,'',44,'lt'),(6486,16,'lom/general/structure6495',6495,'',44,'lt'),(6486,16,'lom/general/title6490',6490,'MO vartotojo2',44,'lt'),(0,16,'lom/general6486',6486,'',44,'lt'),(6497,16,'lom/rights/copyrightandotherrestrictions6499',6499,'',44,'lt'),(6497,16,'lom/rights/cost6498',6498,'ne',44,'lt'),(6497,16,'lom/rights/description6500',6500,'',44,'lt'),(0,16,'lom/rights6497',6497,'',44,'lt'),(3266,17,'lom/classification/description3273',3273,'',0,'lt'),(3266,17,'lom/classification/keyword3274',3274,'',0,'lt'),(3266,17,'lom/classification/purpose3267',3267,'',0,'lt'),(3268,17,'lom/classification/taxonpath/source3269',3269,'Pabandymui toks',0,'lt'),(3270,17,'lom/classification/taxonpath/taxon/entry3272',3272,'35',0,'lt'),(3270,17,'lom/classification/taxonpath/taxon/id3271',3271,'35',0,'lt'),(3268,17,'lom/classification/taxonpath/taxon3270',3270,'',0,'lt'),(3266,17,'lom/classification/taxonpath3268',3268,'',0,'lt'),(0,17,'lom/classification3266',3266,'',0,'lt'),(6711,17,'lom/general/aggregationlevel6721',6721,'1',0,'lt'),(6711,17,'lom/general/coverage6719',6719,'',0,'lt'),(6711,17,'lom/general/description6717',6717,'aaa bbb',0,'lt'),(6712,17,'lom/general/identifier/catalog6713',6713,'',0,'lt'),(6712,17,'lom/general/identifier/entry6714',6714,'',0,'lt'),(6711,17,'lom/general/identifier6712',6712,'',0,'lt'),(6711,17,'lom/general/keyword6718',6718,'',0,'lt'),(6711,17,'lom/general/language6716_by',6716,'',0,'lt'),(6711,17,'lom/general/language6716_de',6716,'',0,'lt'),(6711,17,'lom/general/language6716_dk',6716,'',0,'lt'),(6711,17,'lom/general/language6716_en',6716,'',0,'lt'),(6711,17,'lom/general/language6716_es',6716,'',0,'lt'),(6711,17,'lom/general/language6716_fr',6716,'on',0,'lt'),(6711,17,'lom/general/language6716_it',6716,'',0,'lt'),(6711,17,'lom/general/language6716_lt',6716,'on',0,'lt'),(6711,17,'lom/general/language6716_no',6716,'',0,'lt'),(6711,17,'lom/general/language6716_pl',6716,'',0,'lt'),(6711,17,'lom/general/language6716_ru',6716,'on',0,'lt'),(6711,17,'lom/general/language6716_x-none',6716,'',0,'lt'),(6711,17,'lom/general/structure6720',6720,'',0,'lt'),(6711,17,'lom/general/title6715',6715,'Testinis mokymo objektas',0,'lt'),(0,17,'lom/general6711',6711,'',0,'lt'),(3234,17,'lom/lifecycle/contribute/date3237',3237,'',0,'lt'),(3234,17,'lom/lifecycle/contribute/entity3236',3236,'',0,'lt'),(3234,17,'lom/lifecycle/contribute/role3235',3235,'',0,'lt'),(3231,17,'lom/lifecycle/contribute3234',3234,'',0,'lt'),(3231,17,'lom/lifecycle/status3233',3233,'',0,'lt'),(3231,17,'lom/lifecycle/version3232',3232,'',0,'lt'),(0,17,'lom/lifecycle3231',3231,'',0,'lt'),(3259,17,'lom/relation/kind3260',3260,'',0,'lt'),(3261,17,'lom/relation/resource/description3265',3265,'',0,'lt'),(3262,17,'lom/relation/resource/identifier/catalog3263',3263,'',0,'lt'),(3262,17,'lom/relation/resource/identifier/entry3264',3264,'',0,'lt'),(3261,17,'lom/relation/resource/identifier3262',3262,'',0,'lt'),(3259,17,'lom/relation/resource3261',3261,'',0,'lt'),(0,17,'lom/relation3259',3259,'',0,'lt'),(3255,17,'lom/rights/copyrightandotherrestrictions3257',3257,'',0,'lt'),(3255,17,'lom/rights/cost3256',3256,'',0,'lt'),(3255,17,'lom/rights/description3258',3258,'',0,'lt'),(0,17,'lom/rights3255',3255,'',0,'lt'),(3238,17,'lom/technical/duration3250',3250,'',0,'lt'),(3251,17,'lom/technical/facet/description3254',3254,'',0,'lt'),(3251,17,'lom/technical/facet/name3252',3252,'',0,'lt'),(3251,17,'lom/technical/facet/value3253',3253,'pako formatas:::application:::įprastas pakas',0,'lt'),(3238,17,'lom/technical/facet3251',3251,'',0,'lt'),(3238,17,'lom/technical/format3239_1',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_10',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_11',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_12',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_13',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_14',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_15',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_16',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_17',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_18',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_19',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_2',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_20',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_21',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_22',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_23',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_24',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_25',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_26',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_27',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_28',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_29',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_3',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_30',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_31',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_4',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_5',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_6',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_7',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_8',3239,'',0,'lt'),(3238,17,'lom/technical/format3239_9',3239,'',0,'lt'),(3238,17,'lom/technical/installationremarks3248',3248,'',0,'lt'),(3238,17,'lom/technical/location3241',3241,'',0,'lt'),(3238,17,'lom/technical/otherplatformrequirements3249',3249,'',0,'lt'),(3243,17,'lom/technical/requirement/orcomposite/maximumversion3247',3247,'',0,'lt'),(3243,17,'lom/technical/requirement/orcomposite/minimumversion3246',3246,'',0,'lt'),(3243,17,'lom/technical/requirement/orcomposite/name3245',3245,'os:::pcdos:::pc-dos',0,'lt'),(3243,17,'lom/technical/requirement/orcomposite/type3244',3244,'',0,'lt'),(3242,17,'lom/technical/requirement/orcomposite3243',3243,'',0,'lt'),(3238,17,'lom/technical/requirement3242',3242,'',0,'lt'),(3238,17,'lom/technical/size3240',3240,'',0,'lt'),(0,17,'lom/technical3238',3238,'',0,'lt'),(0,17,'lom1',0,'',0,'lt'),(2921,18,'lom/general/aggregationlevel2931',2931,'1',1,'lt'),(2921,18,'lom/general/coverage2929',2929,'aprėptis',1,'lt'),(2921,18,'lom/general/description2927',2927,'Ąžuolas ąčęėįšųūž ĄČĘĖĮŠŲŪŽ',1,'lt'),(2922,18,'lom/general/identifier/catalog2923',2923,'ISBN',1,'lt'),(2922,18,'lom/general/identifier/entry2924',2924,'kažkoks įraša čia',1,'lt'),(2921,18,'lom/general/identifier2922',2922,'',1,'lt'),(2921,18,'lom/general/keyword2928',2928,'Reiškminis',1,'lt'),(2921,18,'lom/general/language2926_by',2926,'',1,'lt'),(2921,18,'lom/general/language2926_de',2926,'',1,'lt'),(2921,18,'lom/general/language2926_dk',2926,'',1,'lt'),(2921,18,'lom/general/language2926_en',2926,'on',1,'lt'),(2921,18,'lom/general/language2926_es',2926,'',1,'lt'),(2921,18,'lom/general/language2926_fr',2926,'',1,'lt'),(2921,18,'lom/general/language2926_it',2926,'on',1,'lt'),(2921,18,'lom/general/language2926_lt',2926,'',1,'lt'),(2921,18,'lom/general/language2926_no',2926,'',1,'lt'),(2921,18,'lom/general/language2926_pl',2926,'on',1,'lt'),(2921,18,'lom/general/language2926_ru',2926,'',1,'lt'),(2921,18,'lom/general/language2926_x-none',2926,'',1,'lt'),(2921,18,'lom/general/structure2930',2930,'elementarus objektas',1,'lt'),(2921,18,'lom/general/title2925',2925,'Privalomasisi įrašas',1,'lt'),(0,18,'lom/general2921',2921,'',1,'lt'),(3075,20,'lom/general/aggregationlevel3085',3085,'2',1,'lt'),(3075,20,'lom/general/coverage3083',3083,'asdf',1,'lt'),(3075,20,'lom/general/description3081',3081,'asdf',1,'lt'),(3076,20,'lom/general/identifier/catalog3077',3077,'ISBN',1,'lt'),(3076,20,'lom/general/identifier/entry3078',3078,'adsf',1,'lt'),(3075,20,'lom/general/identifier3076',3076,'',1,'lt'),(3075,20,'lom/general/keyword3082',3082,'afs',1,'lt'),(3075,20,'lom/general/language3080_by',3080,'',1,'lt'),(3075,20,'lom/general/language3080_de',3080,'',1,'lt'),(3075,20,'lom/general/language3080_dk',3080,'',1,'lt'),(3075,20,'lom/general/language3080_en',3080,'on',1,'lt'),(3075,20,'lom/general/language3080_es',3080,'',1,'lt'),(3075,20,'lom/general/language3080_fr',3080,'',1,'lt'),(3075,20,'lom/general/language3080_it',3080,'',1,'lt'),(3075,20,'lom/general/language3080_lt',3080,'',1,'lt'),(3075,20,'lom/general/language3080_no',3080,'',1,'lt'),(3075,20,'lom/general/language3080_pl',3080,'',1,'lt'),(3075,20,'lom/general/language3080_ru',3080,'',1,'lt'),(3075,20,'lom/general/language3080_x-none',3080,'',1,'lt'),(3075,20,'lom/general/structure3084',3084,'rinkinys',1,'lt'),(3075,20,'lom/general/title3079',3079,'adf',1,'lt'),(0,20,'lom/general3075',3075,'',1,'lt'),(6565,21,'lom/classification/description6572',6572,'',1,'lt'),(6565,21,'lom/classification/keyword6573',6573,'',1,'lt'),(6565,21,'lom/classification/purpose6566',6566,'',1,'lt'),(6567,21,'lom/classification/taxonpath/source6568',6568,'Lietuvos klasifikatorius',1,'lt'),(6934,21,'lom/classification/taxonpath/source6935',6935,'spalvos',1,'lt'),(6939,21,'lom/classification/taxonpath/source6940',6940,'Dalykinės sritys',1,'lt'),(6569,21,'lom/classification/taxonpath/taxon/entry6571',6571,'42',1,'lt'),(6936,21,'lom/classification/taxonpath/taxon/entry6938',6938,'361',1,'lt'),(6941,21,'lom/classification/taxonpath/taxon/entry6943',6943,'380',1,'lt'),(6941,21,'lom/classification/taxonpath/taxon/entry6944',6944,'',1,'lt'),(6941,21,'lom/classification/taxonpath/taxon/entry6945',6945,'',1,'lt'),(6569,21,'lom/classification/taxonpath/taxon/entry6946',6946,'1',1,'lt'),(6569,21,'lom/classification/taxonpath/taxon/entry6947',6947,'35',1,'lt'),(6569,21,'lom/classification/taxonpath/taxon/id6570',6570,'35',1,'lt'),(6936,21,'lom/classification/taxonpath/taxon/id6937',6937,'361',1,'lt'),(6941,21,'lom/classification/taxonpath/taxon/id6942',6942,'380',1,'lt'),(6567,21,'lom/classification/taxonpath/taxon6569',6569,'',1,'lt'),(6934,21,'lom/classification/taxonpath/taxon6936',6936,'',1,'lt'),(6939,21,'lom/classification/taxonpath/taxon6941',6941,'',1,'lt'),(6565,21,'lom/classification/taxonpath6567',6567,'',1,'lt'),(6565,21,'lom/classification/taxonpath6934',6934,'',1,'lt'),(6565,21,'lom/classification/taxonpath6939',6939,'',1,'lt'),(0,21,'lom/classification6565',6565,'',1,'lt'),(5071,21,'lom/general/aggregationlevel5081',5081,'4',1,'lt'),(5071,21,'lom/general/coverage5079',5079,'',1,'lt'),(5071,21,'lom/general/description5077',5077,'Aprašas1',1,'lt'),(5072,21,'lom/general/identifier/catalog5073',5073,'URL',1,'lt'),(6928,21,'lom/general/identifier/catalog6929',6929,'ISBN',1,'lt'),(6931,21,'lom/general/identifier/catalog6932',6932,'URL',1,'lt'),(5072,21,'lom/general/identifier/entry5074',5074,'Išrašas1',1,'lt'),(6928,21,'lom/general/identifier/entry6930',6930,'3',1,'lt'),(6931,21,'lom/general/identifier/entry6933',6933,'išrašas3',1,'lt'),(5071,21,'lom/general/identifier5072',5072,'',1,'lt'),(5071,21,'lom/general/identifier6928',6928,'',1,'lt'),(5071,21,'lom/general/identifier6931',6931,'',1,'lt'),(5071,21,'lom/general/keyword5078',5078,'reikšminis',1,'lt'),(5071,21,'lom/general/language5076_by',5076,'',1,'lt'),(5071,21,'lom/general/language5076_de',5076,'',1,'lt'),(5071,21,'lom/general/language5076_dk',5076,'',1,'lt'),(5071,21,'lom/general/language5076_en',5076,'on',1,'lt'),(5071,21,'lom/general/language5076_es',5076,'',1,'lt'),(5071,21,'lom/general/language5076_fr',5076,'',1,'lt'),(5071,21,'lom/general/language5076_it',5076,'',1,'lt'),(5071,21,'lom/general/language5076_lt',5076,'on',1,'lt'),(5071,21,'lom/general/language5076_no',5076,'',1,'lt'),(5071,21,'lom/general/language5076_pl',5076,'',1,'lt'),(5071,21,'lom/general/language5076_ru',5076,'',1,'lt'),(5071,21,'lom/general/language5076_x-none',5076,'',1,'lt'),(5071,21,'lom/general/structure5080',5080,'elementarus objektas',1,'lt'),(5071,21,'lom/general/title5075',5075,'Pavadinimas1',1,'lt'),(0,21,'lom/general5071',5071,'',1,'lt'),(5228,21,'lom/lifecycle/contribute/date5231',5231,'',1,'lt'),(5228,21,'lom/lifecycle/contribute/entity5230',5230,'',1,'lt'),(5228,21,'lom/lifecycle/contribute/role5229',5229,'',1,'lt'),(5225,21,'lom/lifecycle/contribute5228',5228,'',1,'lt'),(5225,21,'lom/lifecycle/status5227',5227,'',1,'lt'),(5225,21,'lom/lifecycle/version5226',5226,'',1,'lt'),(0,21,'lom/lifecycle5225',5225,'',1,'lt'),(6558,21,'lom/relation/kind6559',6559,'',1,'lt'),(6560,21,'lom/relation/resource/description6564',6564,'',1,'lt'),(6561,21,'lom/relation/resource/identifier/catalog6562',6562,'',1,'lt'),(6561,21,'lom/relation/resource/identifier/entry6563',6563,'',1,'lt'),(6560,21,'lom/relation/resource/identifier6561',6561,'',1,'lt'),(6558,21,'lom/relation/resource6560',6560,'',1,'lt'),(0,21,'lom/relation6558',6558,'',1,'lt'),(6574,21,'lom/rights/copyrightandotherrestrictions6576',6576,'',1,'lt'),(6574,21,'lom/rights/cost6575',6575,'',1,'lt'),(6574,21,'lom/rights/description6577',6577,'',1,'lt'),(0,21,'lom/rights6574',6574,'',1,'lt'),(5232,21,'lom/technical/duration5244',5244,'12',1,'lt'),(5245,21,'lom/technical/facet/description5248',5248,'',1,'lt'),(5245,21,'lom/technical/facet/name5246',5246,'',1,'lt'),(5245,21,'lom/technical/facet/value5247',5247,'pako formatas:::application:::įprastas pakas',1,'lt'),(5232,21,'lom/technical/facet5245',5245,'',1,'lt'),(5232,21,'lom/technical/format5233_1',5233,'on',1,'lt'),(5232,21,'lom/technical/format5233_10',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_11',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_12',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_13',5233,'on',1,'lt'),(5232,21,'lom/technical/format5233_14',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_15',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_16',5233,'on',1,'lt'),(5232,21,'lom/technical/format5233_17',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_18',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_19',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_2',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_20',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_21',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_22',5233,'on',1,'lt'),(5232,21,'lom/technical/format5233_23',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_24',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_25',5233,'on',1,'lt'),(5232,21,'lom/technical/format5233_26',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_27',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_28',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_29',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_3',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_30',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_31',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_4',5233,'on',1,'lt'),(5232,21,'lom/technical/format5233_5',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_6',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_7',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_8',5233,'',1,'lt'),(5232,21,'lom/technical/format5233_9',5233,'',1,'lt'),(5232,21,'lom/technical/installationremarks5242',5242,'Diegimo instrukcija',1,'lt'),(5232,21,'lom/technical/location5235',5235,'12',1,'lt'),(5232,21,'lom/technical/otherplatformrequirements5243',5243,'Kiti reikalavimai',1,'lt'),(5237,21,'lom/technical/requirement/orcomposite/maximumversion5241',5241,'Versija vėliausioji',1,'lt'),(5237,21,'lom/technical/requirement/orcomposite/minimumversion5240',5240,'Versija ankščiausioj',1,'lt'),(5237,21,'lom/technical/requirement/orcomposite/name5239',5239,'os:::pcdos:::pc-dos',1,'lt'),(5237,21,'lom/technical/requirement/orcomposite/type5238',5238,'browser:::naršyklė',1,'lt'),(5236,21,'lom/technical/requirement/orcomposite5237',5237,'',1,'lt'),(5232,21,'lom/technical/requirement5236',5236,'',1,'lt'),(5232,21,'lom/technical/size5234',5234,'12',1,'lt'),(0,21,'lom/technical5232',5232,'',1,'lt'),(6998,29,'lom/classification/description7005',7005,'',1,'lt'),(6998,29,'lom/classification/keyword7006',7006,'',1,'lt'),(6998,29,'lom/classification/purpose6999',6999,'',1,'lt'),(7000,29,'lom/classification/taxonpath/source7001',7001,'Lietuvos klasifikatorius',1,'lt'),(7002,29,'lom/classification/taxonpath/taxon/entry7004',7004,'1',1,'lt'),(7002,29,'lom/classification/taxonpath/taxon/id7003',7003,'1',1,'lt'),(7000,29,'lom/classification/taxonpath/taxon7002',7002,'',1,'lt'),(6998,29,'lom/classification/taxonpath7000',7000,'',1,'lt'),(0,29,'lom/classification6998',6998,'',1,'lt'),(6959,29,'lom/general/aggregationlevel6969',6969,'3',1,'lt'),(6959,29,'lom/general/coverage6967',6967,'',1,'lt'),(6959,29,'lom/general/description6965',6965,'aprašas',1,'lt'),(6960,29,'lom/general/identifier/catalog6961',6961,'',1,'lt'),(6960,29,'lom/general/identifier/entry6962',6962,'',1,'lt'),(6959,29,'lom/general/identifier6960',6960,'',1,'lt'),(6959,29,'lom/general/keyword6966',6966,'reikšminis žods',1,'lt'),(6959,29,'lom/general/language6964_by',6964,'',1,'lt'),(6959,29,'lom/general/language6964_de',6964,'',1,'lt'),(6959,29,'lom/general/language6964_dk',6964,'',1,'lt'),(6959,29,'lom/general/language6964_en',6964,'on',1,'lt'),(6959,29,'lom/general/language6964_es',6964,'',1,'lt'),(6959,29,'lom/general/language6964_fr',6964,'',1,'lt'),(6959,29,'lom/general/language6964_it',6964,'',1,'lt'),(6959,29,'lom/general/language6964_lt',6964,'on',1,'lt'),(6959,29,'lom/general/language6964_no',6964,'',1,'lt'),(6959,29,'lom/general/language6964_pl',6964,'',1,'lt'),(6959,29,'lom/general/language6964_ru',6964,'',1,'lt'),(6959,29,'lom/general/language6964_x-none',6964,'',1,'lt'),(6959,29,'lom/general/structure6968',6968,'',1,'lt'),(6959,29,'lom/general/title6963',6963,'Kursas',1,'lt'),(0,29,'lom/general6959',6959,'',1,'lt'),(6973,29,'lom/lifecycle/contribute/date6976',6976,'2013',1,'lt'),(6973,29,'lom/lifecycle/contribute/entity6975',6975,'asdf',1,'lt'),(6973,29,'lom/lifecycle/contribute/role6974',6974,'autorius',1,'lt'),(6970,29,'lom/lifecycle/contribute6973',6973,'',1,'lt'),(6970,29,'lom/lifecycle/status6972',6972,'galutinis',1,'lt'),(6970,29,'lom/lifecycle/version6971',6971,'1',1,'lt'),(0,29,'lom/lifecycle6970',6970,'',1,'lt'),(6994,29,'lom/rights/copyrightandotherrestrictions6996',6996,'',1,'lt'),(6994,29,'lom/rights/cost6995',6995,'',1,'lt'),(6994,29,'lom/rights/description6997',6997,'',1,'lt'),(0,29,'lom/rights6994',6994,'',1,'lt'),(6977,29,'lom/technical/duration6989',6989,'',1,'lt'),(6990,29,'lom/technical/facet/description6993',6993,'',1,'lt'),(6990,29,'lom/technical/facet/name6991',6991,'',1,'lt'),(6990,29,'lom/technical/facet/value6992',6992,'pako formatas:::application:::įprastas pakas',1,'lt'),(6977,29,'lom/technical/facet6990',6990,'',1,'lt'),(6977,29,'lom/technical/format6978_1',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_10',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_11',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_12',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_13',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_14',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_15',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_16',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_17',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_18',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_19',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_2',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_20',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_21',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_22',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_23',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_24',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_25',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_26',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_27',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_28',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_29',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_3',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_30',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_31',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_4',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_5',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_6',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_7',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_8',6978,'',1,'lt'),(6977,29,'lom/technical/format6978_9',6978,'',1,'lt'),(6977,29,'lom/technical/installationremarks6987',6987,'',1,'lt'),(6977,29,'lom/technical/location6980',6980,'',1,'lt'),(6977,29,'lom/technical/otherplatformrequirements6988',6988,'',1,'lt'),(6982,29,'lom/technical/requirement/orcomposite/maximumversion6986',6986,'',1,'lt'),(6982,29,'lom/technical/requirement/orcomposite/minimumversion6985',6985,'',1,'lt'),(6982,29,'lom/technical/requirement/orcomposite/name6984',6984,'os:::pcdos:::pc-dos',1,'lt'),(6982,29,'lom/technical/requirement/orcomposite/type6983',6983,'',1,'lt'),(6981,29,'lom/technical/requirement/orcomposite6982',6982,'',1,'lt'),(6977,29,'lom/technical/requirement6981',6981,'',1,'lt'),(6977,29,'lom/technical/size6979',6979,'',1,'lt'),(0,29,'lom/technical6977',6977,'',1,'lt'),(7018,30,'lom/classification/description7025',7025,'',1,'lt'),(7018,30,'lom/classification/keyword7026',7026,'',1,'lt'),(7018,30,'lom/classification/purpose7019',7019,'',1,'lt'),(7020,30,'lom/classification/taxonpath/source7021',7021,'Dalykinės sritys',1,'lt'),(7022,30,'lom/classification/taxonpath/taxon/entry7024',7024,'380',1,'lt'),(7022,30,'lom/classification/taxonpath/taxon/id7023',7023,'380',1,'lt'),(7020,30,'lom/classification/taxonpath/taxon7022',7022,'',1,'lt'),(7018,30,'lom/classification/taxonpath7020',7020,'',1,'lt'),(0,30,'lom/classification7018',7018,'',1,'lt'),(7007,30,'lom/general/aggregationlevel7017',7017,'3',1,'lt'),(7007,30,'lom/general/coverage7015',7015,'',1,'lt'),(7007,30,'lom/general/description7013',7013,'',1,'lt'),(7008,30,'lom/general/identifier/catalog7009',7009,'',1,'lt'),(7008,30,'lom/general/identifier/entry7010',7010,'',1,'lt'),(7007,30,'lom/general/identifier7008',7008,'',1,'lt'),(7007,30,'lom/general/keyword7014',7014,'',1,'lt'),(7007,30,'lom/general/language7012_by',7012,'',1,'lt'),(7007,30,'lom/general/language7012_de',7012,'',1,'lt'),(7007,30,'lom/general/language7012_dk',7012,'',1,'lt'),(7007,30,'lom/general/language7012_en',7012,'',1,'lt'),(7007,30,'lom/general/language7012_es',7012,'',1,'lt'),(7007,30,'lom/general/language7012_fr',7012,'',1,'lt'),(7007,30,'lom/general/language7012_it',7012,'',1,'lt'),(7007,30,'lom/general/language7012_lt',7012,'on',1,'lt'),(7007,30,'lom/general/language7012_no',7012,'',1,'lt'),(7007,30,'lom/general/language7012_pl',7012,'',1,'lt'),(7007,30,'lom/general/language7012_ru',7012,'',1,'lt'),(7007,30,'lom/general/language7012_x-none',7012,'',1,'lt'),(7007,30,'lom/general/structure7016',7016,'',1,'lt'),(7007,30,'lom/general/title7011',7011,'Kursas 2',1,'lt'),(0,30,'lom/general7007',7007,'',1,'lt'),(7027,31,'lom/general/aggregationlevel7037',7037,'3',1,'lt'),(7027,31,'lom/general/coverage7035',7035,'',1,'lt'),(7027,31,'lom/general/description7033',7033,'',1,'lt'),(7028,31,'lom/general/identifier/catalog7029',7029,'',1,'lt'),(7028,31,'lom/general/identifier/entry7030',7030,'',1,'lt'),(7027,31,'lom/general/identifier7028',7028,'',1,'lt'),(7027,31,'lom/general/keyword7034',7034,'',1,'lt'),(7027,31,'lom/general/language7032_by',7032,'',1,'lt'),(7027,31,'lom/general/language7032_de',7032,'',1,'lt'),(7027,31,'lom/general/language7032_dk',7032,'',1,'lt'),(7027,31,'lom/general/language7032_en',7032,'',1,'lt'),(7027,31,'lom/general/language7032_es',7032,'',1,'lt'),(7027,31,'lom/general/language7032_fr',7032,'on',1,'lt'),(7027,31,'lom/general/language7032_it',7032,'',1,'lt'),(7027,31,'lom/general/language7032_lt',7032,'on',1,'lt'),(7027,31,'lom/general/language7032_no',7032,'',1,'lt'),(7027,31,'lom/general/language7032_pl',7032,'',1,'lt'),(7027,31,'lom/general/language7032_ru',7032,'',1,'lt'),(7027,31,'lom/general/language7032_x-none',7032,'',1,'lt'),(7027,31,'lom/general/structure7036',7036,'',1,'lt'),(7027,31,'lom/general/title7031',7031,'Kursas 3',1,'lt'),(0,31,'lom/general7027',7027,'',1,'lt');
/*!40000 ALTER TABLE `metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metadata_test`
--

DROP TABLE IF EXISTS `metadata_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metadata_test` (
  `parent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'parent object id',
  `object` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'object id to which this metadata belongs',
  `xpath` varchar(254) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'xpath to this metadata item',
  `item` int(10) unsigned DEFAULT '0' COMMENT 'metadata item number, where multiple occurence allowed',
  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'metadata item value',
  `author` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT 'author user id',
  `language` varchar(2) COLLATE utf8_bin NOT NULL DEFAULT 'lt' COMMENT 'metadata language',
  PRIMARY KEY (`object`,`xpath`,`language`),
  KEY `xpath` (`xpath`(64)),
  KEY `object` (`object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metadata_test`
--

LOCK TABLES `metadata_test` WRITE;
/*!40000 ALTER TABLE `metadata_test` DISABLE KEYS */;
INSERT INTO `metadata_test` VALUES (2000,28,'lom/classification/purpose2000',2000,'Ugdymo programos',1,'lt'),(10000,28,'lom/classification/taxonpath/source10000',10000,'Lietuvos klasifikatorius',1,'lt'),(7000,28,'lom/classification/taxonpath/source7000',7000,'Kompetencijos',1,'lt'),(8000,28,'lom/classification/taxonpath/source8000',8000,'Kompetencijos',1,'lt'),(9000,28,'lom/classification/taxonpath/source9000',9000,'Dalykinės sritys',1,'lt'),(10000,28,'lom/classification/taxonpath/taxon/entry10000',10000,'Lietuvių kalba (gimtoji)',1,'lt'),(10000,28,'lom/classification/taxonpath/taxon/entry10001',10001,'Užsienio kalba (rusų)',1,'lt'),(7000,28,'lom/classification/taxonpath/taxon/entry7000',7000,'Sudoku sprendimo',1,'lt'),(7000,28,'lom/classification/taxonpath/taxon/entry7001',7001,'Žaibiško mąstymo',1,'lt'),(8000,28,'lom/classification/taxonpath/taxon/entry8000',8000,'Erelio akies',1,'lt'),(8000,28,'lom/classification/taxonpath/taxon/entry8001',8001,'Tėvo akis',1,'lt'),(9000,28,'lom/classification/taxonpath/taxon/entry9000',9000,'Dalykinė sritis 1',1,'lt'),(9000,28,'lom/classification/taxonpath/taxon/entry9001',9001,'Dalykinė sritisi 3',1,'lt'),(11000,28,'lom/general/coverage11000',11000,'gfj',1,'lt'),(5000,28,'lom/general/description5000',5000,'hjgfjgfj',1,'lt'),(6000,28,'lom/general/keyword6000',6000,'gfjghj',1,'lt'),(3000,28,'lom/general/title3000',3000,'gfhj',1,'lt'),(15000,28,'lom/lifecycle/contribute/entity15000',15000,'gfjgfj',1,'lt'),(15000,28,'lom/lifecycle/contribute/entity15001',15001,'abc',1,'lt'),(17000,28,'lom/lifecycle/contribute/entity17000',17000,'abc',1,'lt'),(17000,28,'lom/lifecycle/contribute/entity17001',17001,'d',1,'lt'),(4000,28,'lom/lifecycle/contribute/entity4000',4000,'ghjg',1,'lt'),(12000,28,'lom/lifecycle/contribute/role12000',12000,'leidėjas',1,'lt'),(18000,28,'lom/lifecycle/contribute/role18000',18000,'leidėjas',1,'lt'),(13000,28,'lom/lifecycle/status13000',13000,'galutinis',1,'lt'),(14000,28,'lom/lifecycle/version14000',14000,'fhjgfj',1,'lt');
/*!40000 ALTER TABLE `metadata_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_data`
--

DROP TABLE IF EXISTS `object_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_data` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  FULLTEXT KEY `data` (`data`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_data`
--

LOCK TABLES `object_data` WRITE;
/*!40000 ALTER TABLE `object_data` DISABLE KEYS */;
INSERT INTO `object_data` VALUES (1432,'otherfile','2010-Horizon-Report.pdf',0),(1,'otherfile','header.jpg',0),(1,'otherfile','pdncrash.log',0),(4,'otherfile','advpoll-7.x-3.x-dev.tar.gz',0),(5,'otherfile','1.jpg',0),(6,'otherfile','advpoll-7.x-3.x-dev.tar.gz',0),(7,'otherfile','images.jpg',0),(8,'otherfile','Web button_FINAL.jpg',0),(7,'otherfile','Web button_FINAL.jpg',0),(10,'otherfile','MO1.txt',0),(11,'otherfile','MO1.txt',0),(11,'otherfile','printscr FN_Shift_F11.txt',0),(13,'otherfile','lom saugyklos past.txt',0),(14,'otherfile','MO1.txt',0),(3,'otherfile','ContentPackagingMetadata_SCORM20043rdEdition.zip',0),(17,'otherfile','MO1.txt',0),(17,'otherfile','upc.jpg',0),(17,'otherfile','upc.jpg',0),(1,'otherfile','MO1.txt',0),(10,'otherfile','upc.jpg',0);
/*!40000 ALTER TABLE `object_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `object_view_counter`
--

DROP TABLE IF EXISTS `object_view_counter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `object_view_counter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `REQUEST_URL` varchar(50) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1200 DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `object_view_counter`
--

LOCK TABLES `object_view_counter` WRITE;
/*!40000 ALTER TABLE `object_view_counter` DISABLE KEYS */;
INSERT INTO `object_view_counter` VALUES (1,1432,1,'2013-04-04','public'),(2,1432,1,'2013-04-04','login.php'),(3,1432,1,'2013-04-04','user'),(4,1432,1,'2013-04-04','admin'),(5,1432,1,'2013-04-04','general'),(6,1,1,'2013-04-04','object_load.php'),(7,1,1,'2013-04-04','general'),(8,1,1,'2013-04-04','general'),(9,1,1,'2013-04-04','add_object_files.php'),(10,1,1,'2013-04-04','index.php'),(11,1,1,'2013-04-04','admin'),(12,1,1,'2013-04-04','object_list.php'),(13,1,1,'2013-04-04','object_load.php'),(14,1,1,'2013-04-04','general'),(15,1,1,'2013-04-04','general'),(16,1,1,'2013-04-04','public'),(17,1,1,'2013-04-04','public'),(18,1,1,'2013-04-04','login.php'),(19,1,1,'2013-04-04','user'),(20,1,1,'2013-04-04','admin'),(21,1,1,'2013-04-04','object_list.php'),(22,1,1,'2013-04-04','object_load.php'),(23,1,1,'2013-04-04','general'),(24,1,1,'2013-04-04','general'),(25,1,1,'2013-04-04','object_export_lom.php'),(26,1,1,'2013-04-04','object_load.php'),(27,1,1,'2013-04-04','general'),(28,1,1,'2013-04-04','object_list.php'),(29,1,1,'2013-04-04','object_load.php'),(30,1,1,'2013-04-04','general'),(31,1,1,'2013-04-04','identifier&parent=1926'),(32,1,1,'2013-04-04','general'),(33,1,1,'2013-04-04','identifier1937'),(34,1,1,'2013-04-04','general'),(35,1,1,'2013-04-04','general'),(36,1,1,'2013-04-04','object_list.php'),(37,1,1,'2013-04-04','general'),(38,1,1,'2013-04-04','general'),(39,1,1,'2013-04-04','general'),(40,1,1,'2013-04-04','add_object_files.php'),(41,1,1,'2013-04-04','index.php'),(42,1,1,'2013-04-04','admin'),(43,1,1,'2013-04-04','object_list.php'),(44,1,1,'2013-04-04','object_view.php'),(45,1,1,'2013-04-04','object_list.php'),(46,1,1,'2013-04-04','object_view.php'),(47,1,1,'2013-04-04','object_list.php'),(48,1,1,'2013-04-04','object_load.php'),(49,1,1,'2013-04-04','general'),(50,1,1,'2013-04-04','object_list.php'),(51,1,1,'2013-04-04','object_view_com.php'),(52,1,1,'2013-04-04','object_list.php'),(53,2,1,'2013-04-04','object_view_coment.php'),(54,2,1,'2013-04-04','article.php'),(55,2,1,'2013-04-04','public'),(56,2,1,'2013-04-04','login.php'),(57,2,1,'2013-04-04','user'),(58,2,1,'2013-04-04','admin'),(59,2,1,'2013-04-04','general'),(60,1,1,'2013-04-10','object_view.php'),(61,1,1,'2013-04-10','object_list.php'),(62,1,1,'2013-04-10','object_load.php'),(63,1,1,'2013-04-10','general'),(64,1,1,'2013-04-10','object_list.php'),(65,1,1,'2013-04-10','object_load.php'),(66,1,1,'2013-04-10','general'),(67,1,1,'2013-04-10','object_list.php'),(68,1,1,'2013-04-10','object_export_lom.php'),(69,1,1,'2013-04-10','object_view.php'),(70,1,1,'2013-04-10','object_list.php'),(71,1,1,'2013-04-10','object_view.php'),(72,1,1,'2013-04-10','object_list.php'),(73,1,1,'2013-04-10','object_load.php'),(74,1,1,'2013-04-10','general'),(75,1,1,'2013-04-10','general'),(76,1,1,'2013-04-10','object_save.php'),(77,1,1,'2013-04-10','object_view.php'),(78,1,1,'2013-04-10','object_list.php'),(79,1,1,'2013-04-10','object_load.php'),(80,1,1,'2013-04-10','general'),(81,1,1,'2013-04-10','general'),(82,1,1,'2013-04-10','object_save.php'),(83,4,1,'2013-04-10','object_view.php'),(84,4,1,'2013-04-10','object_list.php'),(85,4,1,'2013-04-10','object_view.php'),(86,4,1,'2013-04-10','object_list.php'),(87,4,1,'2013-04-10','object_load.php'),(88,4,1,'2013-04-10','general'),(89,4,1,'2013-04-10','general'),(90,4,1,'2013-04-10','add_object_files.php'),(91,4,1,'2013-04-10','index.php'),(92,4,1,'2013-04-10','admin'),(93,4,1,'2013-04-10','user_view.php'),(94,4,1,'2013-04-10','user_edit.php'),(95,4,1,'2013-04-10','search.php'),(96,4,1,'2013-04-10','object_list.php'),(97,1,1,'2013-04-10','object_view.php'),(98,1,1,'2013-04-10','object_list.php'),(99,1,1,'2013-04-10','object_export_lom.php'),(100,1,1,'2013-04-10','index.php'),(101,1,1,'2013-04-10','search.php'),(102,1,1,'2013-04-10','search_result.php'),(103,1,1,'2013-04-10','search.php'),(104,1,1,'2013-04-10','object_view.php'),(105,1,1,'2013-04-10','index.php'),(106,1,1,'2013-04-10','object_list.php'),(107,5,1,'2013-04-10','object_view.php'),(108,5,1,'2013-04-10','object_load.php'),(109,5,1,'2013-04-10','general'),(110,5,1,'2013-04-10','general'),(111,5,1,'2013-04-10','general'),(112,5,1,'2013-04-10','general'),(113,5,1,'2013-04-10','object_save.php'),(114,5,1,'2013-04-10','object_load.php'),(115,5,1,'2013-04-10','general'),(116,5,1,'2013-04-10','identifier&parent=2026'),(117,5,1,'2013-04-10','general'),(118,5,1,'2013-04-10','general'),(119,5,1,'2013-04-10','add_object_files.php'),(120,5,1,'2013-04-10','general'),(121,5,1,'2013-04-10','general'),(122,5,1,'2013-04-10','general'),(123,5,1,'2013-04-10','general'),(124,5,1,'2013-04-10','general'),(125,5,1,'2013-04-10','add_object_files.php'),(126,5,1,'2013-04-10','index.php'),(127,5,1,'2013-04-10','admin'),(128,5,1,'2013-04-10','user_view.php'),(129,5,1,'2013-04-10','object_list.php'),(130,5,1,'2013-04-10','object_load.php'),(131,5,1,'2013-04-10','general'),(132,5,1,'2013-04-10','general'),(133,5,1,'2013-04-10','add_object_files.php'),(134,5,1,'2013-04-10','general'),(135,5,1,'2013-04-10','general'),(136,5,1,'2013-04-10','object_save.php'),(137,5,1,'2013-04-10','object_load.php'),(138,5,1,'2013-04-10','general'),(139,5,1,'2013-04-10','general'),(140,5,1,'2013-04-10','object_save.php'),(141,5,1,'2013-04-10','object_load.php'),(142,5,1,'2013-04-10','general'),(143,5,1,'2013-04-10','object_list.php'),(144,5,1,'2013-04-10','object_load.php'),(145,5,1,'2013-04-10','general'),(146,5,1,'2013-04-10','object_list.php'),(147,5,1,'2013-04-10','object_view.php'),(148,5,1,'2013-04-10','index.php'),(149,5,1,'2013-04-10','edit_version.php'),(150,5,1,'2013-04-10','public'),(151,5,1,'2013-04-10','admin'),(152,5,1,'2013-04-10','object_list.php'),(153,5,1,'2013-04-10','object_load.php'),(154,5,1,'2013-04-10','general'),(155,5,1,'2013-04-10','general'),(156,5,1,'2013-04-10','general'),(157,5,1,'2013-04-10','general'),(158,5,1,'2013-04-10','object_save.php'),(159,5,1,'2013-04-10','object_save.php'),(160,5,1,'2013-04-10','object_save.php'),(161,5,1,'2013-04-10','object_save.php'),(162,5,1,'2013-04-10','object_save.php'),(163,5,1,'2013-04-10','object_load.php'),(164,5,1,'2013-04-10','general'),(165,5,-1,'2013-04-10','object_view.php'),(166,5,-1,'2013-04-10','object_export_lom.php'),(167,5,-1,'2013-04-10','login.php'),(168,5,1,'2013-04-10','user'),(169,5,1,'2013-04-10','admin'),(170,5,1,'2013-04-10','object_list.php'),(171,5,1,'2013-04-10','object_load.php'),(172,5,1,'2013-04-10','general'),(173,5,1,'2013-04-10','general'),(174,5,1,'2013-04-10','general'),(175,5,1,'2013-04-10','general'),(176,5,1,'2013-04-10','add_object_files.php'),(177,5,1,'2013-04-10','index.php'),(178,5,1,'2013-04-10','admin'),(179,5,1,'2013-04-10','object_list.php'),(180,5,1,'2013-04-10','object_view.php'),(181,5,1,'2013-04-10','object_list.php'),(182,5,1,'2013-04-10','object_view.php'),(183,5,1,'2013-04-10','add_com.php'),(184,5,1,'2013-04-10','object_view.php'),(185,5,1,'2013-04-10','index.php'),(186,5,1,'2013-04-10','search.php'),(187,5,1,'2013-04-10','object_list.php'),(188,5,1,'2013-04-10','object_view.php'),(189,5,1,'2013-04-10','object_list.php'),(190,5,1,'2013-04-10','object_export_lom.php'),(191,5,1,'2013-04-10','index.php'),(192,5,1,'2013-04-10','search.php'),(193,5,1,'2013-04-10','object_list.php'),(194,6,1,'2013-04-10','object_load.php'),(195,6,1,'2013-04-10','general'),(196,6,1,'2013-04-10','general'),(197,6,1,'2013-04-10','add_object_files.php'),(198,6,1,'2013-04-10','index.php'),(199,6,1,'2013-04-10','admin'),(200,6,1,'2013-04-10','search.php'),(201,6,1,'2013-04-10','object_view.php'),(202,6,1,'2013-04-10','search.php'),(203,6,1,'2013-04-10','index.php'),(204,6,1,'2013-04-10','object_list.php'),(205,6,1,'2013-04-10','object_view.php'),(206,6,1,'2013-04-10','object_list.php'),(207,6,1,'2013-04-10','object_load.php'),(208,6,1,'2013-04-10','general'),(209,6,1,'2013-04-10','general'),(210,6,1,'2013-04-10','add_object_files.php'),(211,6,1,'2013-04-10','general'),(212,6,1,'2013-04-10','catalog'),(213,6,1,'2013-04-10','object_list.php'),(214,7,43,'2013-04-10','object_load.php'),(215,7,43,'2013-04-10','general'),(216,7,43,'2013-04-10','general'),(217,7,43,'2013-04-10','add_object_files.php'),(218,7,43,'2013-04-10','index.php'),(219,7,43,'2013-04-10','object_view.php'),(220,5,1,'2013-04-10','object_load.php'),(221,5,1,'2013-04-10','general'),(222,5,1,'2013-04-10','general'),(223,5,1,'2013-04-10','object_save.php'),(224,5,1,'2013-04-10','object_load.php'),(225,5,1,'2013-04-10','general'),(226,5,1,'2013-04-10','object_list.php'),(227,1,1,'2013-04-10','object_load.php'),(228,1,1,'2013-04-10','general'),(229,1,1,'2013-04-10','general'),(230,1,1,'2013-04-10','object_save.php'),(231,1,1,'2013-04-10','object_view.php'),(232,7,1,'2013-04-10','object_load.php'),(233,7,1,'2013-04-10','general'),(234,7,1,'2013-04-10','general'),(235,7,1,'2013-04-10','object_save.php'),(236,8,1,'2013-04-10','object_export_lom.php'),(237,8,1,'2013-04-10','object_load.php'),(238,8,1,'2013-04-10','general'),(239,8,1,'2013-04-10','general'),(240,8,1,'2013-04-10','object_list.php'),(241,7,1,'2013-04-10','object_load.php'),(242,7,1,'2013-04-10','general'),(243,7,1,'2013-04-10','general'),(244,7,1,'2013-04-10','general'),(245,7,1,'2013-04-10','facet&parent=6115'),(246,7,1,'2013-04-10','technical'),(247,7,1,'2013-04-10','facet4510'),(248,7,1,'2013-04-10','technical'),(249,7,1,'2013-04-10','technical'),(250,7,1,'2013-04-10','technical'),(251,7,1,'2013-04-10','description&parent=6144'),(252,7,1,'2013-04-10','rights'),(253,7,1,'2013-04-10','rights'),(254,7,1,'2013-04-10','identifier&parent=6150'),(255,7,1,'2013-04-10','relation'),(256,7,1,'2013-04-10','entry'),(257,7,1,'2013-04-10','relation'),(258,7,1,'2013-04-10','relation'),(259,7,1,'2013-04-10','relation'),(260,7,1,'2013-04-10','relation'),(261,7,1,'2013-04-10','relation'),(262,7,1,'2013-04-10','relation'),(263,7,1,'2013-04-10','relation'),(264,7,1,'2013-04-10','relation'),(265,7,1,'2013-04-10','object_save.php'),(266,7,1,'2013-04-10','object_view_com.php'),(267,7,1,'2013-04-10','object_list.php'),(268,1,1,'2013-04-10','object_load.php'),(269,1,1,'2013-04-10','general'),(270,1,1,'2013-04-10','general'),(271,1,1,'2013-04-10','general'),(272,1,1,'2013-04-10','general'),(273,1,1,'2013-04-10','general'),(274,1,1,'2013-04-10','general'),(275,1,1,'2013-04-10','object_list.php'),(276,8,1,'2013-04-10','object_view.php'),(277,8,1,'2013-04-10','object_list.php'),(278,8,1,'2013-04-10','object_load.php'),(279,8,1,'2013-04-10','general'),(280,8,1,'2013-04-10','general'),(281,8,1,'2013-04-10','object_list.php'),(282,8,1,'2013-04-10','object_load.php'),(283,8,1,'2013-04-10','general'),(284,8,1,'2013-04-10','general'),(285,8,1,'2013-04-10','general'),(286,8,1,'2013-04-10','object_list.php'),(287,7,1,'2013-04-10','object_load.php'),(288,7,1,'2013-04-10','general'),(289,7,1,'2013-04-10','general'),(290,7,1,'2013-04-10','object_list.php'),(291,8,1,'2013-04-11','object_load.php'),(292,8,1,'2013-04-11','general'),(293,8,1,'2013-04-11','general'),(294,8,1,'2013-04-11','taxonpath'),(295,8,1,'2013-04-11','taxon&parent=4678'),(296,8,1,'2013-04-11','classification'),(297,8,1,'2013-04-11','classification'),(298,8,1,'2013-04-11','add_object_files.php'),(299,8,1,'2013-04-11','index.php'),(300,8,1,'2013-04-11','admin'),(301,8,1,'2013-04-11','object_list.php'),(302,8,1,'2013-04-11','object_load.php'),(303,8,1,'2013-04-11','general'),(304,8,1,'2013-04-11','general'),(305,8,1,'2013-04-11','add_object_files.php'),(306,8,1,'2013-04-11','classification'),(307,8,1,'2013-04-11','classification'),(308,8,1,'2013-04-11','classification'),(309,8,1,'2013-04-11','classification'),(310,8,1,'2013-04-11','public'),(311,8,1,'2013-04-11','login.php'),(312,8,1,'2013-04-11','user'),(313,8,1,'2013-04-11','admin'),(314,8,1,'2013-04-11','search.php'),(315,8,1,'2013-04-11','object_list.php'),(316,8,1,'2013-04-11','object_load.php'),(317,8,1,'2013-04-11','general'),(318,8,1,'2013-04-11','object_load.php'),(319,8,1,'2013-04-11','general'),(320,8,1,'2013-04-11','general'),(321,8,1,'2013-04-11','general'),(322,8,1,'2013-04-11','object_save.php'),(323,6,1,'2013-04-11','object_view.php'),(324,6,1,'2013-04-11','object_view.php'),(325,6,1,'2013-04-11','object_list.php'),(326,6,1,'2013-04-11','object_view_com.php'),(327,6,1,'2013-04-11','public'),(328,6,1,'2013-04-11','login.php'),(329,6,43,'2013-04-11','user'),(330,9,43,'2013-04-11','object_delete.php'),(331,7,43,'2013-04-11','object_load.php'),(332,7,43,'2013-04-11','general'),(333,7,43,'2013-04-11','general'),(334,7,43,'2013-04-11','add_object_files.php'),(335,7,43,'2013-04-11','index.php'),(336,7,43,'2013-04-11','object_load.php'),(337,7,43,'2013-04-11','general'),(338,7,43,'2013-04-11','general'),(339,7,43,'2013-04-11','add_object_files.php'),(340,7,43,'2013-04-11','index.php'),(341,7,43,'2013-04-11','object_load.php'),(342,7,43,'2013-04-11','general'),(343,7,43,'2013-04-11','general'),(344,7,43,'2013-04-11','add_object_files.php'),(345,7,43,'2013-04-11','general'),(346,7,43,'2013-04-11','index.php'),(347,3,1,'2013-04-11','object_load.php'),(348,3,1,'2013-04-11','general'),(349,3,1,'2013-04-11','general'),(350,7,43,'2013-04-11','object_load.php'),(351,7,43,'2013-04-11','general'),(352,7,43,'2013-04-11','general'),(353,7,43,'2013-04-11','general'),(354,7,43,'2013-04-11','general'),(355,7,43,'2013-04-11','index.php'),(356,7,43,'2013-04-11','object_load.php'),(357,7,43,'2013-04-11','general'),(358,7,43,'2013-04-11','general'),(359,7,43,'2013-04-11','general'),(360,7,43,'2013-04-11','object_save.php'),(361,7,43,'2013-04-11','object_load.php'),(362,7,43,'2013-04-11','general'),(363,7,43,'2013-04-11','general'),(364,7,43,'2013-04-11','general'),(365,7,43,'2013-04-11','user'),(366,2,1,'2013-04-11','object_load.php'),(367,2,1,'2013-04-11','general'),(368,2,1,'2013-04-11','general'),(369,2,1,'2013-04-11','object_save.php'),(370,3,1,'2013-04-11','object_load.php'),(371,3,1,'2013-04-11','general'),(372,3,1,'2013-04-11','general'),(373,3,1,'2013-04-11','object_save.php'),(374,8,1,'2013-04-11','object_load.php'),(375,8,1,'2013-04-11','general'),(376,8,1,'2013-04-11','general'),(377,8,1,'2013-04-11','object_save.php'),(378,8,1,'2013-04-11','object_delete.php'),(379,4,1,'2013-04-11','object_delete.php'),(380,2,-1,'2013-04-11','object_view.php'),(381,3,-1,'2013-04-11','object_view.php'),(382,3,-1,'2013-04-11','login.php'),(383,3,43,'2013-04-11','user'),(384,7,43,'2013-04-11','object_view.php'),(385,7,43,'2013-04-11','general'),(386,7,43,'2013-04-11','object_load.php'),(387,7,43,'2013-04-11','general'),(388,7,43,'2013-04-11','general'),(389,7,43,'2013-04-11','general'),(390,7,43,'2013-04-11','general'),(391,7,43,'2013-04-11','general'),(392,7,43,'2013-04-11','general'),(393,7,43,'2013-04-11','general'),(394,7,43,'2013-04-11','general'),(395,7,43,'2013-04-11','general'),(396,7,43,'2013-04-11','general'),(397,7,43,'2013-04-11','index.php'),(398,10,43,'2013-04-11','object_load.php'),(399,10,43,'2013-04-11','general'),(400,10,43,'2013-04-11','general'),(401,10,43,'2013-04-11','add_object_files.php'),(402,10,43,'2013-04-11','index.php'),(403,10,43,'2013-04-11','object_view.php'),(404,10,43,'2013-04-11','index.php'),(405,10,43,'2013-04-11','object_view.php'),(406,10,43,'2013-04-11','index.php'),(407,7,43,'2013-04-11','object_view.php'),(408,7,43,'2013-04-11','index.php'),(409,6,-1,'2013-04-11','object_view.php'),(410,11,43,'2013-04-11','object_load.php'),(411,11,43,'2013-04-11','general'),(412,11,43,'2013-04-11','general'),(413,11,43,'2013-04-11','add_object_files.php'),(414,11,43,'2013-04-11','index.php'),(415,11,43,'2013-04-11','object_view.php'),(416,6,-1,'2013-04-11','object_view.php'),(417,6,-1,'2013-04-11','search_result.php'),(418,11,43,'2013-04-11','index.php'),(419,11,43,'2013-04-11','object_export_lom.php'),(420,6,-1,'2013-04-11','search_result.php'),(421,6,-1,'2013-04-11','search_result.php'),(422,6,-1,'2013-04-11','search_result.php'),(423,6,-1,'2013-04-11','search_advanced.php'),(424,6,-1,'2013-04-11','search_result.php'),(425,6,-1,'2013-04-11','article.php'),(426,6,-1,'2013-04-11','article.php'),(427,6,-1,'2013-04-11','article.php'),(428,6,-1,'2013-04-11','article.php'),(429,6,-1,'2013-04-11','article.php'),(430,6,-1,'2013-04-11','search_advanced.php'),(431,6,-1,'2013-04-11','search_advanced.php'),(432,6,-1,'2013-04-11','login.php'),(433,6,1,'2013-04-11','user'),(434,6,1,'2013-04-11','admin'),(435,3,1,'2013-04-11','general'),(436,6,1,'2013-04-11','search.php'),(437,6,1,'2013-04-11','object_list.php'),(438,3,1,'2013-04-11','public'),(439,3,1,'2013-04-11','login.php'),(440,3,1,'2013-04-11','user'),(441,3,1,'2013-04-11','admin'),(442,3,1,'2013-04-11','search.php'),(443,3,1,'2013-04-11','object_list.php'),(444,10,1,'2013-04-11','object_load.php'),(445,10,1,'2013-04-11','general'),(446,10,1,'2013-04-11','general'),(447,10,1,'2013-04-11','add_object_files.php'),(448,10,1,'2013-04-11','general'),(449,10,1,'2013-04-11','general'),(450,10,1,'2013-04-11','object_save.php'),(451,11,43,'2013-04-11','general'),(452,12,43,'2013-04-11','object_view.php'),(453,12,43,'2013-04-11','object_view.php'),(454,12,43,'2013-04-11','index.php'),(455,12,43,'2013-04-11','object_delete.php'),(456,11,43,'2013-04-11','object_load.php'),(457,11,43,'2013-04-11','general'),(458,11,43,'2013-04-11','general'),(459,11,43,'2013-04-11','add_object_files.php'),(460,11,43,'2013-04-11','index.php'),(461,11,43,'2013-04-11','object_load.php'),(462,11,43,'2013-04-11','general'),(463,11,43,'2013-04-11','general'),(464,11,43,'2013-04-11','object_save.php'),(465,11,43,'2013-04-11','object_export_lom.php'),(466,11,43,'2013-04-11','object_view.php'),(467,11,43,'2013-04-11','index.php'),(468,11,43,'2013-04-11','object_view.php'),(469,11,43,'2013-04-11','view_new_objects.php'),(470,11,43,'2013-04-11','find_new_objects.php'),(471,11,43,'2013-04-11','view_new_objects.php'),(472,11,43,'2013-04-11','object_view.php'),(473,11,43,'2013-04-11','index.php'),(474,12,43,'2013-04-11','public'),(475,12,43,'2013-04-11','login.php'),(476,12,1,'2013-04-11','user'),(477,12,1,'2013-04-11','admin'),(478,12,1,'2013-04-11','object_list.php'),(479,1,1,'2013-04-11','object_load.php'),(480,1,1,'2013-04-11','general'),(481,1,1,'2013-04-11','general'),(482,13,43,'2013-04-11','object_load.php'),(483,13,43,'2013-04-11','general'),(484,13,43,'2013-04-11','general'),(485,13,43,'2013-04-11','add_object_files.php'),(486,13,43,'2013-04-11','index.php'),(487,13,43,'2013-04-11','object_view.php'),(488,13,43,'2013-04-11','index.php'),(489,10,43,'2013-04-11','object_view.php'),(490,14,43,'2013-04-11','object_load.php'),(491,14,43,'2013-04-11','general'),(492,14,43,'2013-04-11','general'),(493,14,43,'2013-04-11','add_object_files.php'),(494,14,43,'2013-04-11','index.php'),(495,10,43,'2013-04-11','object_zip_upload.php'),(496,10,43,'2013-04-11','index.php'),(497,15,43,'2013-04-11','view_new_objects.php'),(498,15,43,'2013-04-11','find_new_objects.php'),(499,15,43,'2013-04-11','view_new_objects.php'),(500,15,43,'2013-04-11','object_zip_upload_action.php'),(501,500,43,'2013-04-11','search.php'),(502,500,43,'2013-04-11','object_zip_upload.php'),(503,500,43,'2013-04-11','index.php'),(504,15,43,'2013-04-11','object_view.php'),(505,15,43,'2013-04-11','index.php'),(506,15,43,'2013-04-11','object_load.php'),(507,15,43,'2013-04-11','general'),(508,15,43,'2013-04-11','index.php'),(509,15,43,'2013-04-11','object_view.php'),(510,1,1,'2013-04-11','object_list.php'),(511,15,43,'2013-04-11','object_showfile.php'),(512,15,43,'2013-04-11','index.php'),(513,15,43,'2013-04-11','object_load.php'),(514,15,43,'2013-04-11','general'),(515,1,1,'2013-04-11','object_load.php'),(516,1,1,'2013-04-11','general'),(517,1,1,'2013-04-11','general'),(518,1,1,'2013-04-11','general'),(519,15,43,'2013-04-11','general'),(520,15,43,'2013-04-11','general'),(521,15,43,'2013-04-11','object_save.php'),(522,15,43,'2013-04-11','object_view.php'),(523,15,43,'2013-04-11','index.php'),(524,1,1,'2013-04-11','object_list.php'),(525,10,43,'2013-04-11','object_load.php'),(526,10,43,'2013-04-11','general'),(527,10,43,'2013-04-11','general'),(528,10,43,'2013-04-11','object_save.php'),(529,14,43,'2013-04-11','object_view.php'),(530,6,1,'2013-04-11','object_view.php'),(531,6,43,'2013-04-11','object_view.php'),(532,6,43,'2013-04-11','search_advanced.php'),(533,6,43,'2013-04-11','search_result.php'),(534,6,43,'2013-04-11','search_advanced.php'),(535,6,1,'2013-04-11','search_advanced.php'),(536,6,43,'2013-04-11','login.php'),(537,6,1,'2013-04-11','user'),(538,6,1,'2013-04-11','admin'),(539,6,1,'2013-04-11','user'),(540,6,1,'2013-04-11','admin'),(541,6,1,'2013-04-11','object_list.php'),(542,6,1,'2013-04-11','object_list.php'),(543,1,1,'2013-04-11','object_load.php'),(544,1,1,'2013-04-11','general'),(545,1,1,'2013-04-11','general'),(546,1,1,'2013-04-11','general'),(547,1,1,'2013-04-11','general'),(548,1,1,'2013-04-11','general'),(549,10,43,'2013-04-11','object_load.php'),(550,10,43,'2013-04-11','general'),(551,1,1,'2013-04-11','general'),(552,10,43,'2013-04-11','general'),(553,1,1,'2013-04-11','general'),(554,1,1,'2013-04-11','general'),(555,10,43,'2013-04-11','general'),(556,10,43,'2013-04-11','object_save.php'),(557,1,1,'2013-04-11','general'),(558,1,1,'2013-04-11','general'),(559,7,43,'2013-04-11','object_load.php'),(560,7,43,'2013-04-11','general'),(561,7,43,'2013-04-11','general'),(562,7,43,'2013-04-11','general'),(563,7,43,'2013-04-11','general'),(564,1,1,'2013-04-11','object_list.php'),(565,7,43,'2013-04-11','general'),(566,7,43,'2013-04-11','object_save.php'),(567,1,1,'2013-04-11','object_view.php'),(568,10,43,'2013-04-11','object_load.php'),(569,10,43,'2013-04-11','general'),(570,1,1,'2013-04-11','add_com.php'),(571,1,1,'2013-04-11','object_view.php'),(572,10,43,'2013-04-11','general'),(573,10,43,'2013-04-11','general'),(574,11,43,'2013-04-11','object_load.php'),(575,11,43,'2013-04-11','general'),(576,10,43,'2013-04-11','general'),(577,10,43,'2013-04-11','general'),(578,1,1,'2013-04-11','classificator_list.php'),(579,11,43,'2013-04-11','general'),(580,10,43,'2013-04-11','general'),(581,11,43,'2013-04-11','general'),(582,1,1,'2013-04-11','object_list.php'),(583,15,1,'2013-04-11','object_load.php'),(584,15,1,'2013-04-11','general'),(585,10,43,'2013-04-11','general'),(586,11,43,'2013-04-11','general'),(587,11,43,'2013-04-11','object_save.php'),(588,10,43,'2013-04-11','general'),(589,13,43,'2013-04-11','object_load.php'),(590,15,1,'2013-04-11','general'),(591,13,43,'2013-04-11','general'),(592,13,43,'2013-04-11','general'),(593,10,43,'2013-04-11','general'),(594,13,43,'2013-04-11','general'),(595,13,43,'2013-04-11','general'),(596,6,43,'2013-04-11','object_view.php'),(597,10,43,'2013-04-11','general'),(598,13,43,'2013-04-11','general'),(599,10,43,'2013-04-11','general'),(600,10,43,'2013-04-11','general'),(601,10,43,'2013-04-11','general'),(602,6,43,'2013-04-11','search_result.php'),(603,13,43,'2013-04-11','general'),(604,13,43,'2013-04-11','object_save.php'),(605,15,1,'2013-04-11','general'),(606,15,1,'2013-04-11','object_save.php'),(607,6,43,'2013-04-11','search_advanced.php'),(608,14,43,'2013-04-11','object_load.php'),(609,14,43,'2013-04-11','general'),(610,14,43,'2013-04-11','general'),(611,14,43,'2013-04-11','general'),(612,14,43,'2013-04-11','general'),(613,6,43,'2013-04-11','search_result.php'),(614,14,43,'2013-04-11','general'),(615,14,43,'2013-04-11','object_save.php'),(616,15,43,'2013-04-11','object_load.php'),(617,15,43,'2013-04-11','general'),(618,15,43,'2013-04-11','general'),(619,15,43,'2013-04-11','general'),(620,15,43,'2013-04-11','general'),(621,15,43,'2013-04-11','general'),(622,15,43,'2013-04-11','object_save.php'),(623,6,43,'2013-04-11','search_result.php'),(624,14,43,'2013-04-11','object_view.php'),(625,10,1,'2013-04-11','object_load.php'),(626,10,1,'2013-04-11','general'),(627,14,43,'2013-04-11','search_advanced.php'),(628,10,1,'2013-04-11','general'),(629,10,1,'2013-04-11','general'),(630,10,43,'2013-04-11','general'),(631,14,43,'2013-04-11','search_result.php'),(632,10,43,'2013-04-11','general'),(633,14,43,'2013-04-11','search_advanced.php'),(634,14,43,'2013-04-11','search_result.php'),(635,6,43,'2013-04-11','object_view.php'),(636,6,43,'2013-04-11','search_result.php'),(637,6,43,'2013-04-11','search_result.php'),(638,10,1,'2013-04-11','search.php'),(639,6,43,'2013-04-11','object_view.php'),(640,10,1,'2013-04-11','index.php'),(641,10,1,'2013-04-11','search.php'),(642,1,1,'2013-04-11','object_load.php'),(643,1,1,'2013-04-11','general'),(644,10,1,'2013-04-11','index.php'),(645,1,1,'2013-04-11','general'),(646,10,1,'2013-04-11','edit_version.php'),(647,10,43,'2013-04-11','general'),(648,1,1,'2013-04-11','general'),(649,1,1,'2013-04-11','object_save.php'),(650,10,43,'2013-04-11','general'),(651,10,43,'2013-04-11','general'),(652,10,43,'2013-04-11','general'),(653,10,43,'2013-04-11','general'),(654,10,43,'2013-04-11','general'),(655,10,1,'2013-04-11','public'),(656,10,43,'2013-04-11','general'),(657,10,43,'2013-04-11','object_save.php'),(658,10,43,'2013-04-11','object_load.php'),(659,10,43,'2013-04-11','general'),(660,10,43,'2013-04-11','index.php'),(661,5,1,'2013-04-11','object_load.php'),(662,5,1,'2013-04-11','general'),(663,5,1,'2013-04-11','general'),(664,5,1,'2013-04-11','general'),(665,10,1,'2013-04-11','login.php'),(666,10,1,'2013-04-11','user'),(667,10,1,'2013-04-11','admin'),(668,5,1,'2013-04-11','general'),(669,5,1,'2013-04-11','object_save.php'),(670,10,1,'2013-04-11','search.php'),(671,10,1,'2013-04-11','object_list.php'),(672,3,1,'2013-04-11','object_load.php'),(673,3,1,'2013-04-11','general'),(674,3,1,'2013-04-11','general'),(675,3,1,'2013-04-11','add_object_files.php'),(676,3,1,'2013-04-11','index.php'),(677,3,1,'2013-04-11','admin'),(678,3,1,'2013-04-11','search.php'),(679,3,1,'2013-04-11','search_result.php'),(680,3,1,'2013-04-11','search_result.php'),(681,3,1,'2013-04-11','index.php'),(682,3,1,'2013-04-11','object_list.php'),(683,6,1,'2013-04-11','object_view.php'),(684,6,1,'2013-04-11','object_list.php'),(685,5,1,'2013-04-11','object_export_lom.php'),(686,5,1,'2013-04-11','search.php'),(687,5,1,'2013-04-11','search_result.php'),(688,5,1,'2013-04-11','search.php'),(689,10,43,'2013-04-11','object_load.php'),(690,10,43,'2013-04-11','general'),(691,10,43,'2013-04-11','index.php'),(692,10,-1,'2013-04-11','object_view.php'),(693,10,-1,'2013-04-11','object_view.php'),(694,5,1,'2013-04-11','public'),(695,5,1,'2013-04-11','login.php'),(696,5,1,'2013-04-11','user'),(697,5,1,'2013-04-11','admin'),(698,14,1,'2013-04-11','object_load.php'),(699,14,1,'2013-04-11','general'),(700,14,1,'2013-04-11','object_list.php'),(701,17,1,'2013-04-11','object_load.php'),(702,17,1,'2013-04-11','general'),(703,17,1,'2013-04-11','general'),(704,17,1,'2013-04-11','add_object_files.php'),(705,17,1,'2013-04-11','index.php'),(706,17,1,'2013-04-11','admin'),(707,17,1,'2013-04-11','object_list.php'),(708,17,1,'2013-04-11','object_load.php'),(709,17,1,'2013-04-11','general'),(710,17,1,'2013-04-11','general'),(711,17,1,'2013-04-11','general'),(712,17,1,'2013-04-11','add_object_files.php'),(713,17,1,'2013-04-11','index.php'),(714,17,1,'2013-04-11','admin'),(715,17,1,'2013-04-11','object_list.php'),(716,17,1,'2013-04-11','object_load.php'),(717,17,1,'2013-04-11','general'),(718,17,1,'2013-04-11','general'),(719,17,1,'2013-04-11','general'),(720,17,1,'2013-04-11','add_object_files.php'),(721,17,1,'2013-04-11','general'),(722,17,1,'2013-04-11','general'),(723,17,1,'2013-04-11','object_save.php'),(724,17,1,'2013-04-11','object_load.php'),(725,17,1,'2013-04-11','general'),(726,17,1,'2013-04-11','general'),(727,17,1,'2013-04-11','add_object_files.php'),(728,17,1,'2013-04-11','index.php'),(729,17,1,'2013-04-11','admin'),(730,17,1,'2013-04-11','search.php'),(731,17,1,'2013-04-11','object_load.php'),(732,17,1,'2013-04-11','general'),(733,17,1,'2013-04-11','general'),(734,17,1,'2013-04-11','add_object_files.php'),(735,17,1,'2013-04-11','general'),(736,17,1,'2013-04-11','object_list.php'),(737,10,-1,'2013-04-11','object_view.php'),(738,10,-1,'2013-04-11','object_export_lom.php'),(739,5,-1,'2013-04-11','object_view.php'),(740,5,-1,'2013-04-11','object_view.php'),(741,5,-1,'2013-04-11','object_view.php'),(742,5,-1,'2013-04-11','login.php'),(743,5,-1,'2013-04-11','login.php'),(744,5,1,'2013-04-11','user'),(745,5,1,'2013-04-11','admin'),(746,1,1,'2013-04-11','object_view.php'),(747,1,1,'2013-04-11','object_list.php'),(748,1,1,'2013-04-11','object_load.php'),(749,1,1,'2013-04-11','general'),(750,1,1,'2013-04-11','general'),(751,1,1,'2013-04-11','add_object_files.php'),(752,1,1,'2013-04-11','index.php'),(753,1,1,'2013-04-11','admin'),(754,1,1,'2013-04-11','object_list.php'),(755,1,1,'2013-04-11','object_load.php'),(756,1,1,'2013-04-11','general'),(757,1,1,'2013-04-11','general'),(758,1,1,'2013-04-11','add_object_files.php'),(759,1,1,'2013-04-11','technical'),(760,1,1,'2013-04-11','technical'),(761,1,1,'2013-04-11','technical'),(762,1,1,'2013-04-11','technical'),(763,1,1,'2013-04-11','object_list.php'),(764,10,43,'2013-04-11','object_load.php'),(765,10,43,'2013-04-11','general'),(766,10,43,'2013-04-11','general'),(767,10,43,'2013-04-11','add_object_files.php'),(768,10,43,'2013-04-11','index.php'),(769,10,43,'2013-04-11','object_load.php'),(770,10,43,'2013-04-11','general'),(771,10,43,'2013-04-11','general'),(772,10,43,'2013-04-11','contribute&parent=6501'),(773,10,43,'2013-04-11','lifecycle'),(774,10,43,'2013-04-11','lifecycle'),(775,10,43,'2013-04-11','lifecycle'),(776,10,43,'2013-04-11','lifecycle'),(777,10,43,'2013-04-11','lifecycle'),(778,10,43,'2013-04-11','lifecycle'),(779,10,43,'2013-04-11','object_save.php'),(780,10,43,'2013-04-11','object_load.php'),(781,10,43,'2013-04-11','general'),(782,10,43,'2013-04-11','index.php'),(783,10,-1,'2013-04-11','object_view.php'),(784,17,1,'2013-04-16','object_view.php'),(785,17,1,'2013-04-16','object_list.php'),(786,5,1,'2013-04-16','object_view.php'),(787,5,1,'2013-04-16','status'),(788,5,1,'2013-04-16','catalog'),(789,5,1,'2013-04-16','object_list.php'),(790,2,1,'2013-04-16','object_load.php'),(791,2,1,'2013-04-16','general'),(792,2,1,'2013-04-16','general'),(793,2,1,'2013-04-16','general'),(794,17,-1,'2013-04-17','object_view.php'),(795,17,-1,'2013-04-17','login.php'),(796,17,1,'2013-04-17','user'),(797,17,1,'2013-04-17','admin'),(798,17,1,'2013-04-17','search.php'),(799,17,1,'2013-04-17','public'),(800,17,1,'2013-04-17','search_advanced.php'),(801,17,1,'2013-04-17','index.php'),(802,17,1,'2013-04-17','search.php'),(803,17,1,'2013-04-17','index.php'),(804,17,1,'2013-04-17','object_list.php'),(805,17,1,'2013-04-17','object_view.php'),(806,17,1,'2013-04-17','classificator_list.php'),(807,17,1,'2013-04-17','general'),(808,14,-1,'2013-05-13','object_view.php'),(809,6,-1,'2013-05-13','object_view.php'),(810,6,-1,'2013-05-13','search_result.php'),(811,6,-1,'2013-05-13','search_result.php'),(812,6,-1,'2013-05-13','search_result.php'),(813,6,-1,'2013-05-13','search_result.php'),(814,6,-1,'2013-05-13','search_result.php'),(815,14,-1,'2013-05-13','public'),(816,14,-1,'2013-05-13','search_advanced.php'),(817,14,-1,'2013-05-13','search_advanced.php'),(818,6,-1,'2013-05-13','search_result.php'),(819,14,-1,'2013-05-13','login.php'),(820,14,1,'2013-05-13','user'),(821,14,1,'2013-05-13','admin'),(822,14,1,'2013-05-13','public'),(823,6,-1,'2013-05-13','search_result.php'),(824,6,-1,'2013-05-13','search_result.php'),(825,14,1,'2013-05-13','search_result.php'),(826,6,-1,'2013-05-13','search_result.php'),(827,6,-1,'2013-05-13','search_result.php'),(828,14,1,'2013-05-13','search_result.php'),(829,6,-1,'2013-05-13','search_result.php'),(830,14,1,'2013-05-13','search_result.php'),(831,14,1,'2013-05-13','search_result.php'),(832,14,1,'2013-05-13','search_result.php'),(833,14,1,'2013-05-13','search_result.php'),(834,14,1,'2013-05-13','search_result.php'),(835,14,1,'2013-05-13','search_result.php'),(836,14,1,'2013-05-13','search_result.php'),(837,14,1,'2013-05-13','search_result.php'),(838,14,1,'2013-05-13','search_result.php'),(839,14,1,'2013-05-13','search_result.php'),(840,14,1,'2013-05-13','search_result.php'),(841,14,1,'2013-05-13','search_result.php'),(842,14,1,'2013-05-13','search_result.php'),(843,14,1,'2013-05-13','search_result.php'),(844,14,1,'2013-05-13','search_result.php'),(845,14,1,'2013-05-13','search_result.php'),(846,14,1,'2013-05-13','search_result.php'),(847,14,1,'2013-05-13','search_result.php'),(848,14,1,'2013-05-13','search_result.php'),(849,14,1,'2013-05-13','search_result.php'),(850,14,1,'2013-05-13','search_result.php'),(851,14,1,'2013-05-13','search_result.php'),(852,14,1,'2013-05-13','search_result.php'),(853,14,1,'2013-05-13','search_result.php'),(854,14,1,'2013-05-13','search_result.php'),(855,14,1,'2013-05-13','search_result.php'),(856,14,1,'2013-05-13','search_result.php'),(857,14,1,'2013-05-13','search_result.php'),(858,14,1,'2013-05-13','search_result.php'),(859,6,-1,'2013-05-13','search_result.php'),(860,14,1,'2013-05-13','search_result.php'),(861,14,1,'2013-05-13','search_result.php'),(862,14,1,'2013-05-13','search_result.php'),(863,14,1,'2013-05-13','search_result.php'),(864,14,1,'2013-05-13','search_result.php'),(865,14,1,'2013-05-13','search_result.php'),(866,14,1,'2013-05-13','search_result.php'),(867,14,1,'2013-05-13','search_result.php'),(868,14,1,'2013-05-13','search_result.php'),(869,14,1,'2013-05-13','search_result.php'),(870,6,-1,'2013-05-13','search_result.php'),(871,14,1,'2013-05-13','search_result.php'),(872,14,1,'2013-05-13','search_result.php'),(873,14,1,'2013-05-13','search_result.php'),(874,14,1,'2013-05-13','search_result.php'),(875,14,1,'2013-05-13','search_result.php'),(876,14,1,'2013-05-13','search_result.php'),(877,14,1,'2013-05-13','search_result.php'),(878,14,1,'2013-05-13','search_result.php'),(879,14,1,'2013-05-13','search_result.php'),(880,14,1,'2013-05-13','search_result.php'),(881,14,1,'2013-05-13','search_result.php'),(882,14,1,'2013-05-13','search_result.php'),(883,14,1,'2013-05-13','search_result.php'),(884,14,1,'2013-05-13','search_result.php'),(885,14,1,'2013-05-13','search_result.php'),(886,14,1,'2013-05-13','search_result.php'),(887,14,1,'2013-05-13','search_result.php'),(888,14,1,'2013-05-13','search_result.php'),(889,14,1,'2013-05-13','search_result.php'),(890,14,1,'2013-05-13','search_result.php'),(891,14,1,'2013-05-13','search_result.php'),(892,14,1,'2013-05-13','search_result.php'),(893,14,1,'2013-05-13','search_result.php'),(894,14,1,'2013-05-13','search_result.php'),(895,14,1,'2013-05-13','search_result.php'),(896,14,1,'2013-05-13','search_result.php'),(897,14,1,'2013-05-13','search_result.php'),(898,14,1,'2013-05-13','search_result.php'),(899,14,1,'2013-05-13','search_result.php'),(900,14,1,'2013-05-13','search_result.php'),(901,14,1,'2013-05-13','search_result.php'),(902,14,1,'2013-05-13','search_result.php'),(903,14,1,'2013-05-13','search_result.php'),(904,14,1,'2013-05-13','search_result.php'),(905,14,1,'2013-05-13','search_advanced.php'),(906,14,1,'2013-05-13','search_advanced.php'),(907,6,-1,'2013-05-13','search_advanced.php'),(908,6,-1,'2013-05-13','search_advanced.php'),(909,6,-1,'2013-05-13','search_advanced.php'),(910,6,-1,'2013-05-13','search_advanced.php'),(911,6,-1,'2013-05-13','search_advanced.php'),(912,14,1,'2013-05-13','search_advanced.php'),(913,14,1,'2013-05-13','search_advanced.php'),(914,14,1,'2013-05-13','search_result.php'),(915,6,-1,'2013-05-13','search_result.php'),(916,6,-1,'2013-05-13','search_result.php'),(917,6,-1,'2013-05-13','search_advanced.php'),(918,6,-1,'2013-05-13','search_result.php'),(919,6,-1,'2013-05-13','search_advanced.php'),(920,14,1,'2013-05-13','search_advanced.php'),(921,6,-1,'2013-05-13','search_result.php'),(922,6,-1,'2013-05-13','search_result.php'),(923,14,1,'2013-05-13','search_result.php'),(924,14,-1,'2013-05-13','object_view.php'),(925,14,1,'2013-05-13','object_view.php'),(926,14,-1,'2013-05-13','object_view.php'),(927,14,-1,'2013-05-13','object_view.php'),(928,6,1,'2013-05-13','object_view.php'),(929,6,1,'2013-05-13','object_view.php'),(930,6,1,'2013-05-13','login.php'),(931,6,1,'2013-05-13','user'),(932,6,1,'2013-05-13','admin'),(933,6,1,'2013-05-13','public'),(934,10,1,'2013-05-13','object_view.php'),(935,17,1,'2013-05-13','object_view.php'),(936,10,1,'2013-05-13','object_view.php'),(937,10,1,'2013-05-13','object_view.php'),(938,14,-1,'2013-05-13','object_view.php'),(939,14,-1,'2013-05-13','object_view.php'),(940,14,-1,'2013-05-13','object_view.php'),(941,6,-1,'2013-05-13','object_view.php'),(942,10,1,'2013-05-13','object_view.php'),(943,6,-1,'2013-05-13','object_view.php'),(944,14,-1,'2013-05-13','object_view.php'),(945,10,1,'2013-05-13','object_view.php'),(946,10,1,'2013-05-13','object_view.php'),(947,14,-1,'2013-05-13','object_view.php'),(948,10,1,'2013-05-13','admin'),(949,10,1,'2013-05-13','search.php'),(950,10,1,'2013-05-13','admin'),(951,10,1,'2013-05-13','search_result.php'),(952,14,-1,'2013-05-13','search_result.php'),(953,14,-1,'2013-05-13','search_result.php'),(954,6,1,'2013-05-13','object_view.php'),(955,6,1,'2013-05-13','object_view.php'),(956,6,1,'2013-05-13','object_view.php'),(957,14,-1,'2013-05-13','object_view.php'),(958,14,-1,'2013-05-13','login.php'),(959,14,-1,'2013-05-13','login.php'),(960,14,1,'2013-05-13','user'),(961,14,1,'2013-05-13','admin'),(962,14,1,'2013-05-13','search.php'),(963,14,1,'2013-05-13','search.php'),(964,14,1,'2013-05-13','search_advanced.php'),(965,14,1,'2013-05-13','search_result.php'),(966,14,1,'2013-05-13','search_result.php'),(967,14,1,'2013-05-13','search_result.php'),(968,14,1,'2013-05-13','login.php'),(969,14,1,'2013-05-13','login.php'),(970,14,1,'2013-05-13','user'),(971,14,1,'2013-05-13','admin'),(972,14,1,'2013-05-13','object_list.php'),(973,1,1,'2013-05-13','object_load.php'),(974,1,1,'2013-05-13','general'),(975,6,1,'2013-05-14','search_result.php'),(976,6,1,'2013-05-14','public'),(977,6,1,'2013-05-15','search_result.php'),(978,1,1,'2013-05-23','object_load.php'),(979,1,1,'2013-05-23','general'),(980,1,1,'2013-05-23','object_list.php'),(981,10,-1,'2013-05-30','object_view.php'),(982,10,-1,'2013-06-05','object_view.php'),(983,10,-1,'2013-06-05','login.php'),(984,10,-1,'2013-06-05','object_view.php'),(985,10,-1,'2013-06-05','object_export_lom.php'),(986,1,1,'2013-06-07','object_view.php'),(987,1,1,'2013-06-17','object_view.php'),(988,1,1,'2013-06-17','index.php'),(989,1,1,'2013-06-17','search.php'),(990,1,1,'2013-06-17','login.php'),(991,1,1,'2013-06-17','user'),(992,1,1,'2013-06-17','admin'),(993,1,1,'2013-06-17','object_list.php'),(994,14,1,'2013-06-17','object_view.php'),(995,14,1,'2013-06-17','add_rating.php'),(996,14,1,'2013-06-17','object_view.php'),(997,14,1,'2013-06-17','classificator_list.php'),(998,14,1,'2013-06-17','general'),(999,10,-1,'2013-06-20','object_view.php'),(1000,10,-1,'2013-06-20','search_result.php'),(1001,10,-1,'2013-06-20','search_advanced.php'),(1002,10,-1,'2013-06-20','article.php'),(1003,10,-1,'2013-06-20','article.php'),(1004,10,-1,'2013-06-20','index.php'),(1005,17,-1,'2013-06-20','object_view.php'),(1006,10,-1,'2013-06-20','object_view.php'),(1007,10,-1,'2013-06-20','help.php'),(1008,10,-1,'2013-06-20','search.php'),(1009,10,-1,'2013-06-20','index.php'),(1010,10,-1,'2013-06-20','index.php'),(1011,10,-1,'2013-06-20','login.php'),(1012,10,1,'2013-06-20','user'),(1013,10,1,'2013-06-20','admin'),(1014,10,1,'2013-06-20','search.php'),(1015,10,1,'2013-06-20','object_list.php'),(1016,13,1,'2013-06-21','object_view.php'),(1017,13,1,'2013-06-21','object_list.php'),(1018,16,1,'2013-06-21','object_view.php'),(1019,16,1,'2013-06-21','object_list.php'),(1020,17,-1,'2013-06-21','object_view.php'),(1021,17,-1,'2013-06-21','article.php'),(1022,19,1,'2013-06-21','object_view.php'),(1023,19,1,'2013-06-21','search.php'),(1024,19,1,'2013-06-21','index.php'),(1025,19,1,'2013-06-21','object_list.php'),(1026,18,1,'2013-06-21','object_view.php'),(1027,18,1,'2013-06-21','object_list.php'),(1028,19,1,'2013-06-21','object_view.php'),(1029,17,-1,'2013-06-21','login.php'),(1030,19,1,'2013-06-21','object_list.php'),(1031,17,-1,'2013-06-21','login.php'),(1032,17,1,'2013-06-21','user'),(1033,17,1,'2013-06-21','admin'),(1034,17,1,'2013-06-21','general'),(1035,20,-1,'2013-06-26','object_view.php'),(1036,20,-1,'2013-06-26','article.php'),(1037,20,-1,'2013-06-26','article.php'),(1038,20,-1,'2013-06-26','article.php'),(1039,20,-1,'2013-06-26','article.php'),(1040,20,-1,'2013-06-26','article.php'),(1041,20,-1,'2013-06-26','article.php'),(1042,20,-1,'2013-06-26','article.php'),(1043,20,-1,'2013-06-26','index.php'),(1044,18,-1,'2013-06-26','object_view.php'),(1045,18,-1,'2013-06-26','object_export_lom.php'),(1046,18,-1,'2013-06-26','object_export_lom.php'),(1047,19,1,'2013-07-01','object_load.php'),(1048,19,1,'2013-07-01','general'),(1049,19,1,'2013-07-01','general'),(1050,19,1,'2013-07-01','general'),(1051,19,1,'2013-07-01','add_object_files.php'),(1052,19,1,'2013-07-01','classification'),(1053,19,1,'2013-07-01','classification'),(1054,19,1,'2013-07-01','classification'),(1055,19,1,'2013-07-01','classification'),(1056,19,1,'2013-07-01','classification'),(1057,19,1,'2013-07-01','classification'),(1058,19,1,'2013-07-01','classification'),(1059,19,1,'2013-07-01','classification'),(1060,19,1,'2013-07-01','classification'),(1061,19,1,'2013-07-01','classification'),(1062,19,1,'2013-07-01','classification'),(1063,19,1,'2013-07-01','classification'),(1064,19,1,'2013-07-01','classification'),(1065,19,1,'2013-07-01','classification'),(1066,19,1,'2013-07-01','classification'),(1067,19,1,'2013-07-01','classification'),(1068,19,1,'2013-07-01','classification'),(1069,19,1,'2013-07-01','classification'),(1070,19,1,'2013-07-01','classification'),(1071,19,1,'2013-07-01','classification'),(1072,19,1,'2013-07-01','classification'),(1073,19,1,'2013-07-01','classification'),(1074,19,1,'2013-07-01','add_object_files.php'),(1075,19,1,'2013-07-01','relation'),(1076,20,1,'2013-07-09','object_view.php'),(1077,20,1,'2013-07-09','object_list.php'),(1078,6,1,'2013-07-09','object_view.php'),(1079,6,1,'2013-07-09','object_list.php'),(1080,21,1,'2013-07-09','object_view.php'),(1081,21,1,'2013-07-09','object_list.php'),(1082,18,1,'2013-07-09','object_load.php'),(1083,18,1,'2013-07-09','general'),(1084,18,1,'2013-07-09','object_list.php'),(1085,21,1,'2013-07-09','object_load.php'),(1086,21,1,'2013-07-09','general'),(1087,21,1,'2013-07-09','general'),(1088,21,1,'2013-07-09','general'),(1089,21,1,'2013-07-09','general'),(1090,21,1,'2013-07-09','object_save.php'),(1091,21,1,'2013-07-09','object_view.php'),(1092,21,1,'2013-07-09','object_view.php'),(1093,21,1,'2013-07-09','object_view.php'),(1094,21,1,'2013-07-09','lifecycle'),(1095,21,1,'2013-07-09','role'),(1096,21,1,'2013-07-10','general'),(1097,21,1,'2013-07-10','object_view.php'),(1098,21,1,'2013-07-10','general'),(1099,21,1,'2013-07-10','general'),(1100,21,1,'2013-07-10','general'),(1101,21,1,'2013-07-10','general'),(1102,21,1,'2013-07-10','general'),(1103,21,1,'2013-07-10','general'),(1104,21,1,'2013-07-10','object_list.php'),(1105,21,1,'2013-07-10','object_load.php'),(1106,21,1,'2013-07-10','general'),(1107,21,1,'2013-07-10','general'),(1108,21,1,'2013-07-10','general'),(1109,21,1,'2013-07-10','general'),(1110,21,1,'2013-07-10','general'),(1111,21,1,'2013-07-10','add_object_files.php'),(1112,21,1,'2013-07-10','classification'),(1113,21,1,'2013-07-10','classification'),(1114,21,1,'2013-07-10','classification'),(1115,21,1,'2013-07-10','classification'),(1116,21,1,'2013-07-10','classification'),(1117,21,1,'2013-07-10','classification'),(1118,21,1,'2013-07-10','classification'),(1119,21,1,'2013-07-10','object_save.php'),(1120,21,1,'2013-07-15','object_view.php'),(1121,21,1,'2013-07-15','object_view.php'),(1122,21,1,'2013-07-15','general'),(1123,22,1,'2013-07-16','object_delete.php'),(1124,23,1,'2013-07-16','object_delete.php'),(1125,1,1,'2013-07-16','object_delete.php'),(1126,24,1,'2013-07-16','object_delete.php'),(1127,21,-1,'2013-07-18','object_view.php'),(1128,21,-1,'2013-07-18','admin'),(1129,21,-1,'2013-07-18','public'),(1130,21,-1,'2013-07-18','index.php'),(1131,21,1,'2013-07-19','object_view.php'),(1132,21,1,'2013-07-19','object_list.php'),(1133,21,1,'2013-07-19','object_load.php'),(1134,21,1,'2013-07-19','general'),(1135,21,1,'2013-07-19','identifier&parent=5071'),(1136,21,1,'2013-07-19','general'),(1137,21,1,'2013-07-19','identifier&parent=5071'),(1138,21,1,'2013-07-19','general'),(1139,21,1,'2013-07-19','general'),(1140,21,1,'2013-07-19','object_save.php'),(1141,21,1,'2013-07-19','object_view.php'),(1142,21,1,'2013-07-19','identifier'),(1143,21,1,'2013-07-19','classificator_list.php'),(1144,21,1,'2013-07-19','search.php'),(1145,21,1,'2013-07-19','object_list.php'),(1146,21,1,'2013-07-19','object_load.php'),(1147,21,1,'2013-07-19','general'),(1148,21,1,'2013-07-19','general'),(1149,21,1,'2013-07-19','taxonpath&parent=6565'),(1150,21,1,'2013-07-19','classification'),(1151,21,1,'2013-07-19','taxonpath&parent=6565'),(1152,21,1,'2013-07-19','classification'),(1153,21,1,'2013-07-19','classification'),(1154,21,1,'2013-07-19','object_save.php'),(1155,21,1,'2013-07-19','object_load.php'),(1156,21,1,'2013-07-19','general'),(1157,21,1,'2013-07-19','general'),(1158,21,1,'2013-07-19','entry&parent=6941'),(1159,21,1,'2013-07-19','classification'),(1160,21,1,'2013-07-19','entry&parent=6941'),(1161,21,1,'2013-07-19','classification'),(1162,21,1,'2013-07-19','entry&parent=6569'),(1163,21,1,'2013-07-19','classification'),(1164,21,1,'2013-07-19','entry&parent=6569'),(1165,21,1,'2013-07-19','classification'),(1166,21,1,'2013-07-19','classification'),(1167,21,1,'2013-07-19','object_save.php'),(1168,21,1,'2013-07-19','object_load.php'),(1169,21,1,'2013-07-19','general'),(1170,21,1,'2013-07-19','general'),(1171,21,1,'2013-07-22','entry6571'),(1172,21,1,'2013-07-22','classification'),(1173,21,1,'2013-07-22','entry6943'),(1174,21,1,'2013-07-22','classification'),(1175,21,1,'2013-07-22','classification'),(1176,21,1,'2013-07-22','object_list.php'),(1177,21,1,'2013-07-22','object_view.php'),(1178,21,1,'2013-07-23','classification'),(1179,21,1,'2013-07-23','aggregationlevel'),(1180,21,1,'2013-07-23','classification'),(1181,21,1,'2013-07-23','classification'),(1182,21,1,'2013-07-23','classification'),(1183,21,1,'2013-07-23','classification'),(1184,21,1,'2013-07-23','classification'),(1185,21,1,'2013-07-23','classification'),(1186,21,1,'2013-07-23','classification'),(1187,21,1,'2013-07-23','classification'),(1188,21,1,'2013-07-23','object_list.php'),(1189,21,1,'2013-07-23','object_view.php'),(1190,21,1,'2013-07-23','facet'),(1191,21,1,'2013-07-23','general'),(1192,21,1,'2013-07-24','object_export_lom.php'),(1193,21,1,'2013-07-24','object_load.php'),(1194,21,1,'2013-07-24','general'),(1195,21,1,'2013-07-24','general'),(1196,21,1,'2013-07-24','object_save.php'),(1197,28,1,'2013-07-24','object_delete.php'),(1198,26,1,'2013-07-24','object_delete.php'),(1199,19,1,'2013-07-24','object_delete.php');
/*!40000 ALTER TABLE `object_view_counter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects`
--

DROP TABLE IF EXISTS `objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objects` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` int(10) unsigned DEFAULT '0',
  `file_data` mediumblob,
  `file_path` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `standart` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objects`
--

LOCK TABLES `objects` WRITE;
/*!40000 ALTER TABLE `objects` DISABLE KEYS */;
INSERT INTO `objects` VALUES (2,1,'2013-04-11 07:18:12',NULL,NULL,0,NULL,NULL,0),(3,1,'2013-04-11 07:18:59',NULL,NULL,0,NULL,NULL,0),(10,43,'2013-04-11 09:20:51',NULL,NULL,0,NULL,NULL,0),(5,1,'2013-04-11 08:32:11',NULL,NULL,0,NULL,NULL,0),(6,1,'2013-04-10 14:20:52',NULL,NULL,0,NULL,NULL,0),(7,43,'2013-04-11 08:27:09',NULL,NULL,0,NULL,NULL,0),(11,43,'2013-04-11 08:27:51',NULL,NULL,0,NULL,NULL,0),(13,43,'2013-04-11 08:28:27',NULL,NULL,0,NULL,NULL,0),(14,43,'2013-04-11 08:28:42',NULL,NULL,0,NULL,NULL,0),(15,43,'2013-04-11 08:29:12','ContentPackagingMetadata_SCORM20043rdEdition.zip','application/octet-stream',426663,'file_data','./../learning_objects/15/ContentPackagingMetadata_SCORM20043rdEdition.zip',0),(16,44,'2013-04-11 08:26:01',NULL,NULL,0,NULL,NULL,0),(17,1,'2013-04-11 08:58:13',NULL,NULL,0,NULL,NULL,0),(18,1,'2013-06-21 07:56:16',NULL,NULL,0,NULL,NULL,0),(31,1,'2013-07-24 05:37:35',NULL,NULL,0,NULL,NULL,0),(20,1,'2013-06-21 07:57:34',NULL,NULL,0,NULL,NULL,0),(21,1,'2013-07-24 05:31:46',NULL,NULL,0,NULL,NULL,0),(29,1,'2013-07-24 05:31:11',NULL,NULL,0,NULL,NULL,0),(30,1,'2013-07-24 05:35:02',NULL,NULL,0,NULL,NULL,0);
/*!40000 ALTER TABLE `objects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects_rating`
--

DROP TABLE IF EXISTS `objects_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objects_rating` (
  `object_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objects_rating`
--

LOCK TABLES `objects_rating` WRITE;
/*!40000 ALTER TABLE `objects_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `objects_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects_testingdata`
--

DROP TABLE IF EXISTS `objects_testingdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objects_testingdata` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` int(10) unsigned DEFAULT '0',
  `file_data` mediumblob,
  `file_path` varchar(255) CHARACTER SET ascii COLLATE ascii_bin DEFAULT NULL,
  `standart` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1585 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objects_testingdata`
--

LOCK TABLES `objects_testingdata` WRITE;
/*!40000 ALTER TABLE `objects_testingdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `objects_testingdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects_type`
--

DROP TABLE IF EXISTS `objects_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objects_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `objects_type`
--

LOCK TABLES `objects_type` WRITE;
/*!40000 ALTER TABLE `objects_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `objects_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `thread_id` int(10) unsigned NOT NULL,
  `editor_id` int(10) unsigned DEFAULT NULL,
  `content` text NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_post_author` (`author_id`),
  KEY `FK_post_editor` (`editor_id`),
  KEY `FK_post_thread` (`thread_id`),
  CONSTRAINT `FK_post_author` FOREIGN KEY (`author_id`) REFERENCES `forumuser` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_post_editor` FOREIGN KEY (`editor_id`) REFERENCES `forumuser` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_post_thread` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (1,1,1,NULL,'The first release is a fact!',0,0),(2,2,2,NULL,'This obviously can\'t be right.',0,0),(3,2,3,NULL,'When posting a new thread, the creation date is set to Jan 1, 1970 01:00:00 AM...',0,0),(4,2,3,NULL,'This should be fixed now!',2012,2012),(5,2,3,NULL,'Oops! Let\'s try that again...\r\nThis should be fixed now!',1349540442,1349540442),(6,2,4,NULL,'I believe it shows the the last thread instead...',1349540563,1349540563),(7,2,4,NULL,'Fixed!',1349561144,1349561144),(8,1,4,NULL,'Test reply',1349563344,1349563344),(9,1,4,NULL,'Another test reply, locking thread',1349563360,1349563360),(10,1,4,NULL,'Opps. Locking thread for real now',1349564868,1349564868),(11,1,3,NULL,'Thread locked, maybe',1349564945,1349632036),(12,1,5,NULL,'Allow users to define a signature, and add this to posts by them...',1349570366,1377422409),(13,1,6,NULL,'Add BB code support, and some sort of wysiwyg editor',1349570413,1349570413),(14,1,7,NULL,'Allow attachments to be added to posts\r\n\r\nSome *examples* of **markup**\r\n\r\ninline use of `code` is possible too!\r\n\r\nLet\'s see what a\r\n> blockquote looks like\r\nwithin a pargraph\r\n\r\n    [php showLineNumbers=1]\r\n    echo \'It can highlight code too!\';\r\n',1349578699,1349578699);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `catalog` varchar(10) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_migration`
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_migration`
--

LOCK TABLES `tbl_migration` WRITE;
/*!40000 ALTER TABLE `tbl_migration` DISABLE KEYS */;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base',1368000475),('m110805_153437_installYiiUser',1368000917);
/*!40000 ALTER TABLE `tbl_migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (1,'test1','pass1','test1@example.com'),(2,'test2','pass2','test2@example.com'),(3,'test3','pass3','test3@example.com'),(4,'test4','pass4','test4@example.com'),(5,'test5','pass5','test5@example.com'),(6,'test6','pass6','test6@example.com'),(7,'test7','pass7','test7@example.com'),(8,'test8','pass8','test8@example.com'),(9,'test9','pass9','test9@example.com'),(10,'test10','pass10','test10@example.com'),(11,'test11','pass11','test11@example.com'),(12,'test12','pass12','test12@example.com'),(13,'test13','pass13','test13@example.com'),(14,'test14','pass14','test14@example.com'),(15,'test15','pass15','test15@example.com'),(16,'test16','pass16','test16@example.com'),(17,'test17','pass17','test17@example.com'),(18,'test18','pass18','test18@example.com'),(19,'test19','pass19','test19@example.com'),(20,'test20','pass20','test20@example.com'),(21,'test21','pass21','test21@example.com');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thread`
--

DROP TABLE IF EXISTS `thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thread` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(10) unsigned NOT NULL,
  `subject` varchar(120) NOT NULL,
  `is_sticky` tinyint(1) unsigned NOT NULL,
  `is_locked` tinyint(1) unsigned NOT NULL,
  `view_count` bigint(20) unsigned NOT NULL,
  `created` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_thread_forum` (`forum_id`),
  CONSTRAINT `FK_thread_forum` FOREIGN KEY (`forum_id`) REFERENCES `forum` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thread`
--

LOCK TABLES `thread` WRITE;
/*!40000 ALTER TABLE `thread` DISABLE KEYS */;
INSERT INTO `thread` VALUES (1,2,'First release',1,0,27,0),(2,5,'Subject is allowed to be blank when creating a new thread',0,0,4,0),(3,5,'Post date is not set',0,1,16,0),(4,5,'Forum view does not show correct last post',0,1,10,1349540563),(5,6,'User signatures',0,0,7,1349570366),(6,6,'BB Code',0,0,1,1349570413),(7,5,'Attachments',0,0,21,1349578699);
/*!40000 ALTER TABLE `thread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(16) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `surname` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `address1` varchar(128) DEFAULT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `grp` char(1) DEFAULT 'u',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','asdasd','Sistemos','Administratorius','webmaster@serveris.info',NULL,NULL,NULL,'a'),(44,'vartotojas2','vartotojas2','Vartotojas2','Vartotojauskas2','as@as.lt',NULL,NULL,NULL,'u'),(43,'vartotojas','vartotojas','Vartotojas','Vartotojauskas','vartotojas@pastas.lt',NULL,NULL,NULL,'u');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_Rights`
--

DROP TABLE IF EXISTS `yii_Rights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_Rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`itemname`),
  CONSTRAINT `yii_Rights_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_Rights`
--

LOCK TABLES `yii_Rights` WRITE;
/*!40000 ALTER TABLE `yii_Rights` DISABLE KEYS */;
INSERT INTO `yii_Rights` VALUES ('Admin',2,2),('ContentManager',2,1),('CourseAdmin',2,4),('Guest',2,5),('Operator',2,6),('Student',2,0),('Tutor',2,8),('UserManager',2,3);
/*!40000 ALTER TABLE `yii_Rights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_comment`
--

DROP TABLE IF EXISTS `yii_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeId` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `comment` varchar(2000) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_comment`
--

LOCK TABLES `yii_comment` WRITE;
/*!40000 ALTER TABLE `yii_comment` DISABLE KEYS */;
INSERT INTO `yii_comment` VALUES (41,29,'course',31,'hell yeah x2','2013-07-25 09:31:31'),(67,13,'course',31,'na','2013-07-26 05:51:13'),(68,13,'course',31,'na na na na na batman','2013-07-26 05:51:17'),(69,29,'course',31,'na','2013-07-26 05:51:21'),(70,29,'course',31,'na','2013-07-26 05:51:25'),(71,31,'course',31,'batmaan','2013-07-26 05:51:30'),(73,30,'course',1,'nananananananananananananananananananananananananananananana batman','2013-07-26 07:20:08'),(74,13,'course',1,'sdfgdhdxfxv','2013-07-30 06:48:20'),(78,13,'course',36,'sadf','2013-07-30 07:33:12'),(80,13,'course',31,'sdasfdf','2013-07-30 09:34:56');
/*!40000 ALTER TABLE `yii_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_course_versions`
--

DROP TABLE IF EXISTS `yii_course_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_course_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `changes` text,
  PRIMARY KEY (`id`),
  KEY `course` (`cid`),
  CONSTRAINT `course` FOREIGN KEY (`cid`) REFERENCES `yii_courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_course_versions`
--

LOCK TABLES `yii_course_versions` WRITE;
/*!40000 ALTER TABLE `yii_course_versions` DISABLE KEYS */;
INSERT INTO `yii_course_versions` VALUES (63,1,99,'99'),(64,1,7,'777'),(65,22,4,'4'),(66,22,5,'5'),(67,23,444,'sss'),(68,24,888,'rt'),(69,24,4,'7777');
/*!40000 ALTER TABLE `yii_course_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_courses`
--

DROP TABLE IF EXISTS `yii_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `authors` varchar(255) DEFAULT NULL,
  `keywords` varchar(45) DEFAULT NULL,
  `versions` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `subject_areas` varchar(45) DEFAULT NULL,
  `curated` varchar(45) DEFAULT NULL,
  `rating` varchar(45) DEFAULT NULL,
  `popularity` varchar(45) DEFAULT NULL,
  `purpose` varchar(45) DEFAULT NULL,
  `anotation` varchar(45) DEFAULT NULL,
  `activated` varchar(45) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `updated` int(11) DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_courses`
--

LOCK TABLES `yii_courses` WRITE;
/*!40000 ALTER TABLE `yii_courses` DISABLE KEYS */;
INSERT INTO `yii_courses` VALUES (1,'','','',NULL,'',NULL,'','','','','','',NULL,NULL,NULL,NULL),(11,'new123','[{\"id\":5,\"shortname\":\"new123\"}]','cvbcvb','','cvb','','cvb','cvb','cvb','123','cvb','df',NULL,NULL,NULL,NULL),(12,'new15','','','','','','','','','','','',NULL,NULL,NULL,NULL),(13,'NKursas','','','','','','','','','','','',NULL,NULL,NULL,NULL),(14,'asdf','','','','','','','','','','','',NULL,NULL,NULL,NULL),(15,'asdf21','','','','','','','','','','','',NULL,NULL,NULL,NULL),(16,'tesssting','','','','','','','','','','','',NULL,NULL,NULL,NULL),(17,'asdfg','','','','','','','','','','','',NULL,NULL,NULL,NULL),(18,'sdfsadfasdf','','','','','','','','','','','',NULL,NULL,NULL,NULL),(19,'gfdsgdfgsdfg','','','','','','','','','','','',NULL,NULL,NULL,NULL),(20,'gfdsgdfgsdfgyujryjtyj','','','','','','','','','','','',NULL,NULL,NULL,NULL),(21,'sdfsdf','asdfasdf','dasfasdf',NULL,'',NULL,'','','','','','',NULL,NULL,NULL,NULL),(22,'new','','',NULL,'',NULL,'','','','','','',NULL,NULL,NULL,NULL),(23,'xxx','','',NULL,'',NULL,'','','','','','',NULL,NULL,NULL,NULL),(24,'bbb','','',NULL,'',NULL,'','','','5','','',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `yii_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_dalykines_sritys`
--

DROP TABLE IF EXISTS `yii_dalykines_sritys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_dalykines_sritys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` enum('lt','en') DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_dalykines_sritys`
--

LOCK TABLES `yii_dalykines_sritys` WRITE;
/*!40000 ALTER TABLE `yii_dalykines_sritys` DISABLE KEYS */;
INSERT INTO `yii_dalykines_sritys` VALUES (1,'lt','Dalykine sritis 1'),(2,'lt','Dalykine sritis 2'),(3,'lt','Dalykine sritis 3');
/*!40000 ALTER TABLE `yii_dalykines_sritys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_defratios`
--

DROP TABLE IF EXISTS `yii_defratios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_defratios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cId` int(11) NOT NULL,
  `ratio` int(11) DEFAULT NULL,
  `popularity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_defratios`
--

LOCK TABLES `yii_defratios` WRITE;
/*!40000 ALTER TABLE `yii_defratios` DISABLE KEYS */;
INSERT INTO `yii_defratios` VALUES (1,13,NULL,NULL);
/*!40000 ALTER TABLE `yii_defratios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_kompetencijos`
--

DROP TABLE IF EXISTS `yii_kompetencijos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_kompetencijos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` enum('lt','en') DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_kompetencijos`
--

LOCK TABLES `yii_kompetencijos` WRITE;
/*!40000 ALTER TABLE `yii_kompetencijos` DISABLE KEYS */;
INSERT INTO `yii_kompetencijos` VALUES (1,'lt','Kompetencija 1'),(2,'lt','Kompetencija 2'),(3,'lt','Kompetencija 3');
/*!40000 ALTER TABLE `yii_kompetencijos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_menu`
--

DROP TABLE IF EXISTS `yii_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `url` varchar(45) DEFAULT NULL,
  `weight` int(3) NOT NULL DEFAULT '0',
  `type` int(3) NOT NULL DEFAULT '1',
  `visible` int(1) NOT NULL DEFAULT '1',
  `roles` varchar(255) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `page_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_menu`
--

LOCK TABLES `yii_menu` WRITE;
/*!40000 ALTER TABLE `yii_menu` DISABLE KEYS */;
INSERT INTO `yii_menu` VALUES (16,'Apie projektą','/pages/view&id=3',2,1,1,'',NULL,NULL),(18,'Menu management','/menu/index',3,1,1,'',NULL,NULL),(19,'Page management','/pages/index',4,1,1,'',NULL,NULL),(23,'News','/news/index',100,2,1,'',NULL,NULL),(24,'Search for courses','/courses/index',200,2,1,'',NULL,NULL),(25,'My courses','/courses/mycourses',300,2,1,'Admin;;ContentManager;;CourseAdmin;;Operator;;Student;;Tutor;;UserManager',NULL,NULL),(26,'Virtual Learning Environment','https://upcvma-moodle.elinara.lt',400,2,1,'',NULL,NULL),(27,'Messages','#',710,2,1,'Admin;;ContentManager;;CourseAdmin;;Operator;;Student;;Tutor;;UserManager',28,NULL),(28,'Profile','#',705,2,1,'Admin;;ContentManager;;CourseAdmin;;Operator;;Student;;Tutor;;UserManager',30,NULL),(29,'Create a course','/courses/create',720,2,1,'Admin;;ContentManager;;CourseAdmin',27,NULL),(30,'Testas','#',605,2,1,'',NULL,NULL),(61,'kitas_psl','',25,1,1,'',NULL,NULL);
/*!40000 ALTER TABLE `yii_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_news`
--

DROP TABLE IF EXISTS `yii_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(255) NOT NULL,
  `trans_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `short_body` text,
  `body` text,
  `sms_body` text,
  `state` int(1) DEFAULT NULL COMMENT '1 - paskelbtas, 0 - tvarkomas, 2 - panaikintas',
  `general` int(1) DEFAULT NULL,
  `created` int(11) DEFAULT NULL,
  `edited` int(11) DEFAULT NULL,
  `sent` int(1) DEFAULT '0',
  `last_editor` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `organization` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_news_users1_idx` (`last_editor`),
  KEY `fk_yii_news_yii_users1_idx` (`author`),
  CONSTRAINT `fk_news_users1` FOREIGN KEY (`last_editor`) REFERENCES `yii_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_yii_news_yii_users1` FOREIGN KEY (`author`) REFERENCES `yii_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_news`
--

LOCK TABLES `yii_news` WRITE;
/*!40000 ALTER TABLE `yii_news` DISABLE KEYS */;
INSERT INTO `yii_news` VALUES (1,'en',1,'Test','Short body test','<p><strong>Full body test Full body test Full body test Full body test Full body test Full body test Full body test Full body test Full body test</strong></p>\r\n','SMS body test\r\nSMS body test\r\nSMS body test\r\nSMS body test\r\nSMS body test',1,1,1371191805,1375162135,1,14,14,397),(67,'lt',67,'Tik vienas','asf','asf','',0,0,1371639734,1372242475,0,14,14,NULL),(72,'lt',72,'Dar viena','Trumpas turinys','tr','trump',1,0,1372410776,1373023528,1,1,14,NULL),(73,'lt',73,'Bebrai','Rasti nauji bebrai Nemune','<p>Rasti nauji bebrai Nemune Rasti nauji bebrai Nemune Rasti nauji bebrai Nemune Rasti nauji bebrai Nemune Rasti nauji bebrai Nemune</p>\r\n','Rasti nauji bebrai Nemune SMS',1,1,1372419344,1375177886,1,14,14,397),(74,'lt',74,'Bendro pobūdžio naujiena','Bendro pobūdžio naujienos tekstas trumpas','Bendro pobūdžio naujienos tekstas\r\nBendro pobūdžio naujienos tekstas\r\nBendro pobūdžio naujienos tekstas','Bendro pobūdžio naujienos tekstas SMS',1,1,1372420760,1373023629,1,1,14,NULL),(75,'en',75,'Angliška naujiena','asdf','<p>fasf</p>\r\n','sdfasd',1,1,1372421020,1375272618,0,14,14,397),(76,'lt',76,'Naujiena1 lietuviškai','tekstas','tekstas','tekstas',0,0,1372588176,1372588176,0,34,14,NULL),(77,'en',76,'Naujiena 1 in english','text','text','text',0,0,1372588253,1372588253,0,34,14,NULL),(78,'ru',76,'Naujiena po ruski','po ruski','po ruski','po ruski',1,1,1372545267,1372660935,0,34,14,NULL),(82,'en',73,'Bebras  EN','asdfasdf','asdfasdfas','asdfasdf',0,1,1372759713,1374646184,0,14,14,NULL),(83,'ru',73,'ghgh','hggh','hggh','ghgh',0,1,1372857849,1372857849,0,14,14,NULL),(97,'lt',97,'1','asdf','asdf','asdf',1,0,1372920518,1372920518,0,14,14,NULL),(98,'en',98,'fdfsdf','fssfas','fsdfasf','',0,1,1373022996,1373023324,0,14,14,NULL),(100,'lt',100,'Test naujiena','tt','Tai yra testinė naujiena, skirta išbandyti laiškų prenumeratos siuntimą ','',1,1,1373025825,1373025839,1,1,1,NULL),(102,'en',99,'jgkghk','ghkj','ghjkjghj','',0,1,1373286584,1373286584,0,14,14,NULL),(104,'en',104,'asdfsadf','adsfasdf','<p>asfasfsad</p>\r\n','',0,0,1374751953,1374752032,0,14,14,395),(105,'de',76,'Naujiena DE','asdf','<p>sadf</p>\r\n','asdf',1,1,1375161942,1375161942,0,14,14,397);
/*!40000 ALTER TABLE `yii_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_news_sub_methods`
--

DROP TABLE IF EXISTS `yii_news_sub_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_news_sub_methods` (
  `subscription_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  KEY `fk_yii_subscription_methods_yii_news_subscription1_idx` (`subscription_id`),
  CONSTRAINT `fk_yii_subscription_methods_yii_news_subscription1` FOREIGN KEY (`subscription_id`) REFERENCES `yii_news_subscription` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_news_sub_methods`
--

LOCK TABLES `yii_news_sub_methods` WRITE;
/*!40000 ALTER TABLE `yii_news_sub_methods` DISABLE KEYS */;
INSERT INTO `yii_news_sub_methods` VALUES (11,'email'),(13,'email'),(13,'sms'),(15,'email'),(15,'sms'),(16,'email'),(16,'sms'),(14,'sms'),(12,'email'),(12,'sms');
/*!40000 ALTER TABLE `yii_news_sub_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_news_subscription`
--

DROP TABLE IF EXISTS `yii_news_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_news_subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `type` enum('none','all','general','classificator') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_news_subscription_users1_idx` (`uid`),
  CONSTRAINT `fk_news_subscription_users1` FOREIGN KEY (`uid`) REFERENCES `yii_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_news_subscription`
--

LOCK TABLES `yii_news_subscription` WRITE;
/*!40000 ALTER TABLE `yii_news_subscription` DISABLE KEYS */;
INSERT INTO `yii_news_subscription` VALUES (11,35,'all'),(12,14,'classificator'),(13,15,'general'),(14,17,'classificator'),(15,7,'all'),(16,36,'classificator');
/*!40000 ALTER TABLE `yii_news_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_organizacijos`
--

DROP TABLE IF EXISTS `yii_organizacijos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_organizacijos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pavadinimas` text,
  `savivaldybe` text,
  `bustines_adresas` text,
  `telefonas` varchar(80) DEFAULT NULL,
  `el_pastas` varchar(255) DEFAULT NULL,
  `svetaine` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_organizacijos`
--

LOCK TABLES `yii_organizacijos` WRITE;
/*!40000 ALTER TABLE `yii_organizacijos` DISABLE KEYS */;
INSERT INTO `yii_organizacijos` VALUES (1,'VATESI','Kauno','Laisvės al. 74-G10','+3554210968','organizacija@asd.lt','wawadwda.com');
/*!40000 ALTER TABLE `yii_organizacijos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_pages`
--

DROP TABLE IF EXISTS `yii_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `content` text CHARACTER SET utf8,
  `organizacijos_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `busena` int(11) DEFAULT NULL,
  `trans_id` int(11) DEFAULT NULL,
  `lang` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_pages`
--

LOCK TABLES `yii_pages` WRITE;
/*!40000 ALTER TABLE `yii_pages` DISABLE KEYS */;
INSERT INTO `yii_pages` VALUES (3,'Apie projektą','<p>Čia bus informacija apie projektą</p>\r\n',NULL,1,'2013-07-24',1,3,'lt'),(5,'Kitas','<p>kitas puslapis</p>\r\n',NULL,NULL,'2013-07-30',1,5,'lt'),(6,'Testas','<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>\r\n\r\n<blockquote>\r\n<p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p>\r\n</blockquote>\r\n\r\n<p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>\r\n\r\n<p>&nbsp;</p>\r\n',22,16,'2013-07-24',1,6,'lt'),(11,'asd','<p>adasdasdasd</p>\r\n',1,15,'2013-07-21',1,11,'lt'),(12,'Abc','<p>abc</p>\r\n',NULL,NULL,'2013-07-30',1,12,'en'),(14,'Antras vertimas','<p>ASdasd</p>\r\n',1,1,'2013-07-30',1,3,'en'),(15,'Menu_patikrinimas','<p>asdasdawsd</p>\r\n',NULL,NULL,'2013-08-01',NULL,15,'en');
/*!40000 ALTER TABLE `yii_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_populiarumas`
--

DROP TABLE IF EXISTS `yii_populiarumas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_populiarumas` (
  `id` int(11) NOT NULL,
  `cId` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_populiarumas`
--

LOCK TABLES `yii_populiarumas` WRITE;
/*!40000 ALTER TABLE `yii_populiarumas` DISABLE KEYS */;
/*!40000 ALTER TABLE `yii_populiarumas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_profiles`
--

DROP TABLE IF EXISTS `yii_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_profiles` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `savivaldybe` varchar(255) DEFAULT NULL,
  `telefonas` varchar(255) DEFAULT NULL,
  `svetaine` varchar(255) DEFAULT NULL,
  `registravimo_budas` varchar(255) DEFAULT NULL,
  `skype` varchar(255) NOT NULL DEFAULT '',
  `organization` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `user_profile_id` FOREIGN KEY (`user_id`) REFERENCES `yii_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_profiles`
--

LOCK TABLES `yii_profiles` WRITE;
/*!40000 ALTER TABLE `yii_profiles` DISABLE KEYS */;
INSERT INTO `yii_profiles` VALUES (1,'Administrator','Admin','383','+37067506665','',NULL,'',0),(2,'Chuanas','Pablo','382','867506665','',NULL,'chuanito789',0),(7,'Edvinas','Matulaitis','382','','','','',0),(14,'Mindaugas','Laurinaitis','382','','',NULL,'',397),(15,'StudVardas','StudPavardė','382','8675099998','',NULL,'',0),(16,'TutorVard','TutorPavard','382','8673566985','',NULL,'',0),(17,'OperatorVard','OperatorPavard','382','867599987','',NULL,'',0),(18,'CourseAdminVard','CourseAdminPavard','382','8675044478','',NULL,'',0),(20,'ContentManagerVard','ContentManagerPavard','382','','',NULL,'',0),(21,'UsermanagerVard','UserManagerPavard','382','','',NULL,'',0),(22,'administratorius1','administratorius1','382','','','Administratoriaus registravimas','',0),(23,'Giedrius','Balbieris','382','+37068792946','','Administratoriaus registravimas','',0),(24,'Studentas1','Studentauskas1','382','','','Savarankiškas registravimasis','',0),(25,'Vartotojas2','Vartotojauskas2','382','','','Savarankiškas registravimasis','',0),(26,'Gytis','Cibulskis','382','','','Savarankiškas registravimasis','',0),(27,'Vartotojas3','Vartotojauskas3','382','','','Savarankiškas registravimasis','',0),(28,'Testas','Testujus','382','','','Savarankiškas registravimasis','',0),(29,'Testas','Testujus','382','','','Savarankiškas registravimasis','',0),(30,'Testas','Testujusz','382','','','Savarankiškas registravimasis','',0),(31,'Stud','Stud','382','','','Savarankiškas registravimasis','',0),(32,'kursu','adminas','382','','','Savarankiškas registravimasis','',0),(33,'kursuadmin2','kursuadmin2','382','','','Savarankiškas registravimasis','',0),(34,'superadmin1','superadmin1','382','','','Savarankiškas registravimasis','',0),(35,'mind','mind','382','asdf','af','Administratoriaus registravimas','',0),(36,'Vartotojas4','Vartotojauskas4','382','','','Savarankiškas registravimasis','',0),(37,'Varotojas5','Vartotojauskas5','382','','','Administratoriaus registravimas','',0),(38,'arvydas1','vainauskas','382','86161','','Savarankiškas registravimasis','',0),(39,'Mindaugas','Laurinaitis','382','','','Savarankiškas registravimasis','',0),(40,'Paulius','Tatarunas','382','','','Savarankiškas registravimasis','',0),(49,'qweqwe','qweqwe','382','','','Administratoriaus registravimas','',0),(50,'Edvinas','MMM','382','','','Savarankiškas registravimasis','',0),(51,'Stud4','Stud4','382','','','Administratoriaus registravimas','',0);
/*!40000 ALTER TABLE `yii_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_profiles_fields`
--

DROP TABLE IF EXISTS `yii_profiles_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_profiles_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_profiles_fields`
--

LOCK TABLES `yii_profiles_fields` WRITE;
/*!40000 ALTER TABLE `yii_profiles_fields` DISABLE KEYS */;
INSERT INTO `yii_profiles_fields` VALUES (1,'first_name','First Name','VARCHAR',255,3,1,'','','Incorrect First Name (length between 3 and 50 characters).','','','','',1,3),(2,'last_name','Last Name','VARCHAR',255,3,1,'','','Incorrect Last Name (length between 3 and 50 characters).','','','','',2,3),(5,'savivaldybe','Savivaldybė','VARCHAR',255,0,1,'','','','','','','',5,1),(6,'telefonas','Telefonas (formatas pvz  +3706XXXXXXX)','VARCHAR',255,0,2,'/^\\+[0-9]+$/','','','','','','',6,1),(7,'svetaine','Svetainė','VARCHAR',255,0,2,'','','',NULL,'','',NULL,7,1),(8,'registravimo_budas','Registravimo būdas','VARCHAR',255,0,0,'','','',NULL,'','',NULL,8,0),(10,'skype','Skype','VARCHAR',255,0,0,'','','','','','','',0,1),(11,'organization','Organizacija','INTEGER',10,0,0,'','','','','0','','',0,3);
/*!40000 ALTER TABLE `yii_profiles_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_reitingas`
--

DROP TABLE IF EXISTS `yii_reitingas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_reitingas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `vertinimas` int(11) DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_reitingas`
--

LOCK TABLES `yii_reitingas` WRITE;
/*!40000 ALTER TABLE `yii_reitingas` DISABLE KEYS */;
INSERT INTO `yii_reitingas` VALUES (4,13,1,1),(5,13,2,1),(8,13,31,1);
/*!40000 ALTER TABLE `yii_reitingas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_relations`
--

DROP TABLE IF EXISTS `yii_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_relations` (
  `type1` varchar(45) NOT NULL,
  `id_t1` int(11) NOT NULL,
  `type2` varchar(45) NOT NULL,
  `id_t2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_relations`
--

LOCK TABLES `yii_relations` WRITE;
/*!40000 ALTER TABLE `yii_relations` DISABLE KEYS */;
INSERT INTO `yii_relations` VALUES ('news',67,'subject_area',379),('news',67,'competence',375),('news',67,'competence',376),('news',72,'subject_area',378),('news',72,'subject_area',380),('news',72,'competence',373),('news',72,'competence',375),('news',74,'subject_area',379),('news',74,'competence',373),('news',76,'subject_area',378),('news',76,'subject_area',379),('news',76,'competence',373),('news',76,'competence',375),('news',77,'subject_area',378),('news',77,'subject_area',379),('news',77,'competence',373),('news',77,'competence',375),('news',79,'subject_area',378),('news',79,'subject_area',380),('news',79,'competence',373),('news',78,'subject_area',378),('news',78,'subject_area',379),('news',78,'competence',373),('news',78,'competence',375),('news',81,'subject_area',378),('news',81,'subject_area',380),('news',81,'competence',375),('news',81,'competence',377),('news',83,'subject_area',379),('news',83,'subject_area',380),('news',83,'competence',376),('news',85,'subject_area',380),('news',85,'competence',373),('news',85,'competence',375),('news',97,'subject_area',378),('news',97,'subject_area',380),('news',97,'competence',376),('newssubscription',13,'subject_area',379),('newssubscription',13,'competence',375),('news',98,'subject_area',378),('news',98,'subject_area',380),('news',98,'competence',373),('news',98,'competence',376),('newsSubscription',16,'subject_area',378),('newsSubscription',16,'subject_area',379),('newsSubscription',16,'subject_area',380),('newsSubscription',16,'subject_area',381),('newsSubscription',16,'competence',373),('newsSubscription',16,'competence',375),('newsSubscription',16,'competence',376),('newsSubscription',16,'competence',377),('news',100,'subject_area',378),('news',100,'subject_area',380),('news',100,'competence',373),('news',100,'competence',376),('news',102,'subject_area',379),('news',102,'subject_area',380),('news',102,'subject_area',381),('news',102,'competence',375),('news',102,'competence',376),('news',102,'competence',377),('user',49,'subject_area',380),('user',49,'subject_area',381),('user',49,'competence',373),('user',49,'competence',375),('user',49,'subject_area',378),('user',49,'subject_area',380),('user',49,'subject_area',381),('user',49,'competence',373),('user',49,'competence',375),('user',49,'competence',377),('user',50,'subject_area',378),('user',50,'subject_area',379),('user',50,'subject_area',380),('user',50,'subject_area',381),('user',50,'competence',376),('news',82,'subject_area',379),('news',82,'subject_area',381),('news',82,'competence',376),('news',82,'competence',377),('news',82,'course',29),('news',82,'course',30),('news',104,'subject_area',378),('news',104,'course',30),('news',104,'acquired_competence',394),('news',105,'subject_area',378),('news',105,'subject_area',379),('news',105,'competence',373),('news',105,'competence',375),('news',1,'competence',376),('news',1,'competence',377),('news',1,'course',29),('news',1,'course',30),('news',1,'course',31),('news',73,'subject_area',379),('news',73,'subject_area',381),('news',73,'competence',376),('news',73,'competence',377),('news',73,'course',29),('news',73,'course',31),('news',73,'acquired_competence',391),('news',73,'acquired_competence',392),('news',73,'acquired_competence',393),('news',73,'acquired_competence',394),('news',73,'acquired_competence',395),('newsSubscription',14,'subject_area',378),('newsSubscription',14,'subject_area',379),('newsSubscription',14,'subject_area',380),('newsSubscription',14,'subject_area',381),('newsSubscription',14,'competence',373),('newsSubscription',14,'competence',375),('newsSubscription',14,'competence',376),('newsSubscription',14,'competence',377),('newsSubscription',14,'course',30),('newsSubscription',14,'course',31),('news',75,'subject_area',381),('news',75,'competence',376),('news',75,'course',29),('newsSubscription',12,'course',29);
/*!40000 ALTER TABLE `yii_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_settings`
--

DROP TABLE IF EXISTS `yii_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_settings` (
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` varchar(255) NOT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_settings`
--

LOCK TABLES `yii_settings` WRITE;
/*!40000 ALTER TABLE `yii_settings` DISABLE KEYS */;
INSERT INTO `yii_settings` VALUES ('svetaines telefonas','867506668','Svetainės telefonas išvedamas apačioje');
/*!40000 ALTER TABLE `yii_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yii_users`
--

DROP TABLE IF EXISTS `yii_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yii_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `createtime` int(10) NOT NULL DEFAULT '0',
  `lastvisit` int(10) NOT NULL DEFAULT '0',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username` (`username`),
  UNIQUE KEY `user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yii_users`
--

LOCK TABLES `yii_users` WRITE;
/*!40000 ALTER TABLE `yii_users` DISABLE KEYS */;
INSERT INTO `yii_users` VALUES (1,'admin','22eff6ed2ac41e3bb50f228f23051e4d','tomas.madajevas@distance.ktu.lt','f3680d0707c108ffd1494cef2bc42e2f',1368000917,1375710606,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'chuanas','f6b2b361bbe0c9d9a5298a0b331000be','chuanas@test.lt','7f55200e175641a39a6376f39d086968',0,1374470055,0,1,'2013-05-09 07:34:56','0000-00-00 00:00:00'),(7,'edvinas','1e91fc98c5b46c6dc9dea39cb042d358','edvinas@distance.ktu.lt','74bec96dbfd207b78c88486a38190b68',0,1373453830,0,1,'2013-05-27 03:22:53','0000-00-00 00:00:00'),(14,'Mindaugas','f6b2b361bbe0c9d9a5298a0b331000be','mindaugas.laurinaitis@distance.ktu.lt','b055540453b7c8c85d5d36dda45a2282',0,1373005193,0,1,'2013-05-28 06:36:17','0000-00-00 00:00:00'),(15,'Student','f6b2b361bbe0c9d9a5298a0b331000be','student@upcvma.elinara.lt','00849c9c885085336f76b0f66a3bcbe9',0,1374654873,0,1,'2013-05-28 07:22:59','0000-00-00 00:00:00'),(16,'Tutor','f6b2b361bbe0c9d9a5298a0b331000be','tutor@upcvma.elinara.lt','aa291e0db38e99a52fb059d8c00721ae',0,1373973605,0,1,'2013-05-28 07:30:15','0000-00-00 00:00:00'),(17,'Operator','f6b2b361bbe0c9d9a5298a0b331000be','operator@upcvma.elinara.lt','b97fb05c8766dc1b64b6772a8a78a5df',0,1373025292,0,1,'2013-05-28 07:31:55','0000-00-00 00:00:00'),(18,'CourseAdmin','f6b2b361bbe0c9d9a5298a0b331000be','courseadmin@upcvma.elinara.lt','4d1d46fe47e453e176df9ad0a3d0d544',0,0,0,1,'2013-05-28 07:33:14','0000-00-00 00:00:00'),(20,'ContentManager','f6b2b361bbe0c9d9a5298a0b331000be','contentmanager@upcvma.elinara.lt','1e9e4f5fe7f4015e9faa9222ca6e4070',0,0,0,1,'2013-05-28 08:48:07','0000-00-00 00:00:00'),(21,'UserManager','f6b2b361bbe0c9d9a5298a0b331000be','usermanager@upcvma.elinara.lt','d10f8d509f8471dad5b59072ee15b5b9',0,0,0,1,'2013-05-28 08:50:03','0000-00-00 00:00:00'),(22,'administratorius1','288742a6a565f9c3d239f173cae553f9','administratorius1@pastas.lt','60ce21adfa3cb27b9c51e0e32b629563',0,1372583798,0,1,'2013-06-19 10:39:52','0000-00-00 00:00:00'),(23,'balbgied','1e91fc98c5b46c6dc9dea39cb042d358','gb@metalot.com','09547738da89bcff68a793aef6067268',0,1371643460,1,1,'2013-06-19 12:03:25','0000-00-00 00:00:00'),(24,'studentas1','775a2b47806a989492901cbcaa68a9ec','studentas1@pastas.lt','8387ec9a9d33cfcb23c34bdf70529c17',0,0,0,0,'2013-06-19 13:56:07','0000-00-00 00:00:00'),(25,'vartotojas2','7e48a06fd7ac739fd610cea523b9198e','vaidas.astrovas@distance.ktu.lt','7b73efd6ca450704e202bcd5c4f01c59',0,0,0,0,'2013-06-19 13:57:33','0000-00-00 00:00:00'),(26,'gytisc','49bfde8384d0c59c9b08acf66e2dc0d8','gytisc@gmail.com','78b5a72c0c9ccc21f5ea2fd2ef604230',0,1371708344,0,1,'2013-06-20 05:47:40','0000-00-00 00:00:00'),(27,'vartotojas3','d980ca865ed3e638b19d7e53f4878813','vaidas.astrovas+1@distance.ktu.lt','6b630d1c87985c5aeef7755168d8ad66',0,0,0,0,'2013-06-20 07:01:11','0000-00-00 00:00:00'),(28,'Test_vienas','9c298a49e1fcb4a18eb095df1fcdb43d','t@t.lt','f75420813dd21181a93b8a4c0fbcbc06',0,0,0,0,'2013-06-26 09:02:36','0000-00-00 00:00:00'),(29,'gesykle','8d65aebcd37a8f9997c6319d661c6769','gesykle@gmail.com','34153b267d6c55f1bc01a4505b965998',0,0,0,0,'2013-06-26 09:38:46','0000-00-00 00:00:00'),(30,'gesyk','3362a3a59c88a6581bbb75a8a3eff395','tmas1992@yahoo.com','9ccea6fc61bd59723c43b9d7b0de6c59',0,0,0,0,'2013-06-26 09:41:49','0000-00-00 00:00:00'),(31,'stud3','1200d85c4e3c81c33fca7e027ba9dc76','stud3@mail.org','7f994c1cbc9ce095269701bf4185e07f',0,1375704358,0,1,'2013-06-27 12:51:13','0000-00-00 00:00:00'),(32,'kursuAdmin1','5440a8f90c6cbbd897d49cedd645b5d3','ka1@mail.neet','71173c00587b22294f1fd5cac3f3b3d2',0,1373371174,0,1,'2013-06-28 12:00:51','0000-00-00 00:00:00'),(33,'kursuadmin2','5bb7db6caae041af00380716f560b998','kursuadmin2@pastas.lt','d4af3efe6b270ef0a31b61a127f63c21',0,1372588834,0,1,'2013-06-30 09:27:36','0000-00-00 00:00:00'),(34,'superadmin1','f13c3e33664672040df7017dfb3866c1','vaidas.astrovas+2@distance.ktu.lt','53ee78a1786ae7c751ad93a36d9d18e0',0,1374843253,1,1,'2013-06-30 09:55:47','0000-00-00 00:00:00'),(35,'Testinukas','5bda64ad4a309cb144ba75d54a630ea2','minduxz2@gmail.com','5bcce350f0c188629b74123ae49b8b1b',0,0,0,1,'2013-07-01 05:59:14','0000-00-00 00:00:00'),(36,'vartotojas4','22ba233fdfd3ef19f7f3d06495cd7a38','vaidas.astrovas+3@distance.ktu.lt','f9c6d9a5528aca05a638c30ad1d33399',0,1373025747,0,1,'2013-07-01 06:33:19','0000-00-00 00:00:00'),(37,'vartotojas5','1ff9b6265504b4befd5ac06332aaf97e','vaidas.astrovas+4@distance.ktu.lt','84b5a0ad8572bb078165c39960a6996a',0,0,0,1,'2013-07-01 06:36:07','0000-00-00 00:00:00'),(38,'arvydas','db194367e1cba97ae4f27dc1162fef85','arvvns@yahoo.com','69bdc1a0fcc12bd85cb8b618e6921b30',0,1372765628,0,1,'2013-07-02 11:03:35','0000-00-00 00:00:00'),(39,'Registracija','f6b2b361bbe0c9d9a5298a0b331000be','mind.laurinaitis@gmail.com','542ad50eb899cbf7a4d7ef5df714209b',0,0,0,1,'2013-07-08 13:41:20','0000-00-00 00:00:00'),(40,'Paulius','7750fdaccf331d7a79600771ee1fc3a6','p.tatarunas@gmail.com','85f767c8e567f6372f70a0111d817a3a',0,1374584585,0,1,'2013-07-10 06:02:51','0000-00-00 00:00:00'),(49,'qweqwe','66d7b5dd3b96978ec5217b3c098acaad','edvinas+1@distance.ktu.lt','af23d38bf048b71a07cce1bcf88a6a96',0,0,0,1,'2013-07-10 08:56:29','0000-00-00 00:00:00'),(50,'asdasd','1e91fc98c5b46c6dc9dea39cb042d358','edvinas+2@distance.ktu.lt','124a90f6438e3141e0a3b5804b9b1034',0,1374486144,0,1,'2013-07-10 11:57:50','0000-00-00 00:00:00'),(51,'stud4','e01aac8b775dc7a07e2f0eed69b5204d','stud4@mail.org','70e2f4f1c84c81f0e416ae35129bcdbb',0,1374486690,0,1,'2013-07-22 06:47:20','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `yii_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-17 12:55:09
