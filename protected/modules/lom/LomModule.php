<?php

class LomModule extends CWebModule {
	private $_assetsUrl;
	
	public function init() {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		$this->registerAssets();
		
		// import the module-level models and components
		$this->setImport(array(
			'lom.models.*',
			'lom.components.*',
		));
	}
	
	public function registerAssets() {
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/default.css');
	}	
	
	/**
	* @return string the base URL that contains all published asset files of this module.
	*/
	public function getAssetsUrl()	{
		if ($this->_assetsUrl === null)
		    $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
			Yii::getPathOfAlias('lom.assets') );
		return $this->_assetsUrl;
        }
        
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action)) {
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
