<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>
<?php

	include("protected/modules/lom/include/config_yii.php");
	include("protected/modules/lom/include/xml_struct.php");
	include("protected/modules/lom/include/class.Classificator.php");	
	include("protected/modules/lom/include/class.Course.php");
	include("protected/modules/lom/include/class.LomObject.php");
	
	$db = dbc(); 
	if($_REQUEST && $_REQUEST[submit] && $_REQUEST[xpath]) {
		$xml_spec = $db->GetArray("select * from meta1");
		$xml_items = new XmlInputForm($xml_spec);
		$xml_items->saveXmlInstancesIntoSession(); // save new data (coming via form post) into session
		$db->Execute("delete from metadata where object=" . $_SESSION['object_id']);
		foreach ($_SESSION['xml_instances'] as $key=>$value) {
			if(strpos($key, "lom") === 0) { // check if this is lom element, === so that FALSE does not match
				// @todo: if there is a hidden field - put default value in it
				$db->Execute("insert into metadata (object,xpath,value,item,parent,author,language) values ($_SESSION[object_id],'$key','". $value['value']. "'," . $value['id'] . ",". $value['parent'] . ",'" . $_SESSION['xml_instances']['author'] . "','" . $_SESSION['language'] . "')");
			}
		}
		header("Location: view");		
	} else {		
		unset($_SESSION['xml_instances']); // prepare place for the new metadata
		
		// save object info to the session
		$id = $_SESSION['object_id'] = (strlen($_GET[id])>0)?$_GET[id]:2;
		$_SESSION['language']= $CFG->default_metadata_language;
		$language = $CFG->default_metadata_language;
		
		
		$_SESSION['xml_instances'] = $db->GetAssoc("select xpath, item as `id`, xpath as `key`, value, parent from metadata where object=?", array($id));
		$xml_spec = $db->GetArray("select * from meta1");
		$xml_items = new XmlInputForm($xml_spec);
		$xml_items->mode=1; // viewing mode
		$xml_items->debug=0; // debug
		
		$xml_items->initXmlInstancesFromSession("lom"); // initialize object from session
		$xpath = strlen($_REQUEST['xpath']) ? $_REQUEST['xpath'] : 'lom';
		$xml_items->generateXmlForm($xpath); // generate form
?>

<div class="input-form">
<form name="lom" method="POST">
<input type="hidden" name="id" value="<?php echo $id ?>">
<input type="hidden" name="xpath" value="lom">
<?php echo $xml_items->form ?>
<input type="submit" name="submit" value="saugoti">
</form>
<?php
	}
?>
