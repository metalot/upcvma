<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>
<?php

	include("protected/modules/lom/include/config_yii.php");
	include("protected/modules/lom/include/xml_struct.php");
	include("protected/modules/lom/include/class.Classificator.php");
	include("protected/modules/lom/include/class.Course.php");
	include("protected/modules/lom/include/class.LomObject.php");

	$db = dbc(); 
	unset($_SESSION['xml_instances']); // prepare place for the new metadata

	$rs2 = $db->Execute("select * from metadata where xpath LIKE 'lom/general/title%'");
	
	while(!$rs2->EOF) {
		// print_r( $rs2 );
		echo "<div style='float:left;width:100%;'><div style='float:left; width=200px;'>" . $rs2->fields[object] . " " . $rs2->fields['value'] . "</div><div style='float:left'>" .
		" <a href='view?id=" . $rs2->fields[object] . "'>[ View ] </a>   " .
		" <a href='edit?id=" . $rs2->fields[object] . "'>[ Edit ] </a></div></div>";
		$_SESSION['xml_instances'][$rs2->fields['xpath']] = array (
				'id' => $rs2->fields['item'],
				'key' => $rs2->fields['xpath'],
				'value' => $rs2->fields['value'],
				'parent' => $rs2->fields['parent']					
				);				
		$rs2 -> MoveNext();
	}

?>

