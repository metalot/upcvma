<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("div.option_content").hide();
	jQuery("div.option_expand").click( function(){$(this).next().slideToggle(200);} );
});
</script>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>

<h1>Search learning objects</h1>
<div>
	<div style="float: left; width: 300px;">
		<div class="search-form">
			<form method="post">
				<div class="row option_expandable">
					<div class="option_expand">
						<label for="Object_title">Pavadinimas</label>	
					</div>
					<div class="option_content">
						<input size="40" maxlength="45" name="title" id="Object_title" type="text">
					</div>
				</div>
				<div class="row option_expandable">
					<div class="option_expand">
						<label for="Object_keyword">Raktinis žodis</label>
					</div>
					<div class="option_content">
						<input size="40" maxlength="45" name="keyword" id="Object_keyword" type="text">
					</div>
				</div>
				<div class="row option_expandable">
					<input class="btn" type="submit" name="lom_filter" value="Ieškoti">
				</div>
			</form>
		</div><!-- search-form -->
	</div>
	<div style="float: left; width: 500px; margin-left: 30px;">
		<div id="news-grid" class="list-view">
			<div class="items">
<?php

if($_REQUEST && $_REQUEST['lom_filter']) {
	// return ajax table body update for the filter
	include("protected/modules/lom/include/config_yii.php");
	include("protected/modules/lom/include/xml_struct.php");
	include("protected/modules/lom/include/class.Classificator.php");
	
	$db = dbc();
	$lo_list = $db->GetArray("select * from metadata as m where m.`value` LIKE '%".$_REQUEST['title']."%' AND m.xpath LIKE '%lom/general/title%'");
	foreach($lo_list as $lo) {
?>

<div class="view_list_line clearfix view_list_line_odd">
	<div class="view_list_info ">
		<div style="width: 90px; float: left;">
			<b>Pavadinimas:</b>
		</div>
		<div style="width: 200px; float: left; clear: right;">
			<a href="view?id=<?php echo $lo['object']; ?>"><?php echo $lo['object'] . ": " . $lo['value']; ?></a>
		</div>
	</div>
</div>

<?php

	} // foreach
} // if

?>

	</div>
</div>

 


