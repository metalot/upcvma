<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<h1><?php echo $this->uniqueId . '/' . $this->action->id; ?></h1>
<?php

	include("protected/modules/lom/include/config_yii.php");
	include("protected/modules/lom/include/xml_struct.php");
	include("protected/modules/lom/include/class.Classificator.php");
	include("protected/modules/lom/include/class.Course.php");
	include("protected/modules/lom/include/class.LomObject.php");

	$db = dbc(); 
	unset($_SESSION['xml_instances']); // prepare place for the new metadata
	
	// save object info to the session
	$id = $_SESSION['object_id'] = (strlen($_GET[id])>0)?$_GET[id]:2;
	$_SESSION['language']= $CFG->default_metadata_language;
	$language = $CFG->default_metadata_language;
	
	$_SESSION['xml_instances'] = $db->GetAssoc("select xpath, item as `id`, xpath as `key`, value, parent from metadata where object=?", array($id));
	$xml_spec = $db->GetArray("select * from meta1
                	inner join meta1_versions on STRCMP(meta1.xpath, meta1_versions.xpath)=0 and meta1_versions.version=1");
	$xml_items = new XmlInputForm($xml_spec);
	$xml_items->mode=0; // viewing mode
	$xml_items->debug=0; // debug
	
	$xml_items->initXmlInstancesFromSession("lom"); // initialize object from session
	$xpath = strlen($_REQUEST['xpath']) ? $_REQUEST['xpath'] : 'lom';
	$xml_items->generateXmlForm($xpath); // generate form
	$form = $xml_items->form;
	//	$xmlForm->debugToFile();
	$form_template = "object_add1.html";
	echo $form;

//        $course = new Course(29);
//        $course->displayForm(0,1); // displayForm(edit, version)
        

?>

