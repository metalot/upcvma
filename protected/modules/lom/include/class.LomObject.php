<?php

/**
* Class: LomObject
* Created: Giedrius Balbieris <gb@metalot.com>
* Date: 2013-09-11
* Purpose: implement LOM object CRUD functionality.
*/	

class LomObject {
	// object id
	protected $id;
	
	// xml specification is static so keep it in session for optimization
	protected $xml_specification;
	
	// xml values is an array which is temporarily stored in the session and saved to the database upon saving
	protected $xml_values;
	
	// output of the form or a view to be displayed or emailed
	protected $output = "";	

	/**
	* public function LomObject($id = 0)
	* initialize all (specification, object data etc) needed to operate the object.
	*/
	public function LomObject($id = 0) {
		global $db;
		
		$db = dbc();
		$this->loadSpecification();
		$this->loadObject($id);
	}

	/**
	* public function displayView($xpath = 'lom')
	* view object 
	* @param $xpath - which XML branch to show (or all by default) 
	*/	
	public function displayView($xpath = 'lom') {
		// see if this is a branch
		if( $this->isGroup($xpath) ) {
			$this->displayGroup($xpath);
			$children = $this->getChildren($xpath);
			foreach($children as $child) {
				$this->displayView($child);
			}
		} else {
			if($this->xml_values[$xpath][value]) $this->displayLeaf($xpath);
		}
	}

	/**
	* public function displayEdit($xpath = 'lom')
	* view object edit form 
	* @param $xpath - which XML branch to show (or all by default) 
	*/	
	public function displayEdit($xpath = 'lom') {
		echo "<form method=post><input type=hidden name=id value=$this->id>";
		$this->displayForm($xpath);
		echo "<div class='xml-group'><div class='xml-name'>&nbsp;</div><div class='xml-value'>" . CHtml::submitButton('Submit') . 
			"</div></div></form>";
	}		
	
	/**
	* public function displayForm($xpath = 'lom')
	* view object edit form 
	* @param $xpath - which XML branch to show (or all by default) 
	*/	
	public function displayForm($xpath = 'lom') {
		// see if this is a branch
		if( $this->isGroup($xpath) ) {
			$this->displayGroup($xpath);
			$children = $this->getChildren($xpath);
			foreach($children as $child) {
				$this->displayForm($child);
			}
		} else {
			if($this->xml_values[$xpath][value]) $this->displayFormLeaf($xpath);
		}
	}
	
	/**
	* private function loadSpecification()
	* load specification into the session (if not loaded)
	*/
	private function loadSpecification() {
		global $db;
		
		$this->xml_specification = &$_SESSION['xml_specification'];
		
		// if not loaded, load specification into the session
		if (! is_array($this->xml_specification)) {
			$this->xml_specification = $db->GetAssoc("select xpath, meta1.* from meta1");
		}
		
	}
	
	/**
	* private function loadObject($id = 0)
	* load object into the session (if not loaded)
	*/
	private function loadObject($id = 0) {
		global $db;
		
		$this->id = &$_SESSION['object_id'];
		$this->xml_values = &$_SESSION['xml_values'];
		
		if($id == 0) {
			unset($this->id);
			unset($this->xml_values);
		}
		
		$this->id = $id;
		
		// if not loaded, load specification into the session
		if (! is_array($this->xml_values) && $id>0) {
			$this->xml_values = $db->GetAssoc("select xpath, metadata.* from metadata where object=?", array($id));
		}
		
	}
	
	/**
	* public function saveObject($id = 0)
	* save object into the session and db
	*/
	public function saveObject($id = 0) {
		global $db;
		
		$this->xml_values = &$_SESSION['xml_values'];
		
		if($id == 0) {
			unset($this->id);
			unset($this->xml_values);
		}
		
		$this->id = $id;

		// $stmt = $db->prepare('update metadata set (object,xpath,value) values (?,?,?) where object=? and xpath=?');
		
		foreach ($_REQUEST as $xpath=>$value) {
			if(strpos($xpath,"lom") !== false) {
				echo "***+";
				$db->execute($stmt,array($_REQUEST[id],$xpath,$value,$_REQUEST[id],$xpath));
			}
		}
	}	
	
	/**
	* private function displayGroup($xpath)
	* displays group
	*/	
	private function displayGroup($xpath) {
		$xpath_pure = $this->cleanXpath($xpath);
		$level = substr_count($xpath, '/') + 1;
		echo "<div class='xml-group'><div class='xml-name xml-group-name-$level'>" . $this->xml_specification[$xpath_pure][id] . " " . $this->xml_specification[$xpath_pure][name_lt] . "</div></div>";		
	}
	
	/**
	* private function displayLeaf($xpath)
	* displays xml leaf
	*/	
	private function displayLeaf($xpath) {
		$xpath_pure = $this->cleanXpath($xpath);
		switch ( $this->xml_specification[$xpath_pure][html_type] ) {
			case "TEXTAREA":
			case "INPUT": $this->displayLeafInput($xpath); break;
			case 'CHECKBOX': $this->displayLeafCheckbox($xpath); break;
			case "CONNECTED":
			case "INTERVAL":
			case "DATE":				
			case "SELECT+A":
			case "SELECT":
			default: $this->displayLeafInput($xpath);
		}
	}	

	/**
	* private function displayFormLeaf($xpath)
	* displays xml leaf form
	*/	
	private function displayFormLeaf($xpath) {
		$xpath_pure = $this->cleanXpath($xpath);
		switch ( $this->xml_specification[$xpath_pure][html_type] ) {
			case "TEXTAREA":
			case "INPUT": $this->displayLeafFormInput($xpath); break;
			case 'CHECKBOX': $this->displayLeafFormCheckbox($xpath); break;
			case "CONNECTED":
			case "INTERVAL":
			case "DATE":				
			case "SELECT+A":
			case "SELECT":
			default: $this->displayLeafFormInput($xpath);
		}
	}
	
	/**
	* private function displayLeafInput($xpath)
	* displays input field value
	*/	
	private function displayLeafInput($xpath) {
		$xpath_pure = $this->cleanXpath($xpath);
		echo "<div class='xml-leaf'><div class='xml-name'>" . $this->xml_specification[$xpath_pure][name_lt] . "</div><div class='xml-value'>" . $this->xml_values[$xpath][value] . "</div></div>";
	}
	
	/**
	* private function displayLeafFormInput($xpath)
	* displays input field form
	*/	
	private function displayLeafFormInput($xpath) {
		$xpath_pure = $this->cleanXpath($xpath);
		echo "<div class='xml-leaf'><div class='xml-name'>" . $this->xml_specification[$xpath_pure][name_lt] . "</div><div class='xml-value'><input type='text' name='$xpath' size='30' value='" . $this->xml_values[$xpath][value] . "' /></div></div>";
	}	
	
	/**
	* private function displayLeafCheckbox($xpath)
	* displays checkbox field value
	*/	
	private function displayLeafCheckbox($xpath) {
		echo "<p>CHECKBOX: $xpath " . $this->xml_values[$xpath][value] . "</p>";
	}	
	
	/**
	* private function isGroup($xpath)
	* returns true/false
	*/	
	private function isGroup($xpath) {
		return $this->xml_specification[ $this->cleanXpath($xpath) ][html_type] == "GROUP";
	}		
	
	/**
	* private function getChildren($xpath)
	* returns an array of children xpaths
	*/	
	private function getChildren($xpath) {
		$parent = $this->xml_values[$xpath][item] ? $this->xml_values[$xpath][item] : 0;
		$children = array();
		foreach($this->xml_values as $xml_value) {
			if($xml_value[parent] == $parent) $children[] = $xml_value[xpath];
		}
		
		return $children;
	}

	/**
	* private function cleanXpath($xpath)
	* cleans xpath from numbers and and returns the result
	*/	
	private function cleanXpath($xpath) {
		return preg_replace("#^(\D+).*#","$1",$xpath);
	}

}

?>
