<?php

/**
* Class: Course
* Created: Giedrius Balbieris <gb@metalot.com>
* Date: 2013-08-06
* Purpose: implement course CRUD functionality (a limited subset as compared to the full LOM standard).
*/

class Course {
	protected $xml_spec; // xml specification displayable part
	protected $xml_items;	// xml items describing the course
	
	/**
	* @name Course($object_id)
	* initialize course for display or editing of LOM values
	* @param $object_id - LOM object id from the metadata table
	* @param $version LOM subset version from meta1_version table
	*/
	function Course($object_id) {
                $db = dbc();
                
                unset($_SESSION['xml_instances']); // prepare place for the new metadata
                
                // save object info to the session
                $_SESSION['object_id']=$object_id;
                $_SESSION['language']= $CFG->default_metadata_language;
                $language = $CFG->default_metadata_language;

	        $rs = $db->Execute("select * from metadata where object=$object_id");
	        while(!$rs->EOF) {
		        $_SESSION['xml_instances'][$rs->fields['xpath']] = array (
					        'id' => $rs->fields['item'],
					        'key' => $rs->fields['xpath'],
					        'value' => $rs->fields['value'],
					        'parent' => $rs->fields['parent']					
					        );
		        $rs -> MoveNext();
	        }
      	}
      	
	/**
	* @name displayForm ($mode = 0, $version = 0)
	* display course form: if $mode=0 it is view form, if $mode=1 it is edit form
	* @param $mode - view/edit mode selector
	* @param $version version of metadata
	*/      	
      	function displayForm ($mode = 0, $version = 0) {
      	        // save course xml specification
      	        $db = dbc();
                if ($version) {
                        $rs = $db->Execute("select * from meta1
 inner join meta1_versions on STRCMP(meta1.xpath, meta1_versions.xpath)=0 and meta1_versions.version=$version");
                } else {
                        $rs = $db->Execute("select * from meta1");
                }
                //                $rs = $db->Execute("select * from meta1");
                // print_r($_SESSION['xml_instances']);
                $this->xml_spec = $rs->GetArray();
                $this->xml_items = new XmlInputForm($this->xml_spec);
                $this->xml_items->mode=$mode; // viewing mode
                $this->xml_items->debug=0; // debug

		$this->xml_items->initXmlInstancesFromSession("lom"); // initialize object from session
		$xpath = strlen($_REQUEST['xpath']) ? $_REQUEST['xpath'] : 'lom/general';
		$this->xml_items->generateXmlForm($xpath); // generate form
		$form = $this->xml_items->form;
	//	$xmlForm->debugToFile();
		$form_template = "object_add1.html";
		echo $form;     	        
      	}
	
	function displayViewForm ($version = 0) {
	}
		
	function debug() {
		echo "\n<br>Cleassificator items:<br>\n";
		print_r($this->classificator_items);
		echo "\n<br>Cleassificator form:<br>\n";
		print_r($this->form);
	}
}

?>
