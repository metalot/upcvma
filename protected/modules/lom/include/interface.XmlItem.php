<?php

	interface XmlItem {

		/**
		* displays XmlItem
		* param string $return_string - if to echo out or return as a string.
		*/
		public function read($return_string = 0);

	}

?>
