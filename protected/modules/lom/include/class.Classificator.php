<?php

/**
* Class: Classificator
* Created: Giedrius Balbieris <gb@metalot.com>
* Date: 2007-08-20
* Purpose: read classificators from database and be able to represent those as XML, HTML forms etc.
*/

class Classificator {
	var $classificator_items = array (); // classificator items array, one item per line
	var $title = ""; // classisicator title
	var $searchable = 0; // is classificator searchable?
	var $mapping = 0; // mapping classificator is used only for XML export. By default it is not.
	// mapping classificators are special international classificators onto which local classificator values are mapped into.
	
	var $form = ""; // form for xml input

	function Classificator($db,$cid) {
		if(isset($db)) {
			$this->initFromDatabase($db,$cid);
		} else {
			echo "No database found.";
		}
	}
	
	function initFromDatabase($db,$cid) {
		$classificator = $db->GetRow("select title, searchable, mapping from classificators where id=$cid order by created");
		$this->title=$classificator['title'];
		$this->searchable=$classificator['searchable'];
		$this->mapping=$classificator['mapping'];
		$rs = $db->Execute("select * from classificator_items where cid=$cid order by created");
		$classificator_db = $rs->GetArray();
		for($i=0;$i<count($classificator_db); $i++) {
			$this->classificator_items[$classificator_db[$i]['id']] = array ('id' => $classificator_db[$i]['id'], 'parent' => $classificator_db[$i]['parent'], 'title' => $classificator_db[$i]['title']);
		}
	}

	function createForm($id=-1, $separator="", $class="", $value="") { 
		foreach($this->classificator_items as $classificator_item) {
			// Find all children of this $id node and launch into each recursively
			if($id == $classificator_item['parent']) {
				// found object whose parent is id - print it
				$selected = ($value==$classificator_item['id'])? "selected" : "";
				$this->form .= "\n<option value=\"" . $classificator_item['id'] . "\" $class $selected>" . $separator . $classificator_item['title'] . "</option>";
				// and print its' children
				$this->createForm($classificator_item['id'], $separator . "&nbsp;&nbsp;&nbsp;", $class, $value);
			}
		}
	}
	
	function createMapping($id=-1, $separator="", $mappings, $classificator2) { 
		foreach($this->classificator_items as $classificator_item) {
			// Find all children of this $id node and launch into each recursively
			if($id == $classificator_item['parent']) {
				// found object whose parent is id - print it
				$this->form .= "<tr><td>$separator" . $classificator_item['title'] . "</td><td><select name=" . $classificator_item['id'] . " size=1><option></option>";
				foreach($classificator2 as $c2item) {
					$selected_item = "";
					foreach($mappings as $mapping) {
						if($mapping['id1']==$classificator_item['id'] and $mapping['id2']==$c2item['id']) {
							$selected_item = " selected ";
							break;
						}
					}
					$this->form .= "<option $selected_item value=" . $c2item['id'] . ">" . $c2item['title'] . "</option>";
				}
				$this->form .= "</select></td></tr>";
				$this->createMapping($classificator_item['id'], $separator . "&nbsp;&nbsp;&nbsp;", $mappings, $classificator2);
			}
		}
	}
	
	function debug() {
		echo "\n<br>Cleassificator items:<br>\n";
		print_r($this->classificator_items);
		echo "\n<br>Cleassificator form:<br>\n";
		print_r($this->form);
	}
}

?>
