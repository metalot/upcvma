<?php

	class XmlItem extends XmlItemBase implements XmlItem {

		public function read($return_string = 0) {
			$display_value = "<div class=\"lomxml " . $xml_item . "\">" . $xml_value . "</div>";
			if ($return_string) { 
				return $display_value; 
			} else {
				echo $display_value;
			}
		}		

	}

?>
