<?php

/**
* Class: Search
* Created: Giedrius Balbieris <gb@metalot.com>
* Date: 2013-09-24
* Purpose: implements facet based search inside LOMBASE.
*/

include(__DIR__ . "/config_yii.php");
include(__DIR__ . "/xml_struct.php");
include(__DIR__ . "/class.Classificator.php");

class Search {
	
	/**
	* @name Search()
	* search controller
	* @param $version LOM subset version from meta1_version table - which will call each element and its' form will be displayed
	*/
	function Search() {
		if(strlen($_REQUEST['title'])>0) { $this->updateResults(); }
		else $this->displayForm();
      	}
      	
	/**
	* @name updateResults
	* update search results in ajax way
	*/
	function updateResults() {
		$t = "";
		$db = dbc();
		$lo_list = $db->GetArray("select * from metadata as m where m.`value` LIKE '%".$_REQUEST['title']."%' AND m.xpath LIKE '%lom/general/title%'");
		foreach($lo_list as $lo) {
			$t .= '
	<div class="view_list_line clearfix view_list_line_odd">
		<div class="view_list_info ">
			<div style="width: 30%; float: left;">
				<b>Pavadinimas:</b>
			</div>
			<div style="width: 70%; float: left; clear: right;">
				<a href="../view?id=' . $lo['object'] . '">' . $lo['object'] . ': ' . $lo['value'] .'</a><br />
			</div>
		</div>
	</div>';
		} // foreach	
		echo 'jQuery(\'#lom_results\').html(' . json_encode($t) . ');';	
		
	}

	/**
	* @name displayForm ($mode = 0, $version = 0)
	* display course form: if $mode=0 it is view form, if $mode=1 it is edit form
	* @param $mode - view/edit mode selector
	* @param $version version of metadata
	*/      	
      	function displayForm ($mode = 0, $version = 0) {
?>
<html>
<head>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
<body>
<h1>Search learning objects <?php echo $_REQUEST['filter'] ?></h1>
<div>
	<div style="float: left; width: 300px;">
		<div class="search-form">
			<form method="post" id="lom_search">
				<div class="row option_expandable">
					<div class="option_expand" style="background-image: url(http://upcvma.elinara.lt/themes/upcvma2/css/images/down.png); background-position: 100% 100%; background-repeat: no-repeat no-repeat;">
						<label for="Object_title">Pavadinimas</label>	
					</div>
					<div class="option_content">
						<input size="40" maxlength="45" name="title" id="Object_title" type="text">				</div>
				</div>
				<div class="row option_expandable">
 <input class="btn" type="button" name="filter" value="Ieškoti" onclick="jQuery.post(window.location,jQuery('#lom_search').serialize(),function(response){eval(response);});">
				</div>
			</form>
		</div><!-- search-form -->
	</div>
	<div style="float: left; width: 500px; margin-left: 30px;">
		<div id="news-grid" class="list-view">
			<div class="items" id="lom_results">
			</div>
		</div>
	</div>
</div>
</body>
</html>	

<?php		
      	} // function
	
} // class

?>
