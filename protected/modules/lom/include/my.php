<?php
	function dbc() {
		global $CFG;

		$db = NewADOConnection($CFG->dbtype);
		$db->Connect($CFG->host, $CFG->user, $CFG->psw, $CFG->db);
		// $db->Execute("SET NAMES 'cp1257'");
		$db->Execute("SET NAMES 'utf8'");
		return $db;
	}

	/**
	* @name generateId()
	* generate unique id
	* @return $id
	*/
	function generateId() {
		// @todo this generation is good as little objects exist
		// if the are more: initial session starts from 1
		// user picks up to edit object starting id from 10
		// object fields will start overlapping
		// rand is a workaround
		if(isset($_SESSION['xmlid'])) {
			$_SESSION['xmlid']++;
			return $_SESSION['xmlid'];
		} else {
			$_SESSION['xmlid']=rand(1,10000);
			return $_SESSION['xmlid'];
		}
	}
?>
