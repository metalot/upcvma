<?php

class CourseController extends Controller {
    public function actionIndex()
    {
	$this->render('index');
    }
    public function actionView()
    {
	$this->render('view');
    }        
    public function actionEdit()
    {
	$this->render('edit');
    }    
}
